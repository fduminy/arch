package fr.duminy.arch.api;

import java.util.Collection;

public interface ArchCollection<T extends ArchElement> {
    T get(String name);

    T get(int index);

    Collection<T> values();
}
