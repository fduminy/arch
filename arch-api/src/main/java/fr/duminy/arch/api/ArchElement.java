package fr.duminy.arch.api;

public interface ArchElement {
    int id();

    String name();
}
