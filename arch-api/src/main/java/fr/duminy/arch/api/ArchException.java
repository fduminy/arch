package fr.duminy.arch.api;

public final class ArchException extends Exception {
    public ArchException(String message) {
        super(message);
    }

    public ArchException(Exception e) {
        super(e);
    }
}
