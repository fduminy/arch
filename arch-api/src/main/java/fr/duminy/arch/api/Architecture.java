package fr.duminy.arch.api;

public interface Architecture {
    void accept(ArchitectureVisitor visitor);

    ClassDatabase database();
}
