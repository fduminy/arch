package fr.duminy.arch.api;

public interface ArchitectureBuilder {
    Architecture build() throws ArchException;
}
