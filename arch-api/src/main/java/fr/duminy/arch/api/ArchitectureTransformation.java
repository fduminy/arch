package fr.duminy.arch.api;

public interface ArchitectureTransformation {
    Architecture transform(Architecture original);
}
