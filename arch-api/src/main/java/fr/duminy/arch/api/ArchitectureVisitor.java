package fr.duminy.arch.api;

public interface ArchitectureVisitor {
    void visitClass(ArchClass archClass);
}
