package fr.duminy.arch.api;

import java.util.List;
import java.util.function.Consumer;

public interface ClassDatabase {
    ArchCollection<ArchClass> classes();

    List<ArchClass> classes(ArchPackage packag);

    void forEachUsedClass(ArchClass archClass, Consumer<ArchClass> usedClassConsumer);

    int[][] classDependencyIndexes();

    ArchCollection<ArchPackage> packages();

    List<ArchPackage> packages(ArchModule module);

    ArchPackage packag(ArchClass archClass);

    ArchCollection<ArchModule> modules();

    ArchModule module(ArchPackage packag);

    ArchModule module(ArchClass archClass);

    void invalidateCaches();
}
