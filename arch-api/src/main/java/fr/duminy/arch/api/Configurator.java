package fr.duminy.arch.api;

public interface Configurator {
    void configure(SourceArchitectureBuilder builder) throws ArchException;
}
