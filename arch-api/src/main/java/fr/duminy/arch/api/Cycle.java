package fr.duminy.arch.api;

import java.util.Collection;

public interface Cycle<T extends ArchElement> extends Collection<T> {
}
