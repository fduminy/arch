package fr.duminy.arch.api;

public final class Defaults {
    public static final ArchModule DEFAULT_MODULE = new ArchModule() {
        @Override
        public int id() {
            return 0;
        }

        @Override
        public String name() {
            return "";
        }
    };
    public static final ArchPackage DEFAULT_PACKAGE = new ArchPackage() {
        @Override
        public int id() {
            return 0;
        }

        @Override
        public String name() {
            return "";
        }
    };

    private Defaults() {
    }
}
