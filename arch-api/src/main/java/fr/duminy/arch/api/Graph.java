package fr.duminy.arch.api;

import java.util.Collection;

public interface Graph {
    <T extends ArchElement> Collection<Cycle<T>> findCycles(Class<T> type);
}
