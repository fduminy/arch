package fr.duminy.arch.api;

import java.nio.file.Path;

public interface SourceArchitectureBuilder extends ArchitectureBuilder {
    Path getRootFolder();

    SourceArchitectureBuilder configureWith(Configurator configurator);

    SourceArchitectureBuilder addSourceFolder(SourceFolder sourceFolder);

    Iterable<SourceFolder> getSourceFolders();

    SourceArchitectureBuilder addClass(String moduleName, String packageName, String classSimpleName, String className, String... usedClassNames);

}
