package fr.duminy.arch.api;

import java.nio.file.Path;

public record SourceFolder(String moduleName, Path folder) {
}
