package fr.duminy.arch.cli;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.converters.FileConverter;
import fr.duminy.arch.api.ArchException;
import fr.duminy.arch.api.Architecture;
import fr.duminy.arch.java.JavaSourceCodeScanner;
import fr.duminy.arch.maven.MavenConfigurator;

import java.io.File;
import java.io.PrintStream;

import static fr.duminy.arch.engine.ArchEngine.createSourceArchitectureBuilder;

@SuppressWarnings("unused")
abstract class AbstractCommand implements Command {
    @Parameter(names = "--root-folder", description = "The root folder for finding modules", converter = FileConverter.class, required = true)
    private File rootFolder;

    @Override
    @SuppressWarnings("java:S106")
    public final void execute() throws ArchException {
        execute(getArchitecture(), System.out);
    }

    @SuppressWarnings("SameParameterValue")
    abstract void execute(Architecture architecture, PrintStream out) throws ArchException;

    private Architecture getArchitecture() throws ArchException {
        return createSourceArchitectureBuilder(rootFolder.toPath())
                .configureWith(new MavenConfigurator())
                .configureWith(new JavaSourceCodeScanner())
                .build();
    }
}
