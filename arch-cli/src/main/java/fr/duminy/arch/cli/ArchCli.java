package fr.duminy.arch.cli;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.JCommander.Builder;
import com.beust.jcommander.ParameterException;
import fr.duminy.arch.api.ArchException;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;

public class ArchCli {
    @SuppressWarnings("java:S106")
    private static final PrintStream ERR = System.err;

    public static void main(String[] args) throws ArchException {
        Options options = new Options();
        JCommanderBuilder builder = new JCommanderBuilder();
        JCommander jCommander = builder.programName("arch-cli")
                .addObject(options)
                .addCommand("cycle", new CycleCommand())
                .addCommand("recommend", new RecommendCommand())
                .build();
        try {
            jCommander.parse(args);
        } catch (ParameterException pe) {
            ERR.println(pe.getMessage());
            pe.usage();
            return;
        }

        if (options.isHelp()) {
            jCommander.usage();
            return;
        }

        builder.getCommand(jCommander.getParsedCommand()).execute();
    }

    private static class JCommanderBuilder extends Builder {
        private final Map<String, Command> commands = new HashMap<>();

        @Override
        public Builder addCommand(String name, Object command, String... aliases) {
            commands.put(name, (Command) command);
            return super.addCommand(name, command, aliases);
        }

        Command getCommand(String name) {
            return commands.get(name);
        }
    }
}
