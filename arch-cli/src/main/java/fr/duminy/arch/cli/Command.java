package fr.duminy.arch.cli;

import fr.duminy.arch.api.ArchException;

public interface Command {
    void execute() throws ArchException;
}
