package fr.duminy.arch.cli;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import fr.duminy.arch.api.*;
import fr.duminy.arch.cycle.JGraphTFactory;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static java.util.stream.Collectors.joining;

@SuppressWarnings({"FieldCanBeLocal", "FieldMayBeFinal", "unused", "CanBeFinal"})
@Parameters(commandDescription = "Find cycles in the architecture")
public class CycleCommand extends AbstractCommand {
    @Parameter(names = "--module", description = "at module level")
    private boolean moduleLevel = false;

    @Parameter(names = "--package", description = "at package level")
    private boolean packageLevel = false;

    @Parameter(names = "--class", description = "at class level")
    private boolean classLevel = false;

    @Override
    public void execute(Architecture architecture, PrintStream out) {
        findCycles(out, new JGraphTFactory().createGraph(architecture));
    }

    private void findCycles(PrintStream out, Graph graph) {
        getTypes().forEach(type -> {
            out.printf("Cyclic dependencies at %s level%n", type.getSimpleName());
            Collection<? extends Cycle<? extends ArchElement>> cycles = graph.findCycles(type);
            if (cycles.isEmpty()) {
                out.printf("\tnone%n");
            } else {
                for (Cycle<?> cycle : cycles) {
                    out.printf("\t%s%n", cycle.stream()
                            .map(ArchElement::name).collect(joining(",")));
                }
            }
        });
    }

    private Iterable<Class<? extends ArchElement>> getTypes() {
        List<Class<? extends ArchElement>> types = new ArrayList<>();
        if (moduleLevel) {
            types.add(ArchModule.class);
        }
        if (packageLevel) {
            types.add(ArchPackage.class);
        }
        if (classLevel) {
            types.add(ArchClass.class);
        }
        return types;
    }
}
