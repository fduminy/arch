package fr.duminy.arch.cli;

import com.beust.jcommander.Parameter;

@SuppressWarnings({"FieldCanBeLocal", "FieldMayBeFinal", "CanBeFinal"})
public class Options {
    @Parameter(names = {"--help", "-h"}, help = true, description = "Display help")
    private boolean help = false;

    public boolean isHelp() {
        return help;
    }
}
