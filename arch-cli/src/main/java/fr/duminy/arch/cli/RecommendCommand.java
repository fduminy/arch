package fr.duminy.arch.cli;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import fr.duminy.arch.api.ArchElement;
import fr.duminy.arch.api.ArchException;
import fr.duminy.arch.api.Architecture;
import fr.duminy.arch.api.Cycle;
import fr.duminy.arch.engine.Action;
import fr.duminy.arch.recommendation.AbstractCycleBreakerRecommendationsVisitor;
import fr.duminy.arch.recommendation.CycleBreaker;
import fr.duminy.arch.recommendation.CycleBreakerRecommendations;
import org.jheaps.annotations.VisibleForTesting;

import java.io.PrintStream;
import java.util.Arrays;

import static fr.duminy.arch.recommendation.CycleBreakerBuilder.builder;
import static fr.duminy.arch.recommendation.CycleLevel.MODULE;
import static fr.duminy.arch.recommendation.CycleLevel.PACKAGE;
import static java.lang.System.lineSeparator;
import static java.util.stream.Collectors.joining;

@SuppressWarnings({"FieldCanBeLocal", "FieldMayBeFinal", "unused", "CanBeFinal"})
@Parameters(commandDescription = "Recommend changes to your architecture")
public class RecommendCommand extends AbstractCommand {
    @Parameter(names = "--module", description = "at module level")
    private boolean moduleLevel = false;

    @Parameter(names = "--package", description = "at package level")
    private boolean packageLevel = false;

    @Override
    public void execute(Architecture architecture, PrintStream out) throws ArchException {
        if (moduleLevel) {
            recommend(architecture, out, builder().level(MODULE).movePackage(true).moveClass(true).build(), "module");
        }
        if (packageLevel) {
            recommend(architecture, out, builder().level(PACKAGE).moveClass(true).build(), "package");
        }
    }

    @VisibleForTesting
    static void execute(boolean moduleLevel, boolean packageLevel, Architecture architecture, PrintStream out) throws ArchException {
        RecommendCommand command = new RecommendCommand();
        command.moduleLevel = moduleLevel;
        command.packageLevel = packageLevel;
        command.execute(architecture, out);
    }

    private void recommend(Architecture architecture, PrintStream out,
                           CycleBreaker cycleBreaker, String level) {
        out.printf("--- Recommendations at %s level ---%n", level);
        CycleBreakerRecommendations recommendations = cycleBreaker.find(architecture);
        Visitor visitor = new Visitor(out);
        recommendations.accept(visitor);
    }

    private static class Visitor extends AbstractCycleBreakerRecommendationsVisitor {
        private int solutionNumber = 1;

        protected Visitor(PrintStream out) {
            super(out);
        }

        @Override
        public void visitNoCycle() {
            append("Architecture doesn't contain cycles");
            append(lineSeparator());
        }

        @Override
        protected void startCycle(Cycle<?> currentCycle) {
            append("To break cycle (");
            append(currentCycle.stream()
                    .map(ArchElement::name)
                    .collect(joining(", ")));
            append(") try one of these solutions :");
            append(lineSeparator());
            solutionNumber = 1;
        }

        @Override
        protected void startSolution(Action... aSolution) {
            append("\t- solution ");
            append(Integer.toString(solutionNumber++));
            append(lineSeparator());
            Arrays.stream(aSolution).forEach(action -> {
                append("\t\t\t");
                append(action.getClass().getSimpleName());
                append(" : ");
                append(action.toString());
                append(lineSeparator());
            });
        }

        @Override
        protected void endCycle() {
            append(lineSeparator());
        }

        @Override
        protected void endCycleWithoutSolution() {
            append("No solution found");
            append(lineSeparator());
        }
    }
}
