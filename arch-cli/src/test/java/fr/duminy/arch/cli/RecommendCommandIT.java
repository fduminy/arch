package fr.duminy.arch.cli;

import fr.duminy.arch.api.ArchException;
import fr.duminy.arch.api.Architecture;
import fr.duminy.arch.java.JavaSourceCodeScanner;
import fr.duminy.arch.maven.MavenConfigurator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.FileSystem;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

import static fr.duminy.arch.cli.RecommendCommand.execute;
import static fr.duminy.arch.engine.ArchEngine.createSourceArchitectureBuilder;
import static java.lang.Integer.MAX_VALUE;
import static java.net.URI.create;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.nio.file.FileSystems.newFileSystem;
import static java.nio.file.FileVisitResult.CONTINUE;
import static java.nio.file.Files.*;
import static java.util.Collections.emptyMap;
import static java.util.Collections.emptySet;
import static org.assertj.core.api.Assertions.assertThat;

class RecommendCommandIT {
    @Test
    void break_cycles_jnode_fs(@TempDir Path newRootFolder) throws ArchException, URISyntaxException, IOException {
        Path jnodeMaven = copyZipContent(newRootFolder);
        Architecture architecture = createSourceArchitectureBuilder(jnodeMaven)
                .configureWith(new MavenConfigurator())
                .configureWith(new JavaSourceCodeScanner())
                .build();
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        try (PrintStream ps = new PrintStream(output, true, UTF_8)) {
            execute(true, false, architecture, ps);
        }
        assertThat(output).hasToString("""
                --- Recommendations at module level ---
                To break cycle (org.jnode.fs:org.jnode.driver.block, org.jnode.fs:org.jnode.fs) try one of these solutions :
                	- solution 1
                			MovePackage : org.jnode.driver.block->org.jnode.fs:org.jnode.fs
                	- solution 2
                			MoveClass : org.jnode.driver.block.JarFileDevice->org.jnode.fs
                	- solution 3
                			MoveClass : org.jnode.driver.block.JarFileDevice->org.jnode.fs.jarfs
                	- solution 4
                			MoveClass : org.jnode.driver.block.JarFileDevice->org.jnode.fs.service.def
                	- solution 5
                			MoveClass : org.jnode.driver.block.JarFileDevice->org.jnode.fs.spi
                	- solution 6
                			MoveClass : org.jnode.driver.block.JarFileDevice->org.jnode.fs.util
                	- solution 7
                			MoveClass : org.jnode.fs.ReadOnlyFileSystemException->org.jnode.driver.block
                               
                                """);
    }

    @SuppressWarnings("DataFlowIssue")
    private static Path copyZipContent(Path newRootFolder) throws URISyntaxException, IOException {
        String subDirectory = "jnode_maven";
        URI uri = create("jar:" + RecommendCommandIT.class.getResource("/mavenized_jnode_fs.zip").toURI());
        try (FileSystem zipFS = newFileSystem(uri, emptyMap())) {
            Path rootFolder = zipFS.getPath(subDirectory);

            walkFileTree(rootFolder, emptySet(), MAX_VALUE, new SimpleFileVisitor<>() {
                @Override
                public FileVisitResult visitFile(Path sourceFile, BasicFileAttributes attrs) throws IOException {
                    Path targetFile = newRootFolder.resolve(sourceFile.toString());
                    createDirectories(targetFile.getParent());
                    copy(sourceFile, targetFile);
                    return CONTINUE;
                }
            });
        }
        return newRootFolder.resolve(subDirectory);
    }
}