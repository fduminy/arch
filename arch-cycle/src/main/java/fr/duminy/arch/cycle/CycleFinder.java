package fr.duminy.arch.cycle;

import fr.duminy.arch.api.ArchElement;
import fr.duminy.arch.api.Cycle;
import org.jgrapht.Graph;
import org.jgrapht.alg.connectivity.KosarajuStrongConnectivityInspector;
import org.jgrapht.alg.interfaces.StrongConnectivityAlgorithm;
import org.jgrapht.graph.DefaultEdge;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import static java.util.Comparator.comparing;

class CycleFinder<T extends ArchElement> {
    private final Graph<T, DefaultEdge> graph;

    CycleFinder(Graph<T, DefaultEdge> graph) {
        this.graph = graph;
    }

    final Collection<Cycle<T>> findCycles() {
        List<Set<T>> foundCycles = new ArrayList<>();
        StrongConnectivityAlgorithm<T, DefaultEdge> inspector = new KosarajuStrongConnectivityInspector<>(graph);
        List<Set<T>> cycles = inspector.stronglyConnectedSets();
        for (Set<T> cycle : cycles) {
            if (cycle.size() < 2) {
                continue;
            }
            if (!containsSameCycle(foundCycles, cycle)) {
                foundCycles.add(cycle);
            }
        }

        return foundCycles.stream().map(this::createCycle).toList();
    }

    private boolean containsSameCycle(List<Set<T>> foundCycles, Set<T> cycle) {
        return foundCycles.stream()
                .filter(foundCycle -> cycle.size() == foundCycle.size())
                .filter(cycle::containsAll)
                .anyMatch(foundCycle -> foundCycle.containsAll(cycle));
    }

    private Cycle<T> createCycle(Set<T> cycle) {
        JGraphTCycle<T> orderedNodes = new JGraphTCycle<>(cycle.size());
        T currentNode = cycle.stream().min(comparing(ArchElement::name)).orElseThrow();
        do {
            orderedNodes.add(currentNode);
            cycle.remove(currentNode);
            if (cycle.isEmpty()) {
                break;
            }
            Set<DefaultEdge> edges = graph.edgesOf(currentNode);

            T finalCurrentNode = currentNode;
            currentNode = edges.stream()
                    .filter(e -> graph.getEdgeSource(e).equals(finalCurrentNode))
                    .filter(e -> cycle.contains(graph.getEdgeTarget(e)))
                    .findFirst()
                    .map(graph::getEdgeTarget)
                    .orElse(null);
        } while (currentNode != null);

        return orderedNodes;
    }

    static class JGraphTCycle<T extends ArchElement> extends ArrayList<T> implements Cycle<T> {
        public JGraphTCycle(int size) {
            super(size);
        }
    }
}
