package fr.duminy.arch.cycle;

import fr.duminy.arch.api.*;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DirectedPseudograph;

import java.util.Collection;
import java.util.Objects;
import java.util.function.Function;

import static java.util.function.Function.identity;

public final class JGraphTFactory implements GraphFactory {
    public fr.duminy.arch.api.Graph createGraph(Architecture architecture) {
        return new JGraphTGraph(architecture.database());
    }

    private record JGraphTGraph(ClassDatabase database) implements fr.duminy.arch.api.Graph {

        @Override
        public <T extends ArchElement> Collection<Cycle<T>> findCycles(Class<T> type) {
            Graph<T, DefaultEdge> graph = getGraph(type);
            return new CycleFinder<>(graph).findCycles();
        }

        @SuppressWarnings("unchecked")
        private <T extends ArchElement> Graph<T, DefaultEdge> getGraph(Class<T> type) {
            if (ArchClass.class.isAssignableFrom(type)) {
                return (Graph<T, DefaultEdge>) buildGraph(identity());
            }
            if (ArchPackage.class.isAssignableFrom(type)) {
                return (Graph<T, DefaultEdge>) buildGraph(database::packag);
            }
            if (ArchModule.class.isAssignableFrom(type)) {
                return (Graph<T, DefaultEdge>) buildGraph(database::module);
            }
            throw new IllegalArgumentException("Unsupported type : " + type);
        }


        <T extends ArchElement> Graph<T, DefaultEdge> buildGraph(Function<ArchClass, T> elementGetter) {
            Graph<T, DefaultEdge> graph = new DirectedPseudograph<>(DefaultEdge.class);
            database.classes().values().forEach(archClass -> addVertex(archClass, elementGetter, graph));
            return graph;
        }

        private <T extends ArchElement> void addVertex(ArchClass archClass,
                                                       Function<ArchClass, T> elementGetter,
                                                       Graph<T, DefaultEdge> graph) {
            T element = addVertex(elementGetter.apply(archClass), graph);
            database.forEachUsedClass(archClass, usedClass -> {
                T usedElement = addVertex(elementGetter.apply(usedClass), graph);
                if (!Objects.equals(element.name(), usedElement.name()) &&
                        !graph.containsEdge(element, usedElement)) {
                    graph.addEdge(element, usedElement);
                }
            });
        }

        private <T extends ArchElement> T addVertex(T element, Graph<T, DefaultEdge> graph) {
            if (!graph.vertexSet().contains(element)) {
                graph.addVertex(element);
            }
            return element;
        }
    }
}
