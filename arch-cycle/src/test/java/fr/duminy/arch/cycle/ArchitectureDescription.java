package fr.duminy.arch.cycle;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;

import static fr.duminy.arch.utils.ClassUtils.getPackageName;

public class ArchitectureDescription {
    private final Map<String, Set<String>> classToUsedClasses = new HashMap<>();
    private final Map<String, String> classToModule = new HashMap<>();
    private final Map<String, String> packageToModule = new HashMap<>();
    private final fr.duminy.arch.dsl.ArchitectureDescription architecture;

    static ArchitectureDescription parse(String architecture) {
        return new ArchitectureDescription(architecture);
    }

    private ArchitectureDescription(String architecture) {
        this.architecture = new fr.duminy.arch.dsl.ArchitectureDescription(architecture);
        this.architecture.getClassDependencies().forEach(classDependency -> {
            classToUsedClasses.put(classDependency.getClassName(), classDependency.getUsedClasses());
            classToModule.put(classDependency.getClassName(), classDependency.getModuleName());
            String packageName = getPackageName(classDependency.getClassName());
            packageToModule.put(packageName, classDependency.getModuleName());
        });
    }

    public void forEachUsedClass(BiConsumer<? super String, ? super Set<String>> action) {
        classToUsedClasses.forEach(action);
    }

    public String getModuleForClass(String className) {
        return classToModule.get(className);
    }

    public Collection<String> getModules() {
        return classToModule.values();
    }

    public String getModuleForPackage(String packageName) {
        return packageToModule.get(packageName);
    }

    @Override
    public String toString() {
        return architecture.toString();
    }
}
