package fr.duminy.arch.cycle;

import fr.duminy.arch.api.ArchClass;
import fr.duminy.arch.utils.ClassUtils;

import java.util.HashMap;
import java.util.Map;

import static fr.duminy.arch.utils.ClassUtils.getSimpleName;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;

public class Classes extends Elements<ArchClass> {
    private final Map<String, String> simpleNameToPackageName = new HashMap<>();

    public Classes() {
        super(ArchClass.class);
    }

    @Override
    public ArchClass getOrMock(String name) {
        String simpleName = getSimpleName(name);
        ArchClass element = nameToElement.get(simpleName);
        if (element == null) {
            element = mock(ArchClass.class, simpleName);
            lenient().when(element.id()).thenReturn(nameToElement.size());
            lenient().when(element.name()).thenReturn(simpleName);
            nameToElement.put(simpleName, element);
            simpleNameToPackageName.put(simpleName, ClassUtils.getPackageName(name));
        }
        return element;
    }

    public String getPackageName(String simpleName) {
        return simpleNameToPackageName.get(simpleName);
    }
}
