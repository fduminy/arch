package fr.duminy.arch.cycle;

import fr.duminy.arch.api.ArchElement;
import fr.duminy.arch.api.Cycle;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import static fr.duminy.arch.utils.ClassUtils.getSimpleName;
import static java.util.Arrays.stream;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

public class CyclesDescription<T extends ArchElement> extends Expectation {
    private final boolean hasCycle;
    private final Function<String, T> getElementByName;
    private List<Cycle<T>> expectedCycles;

    public CyclesDescription(String expectedCycles, Function<String, T> getElementByName) {
        super(expectedCycles);
        this.getElementByName = getElementByName;
        hasCycle = !expectedCycles.isEmpty();
    }

    protected final void parse() {
        expectedCycles = hasCycle ? parseExpectedCycles(expectation) : emptyList();
    }

    final public Iterable<? extends Cycle<T>> getExpectedCycles() {
        if (expectedCycles == null) {
            parse();
        }
        return expectedCycles;
    }

    final boolean hasCycle() {
        return hasCycle;
    }

    private List<Cycle<T>> parseExpectedCycles(String cyclesDef) {
        return stream(cyclesDef.split("\\|"))
                .map(String::trim)
                .filter(cycleDef -> !cycleDef.isEmpty())
                .map(this::parseExpectedCycle)
                .collect(toList());
    }

    private Cycle<T> parseExpectedCycle(String expectedCycle) {
        MockCycle<T> cycle = new MockCycle<>();
        for (String elementName : expectedCycle.split(",")) {
            cycle.add(getElementByName.apply(getSimpleName(elementName)));
        }
        return cycle;
    }

    public static class MockCycle<E extends ArchElement> extends ArrayList<E> implements Cycle<E> {
    }
}
