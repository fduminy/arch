package fr.duminy.arch.cycle;

import fr.duminy.arch.api.ArchElement;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;

public class Elements<T extends ArchElement> {
    final Map<String, T> nameToElement = new HashMap<>();
    private final Class<T> type;

    public Elements(Class<T> type) {
        this.type = type;
    }

    public T get(String name) {
        return nameToElement.get(name);
    }

    public T getOrMock(String name) {
        T element = nameToElement.get(name);
        if (element == null) {
            element = mock(type, name);
            lenient().when(element.id()).thenReturn(nameToElement.size());
            lenient().when(element.name()).thenReturn(name);
            nameToElement.put(name, element);
        }
        return element;
    }

    public Collection<T> elements() {
        return nameToElement.values();
    }
}
