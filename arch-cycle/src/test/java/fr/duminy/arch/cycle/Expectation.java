package fr.duminy.arch.cycle;

public abstract class Expectation {
    protected final String expectation;

    protected Expectation(String expectation) {
        this.expectation = expectation;
    }

    abstract protected void parse();
}
