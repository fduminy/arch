package fr.duminy.arch.cycle;

import fr.duminy.arch.api.*;
import org.assertj.core.api.SoftAssertions;
import org.assertj.core.api.junit.jupiter.SoftAssertionsExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;

import static java.util.Comparator.comparing;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.util.IterableUtil.toArray;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class JGraphTFactoryTest {
    @InjectMocks
    private JGraphTFactory underTest;

    @Test
    void findCycles_unsupported_class(@Mock Architecture architecture, @Mock ClassDatabase database, @Mock ArchCollection<ArchClass> classes) {
        class Unsupported implements ArchElement {
            @Override
            public int id() {
                return -1;
            }

            @Override
            public String name() {
                return null;
            }
        }
        when(architecture.database()).thenReturn(database);
        lenient().when(database.classes()).thenReturn(classes);

        Graph graph = underTest.createGraph(architecture);
        assertThatThrownBy(() -> graph.findCycles(Unsupported.class))
                .isExactlyInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining("Unsupported type : " + Unsupported.class);
    }

    @ParameterizedTest
    @ExtendWith(SoftAssertionsExtension.class)
    @ValueSource(strings = {
            "x.A => ",
            "x.A->x.B | x.B->x.A => x.A,x.B",
            "x.A->x.B | x.B->x.A,y.C | y.C->x.B => x.A,x.B,y.C",
            "x.A->x.B,y.C | y.C->x.A => x.A,y.C",
            "x.A->y.B | y.B->x.C | x.C->x.A | x.D->x.A | x.E->x.F | x.F->x.E => x.A,y.B,x.C | x.E,x.F"
    })
    @SuppressWarnings("java:S2970")
//assertAll called by SoftAssertionsExtension
    void findCycles_ArchClass(MockArchitectureForClass mockArchitecture, SoftAssertions softly) {
        Graph graph = underTest.createGraph(mockArchitecture.getMock());

        Collection<Cycle<ArchClass>> cycles = graph.findCycles(ArchClass.class);

        softly.assertThat(cycles).usingElementComparator(comparator(ArchClass.class))
                .containsExactlyInAnyOrderElementsOf(mockArchitecture.getExpectedCycles());
    }

    @ParameterizedTest
    @ExtendWith(SoftAssertionsExtension.class)
    @ValueSource(strings = {
            "x.A =>",
            "x.A->x.B | x.B->x.A =>",
            "x.A->x.B | x.B->x.A,y.C | y.C->x.B => x,y",
            "x.A->x.B,y.C | y.C->x.A => x,y",
            "x.A->y.B | y.B->x.C | x.C->x.A | x.D->x.A | x.E->x.F | x.F->x.E => x,y"
    })
    @SuppressWarnings("java:S2970")
//assertAll called by SoftAssertionsExtension
    void findCycles_ArchPackage(MockArchitectureForPackage mockArchitecture, SoftAssertions softly) {
        Graph graph = underTest.createGraph(mockArchitecture.getMock());

        Collection<Cycle<ArchPackage>> cycles = graph.findCycles(ArchPackage.class);

        softly.assertThat(cycles).usingElementComparator(comparator(ArchPackage.class))
                .containsExactlyInAnyOrderElementsOf(mockArchitecture.getExpectedCycles());
    }

    @ParameterizedTest
    @ExtendWith(SoftAssertionsExtension.class)
    @ValueSource(strings = {
            "m1:x.A =>",
            "m1:x.A->x.B | m1:x.B->x.A =>",
            "m1:x.A->x.B | m1:x.B->x.A,y.C | m2:y.C->x.B => m1,m2",
            "m1:x.A->x.B,y.C | m2:y.C->x.A => m1,m2",
            "m1:x.A->y.B | m2:y.B->x.C | m2:z.C->x.A | m2:u.D->x.A | m2:v.E->x.F | m2:w.F->x.E => m1,m2"
    })
    @SuppressWarnings("java:S2970")
//assertAll called by SoftAssertionsExtension
    void findCycles_ArchModule(MockArchitectureForModule mockArchitecture, SoftAssertions softly) {
        Graph graph = underTest.createGraph(mockArchitecture.getMock());

        Collection<Cycle<ArchModule>> cycles = graph.findCycles(ArchModule.class);

        softly.assertThat(cycles).usingElementComparator(comparator(ArchModule.class))
                .containsExactlyInAnyOrderElementsOf(mockArchitecture.getExpectedCycles());
    }

    private <T extends ArchElement> Comparator<Cycle<T>> comparator(Class<T> type) {
        return (o1, o2) -> Arrays.compare(toArray(o1, type), toArray(o2, type), comparing(ArchElement::name));
    }
}