package fr.duminy.arch.cycle;

import fr.duminy.arch.api.*;
import fr.duminy.arch.model.DefaultArchCollection;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;

import static fr.duminy.arch.cycle.ArchitectureDescription.parse;
import static fr.duminy.arch.utils.ClassUtils.getSimpleName;
import static org.mockito.Mockito.*;

public abstract class MockArchitecture<T extends ArchElement, E extends Expectation> {
    private static final int[] EMPTY = new int[0];

    private final Classes nameToClass = new Classes();
    private final Elements<ArchPackage> nameToPackage = new Elements<>(ArchPackage.class);
    private final Elements<ArchModule> nameToModule = new Elements<>(ArchModule.class);
    private final E expectation;
    private final ArchitectureDescription architectureDescription;

    protected MockArchitecture(String architecture) {
        String[] architectureDef = architecture.split("=>");
        architectureDescription = parse(architectureDef[0]);
        expectation = createExpectation((architectureDef.length > 1) ? architectureDef[1].trim() : "");
    }

    final E getExpectation() {
        return expectation;
    }

    final protected Elements<ArchClass> getNameToClass() {
        return nameToClass;
    }

    final public Elements<ArchPackage> getNameToPackage() {
        return nameToPackage;
    }

    final public Elements<ArchModule> getNameToModule() {
        return nameToModule;
    }

    final protected ArchitectureDescription getArchitectureDescription() {
        return architectureDescription;
    }

    protected abstract E createExpectation(String expectation);

    protected abstract void completeMocking(ClassDatabase database, String moduleName, String className, ArchClass archClass);

    protected abstract T getElementByName(String name);

    public final Architecture getMock() {
        ClassDatabase database = mock(ClassDatabase.class);
        mockElements(database);

        Architecture mock = mock(Architecture.class);
        lenient().when(mock.database()).thenReturn(database);
        lenient().doAnswer(invocation -> {
            ArchitectureVisitor visitor = invocation.getArgument(0);
            nameToClass.elements().forEach(visitor::visitClass);
            return null;
        }).when(mock).accept(any());
        expectation.parse();

        lenient().when(mock.database()).thenReturn(database);
        ArchCollection<ArchModule> modules = mockCollection(nameToModule);
        ArchCollection<ArchPackage> packages = mockCollection(nameToPackage);
        ArchCollection<ArchClass> classes = mockCollection(nameToClass);
        lenient().when(database.modules()).thenReturn(modules);
        lenient().when(database.packages()).thenReturn(packages);
        lenient().when(database.packages(any())).then(children(packages, ArchModule.class, database::module));
        lenient().when(database.packag(any())).then(args -> {
            ArchClass clazz = args.getArgument(0);
            String packageName = nameToClass.getPackageName(clazz.name());
            return nameToPackage.get(packageName);
        });
        lenient().when(database.module(any(ArchPackage.class))).then(args ->
                module(database, classes, args.getArgument(0)));
        lenient().when(database.module(any(ArchClass.class))).then(args -> {
            ArchClass clazz = args.getArgument(0);
            ArchPackage archPackage = database.packag(clazz);
            return database.module(archPackage);
        });
        lenient().when(database.classes(any())).then(children(classes, ArchPackage.class, database::packag));
        lenient().when(database.classes()).thenReturn(classes);
        mockClassDependencyIndexes(database);
        return mock;
    }

    public static void mockClassDependencyIndexes(ClassDatabase database) {
        lenient().when(database.classDependencyIndexes()).then(args -> {
            int[][] classDependencyIndexes = new int[database.classes().values().size()][];
            for (ArchClass clazz : database.classes().values()) {
                List<ArchClass> usedClasses = new ArrayList<>();
                database.forEachUsedClass(clazz, usedClasses::add);
                int[] usedClassIndexes = usedClasses.isEmpty() ? EMPTY : new int[usedClasses.size()];
                int i = 0;
                for (ArchClass usedClass : usedClasses) {
                    usedClassIndexes[i++] = usedClass.id();
                }
                classDependencyIndexes[clazz.id()] = usedClassIndexes;
            }
            return classDependencyIndexes;
        });
    }

    private static <C extends ArchElement, P extends ArchElement> Answer<List<C>> children(ArchCollection<C> allChildren,
                                                                                           Class<P> parentClass, Function<C, P> parentGetter) {
        return args -> {
            List<C> modulePackages = new ArrayList<>();
            P parent = args.getArgument(0, parentClass);
            allChildren.values().forEach(child -> {
                if (parentGetter.apply(child) == parent) {
                    modulePackages.add(child);
                }
            });
            return modulePackages;
        };
    }

    private ArchModule module(ClassDatabase database, ArchCollection<ArchClass> classes, ArchPackage packag) {
        final ArchModule[] module = {null};
        for (ArchClass clazz : classes.values()) {
            ArchPackage clazzPackage = database.packag(clazz);
            if (clazzPackage.id() == packag.id()) {
                String moduleName = architectureDescription.getModuleForPackage(clazzPackage.name());
                if (moduleName != null) {
                    module[0] = nameToModule.get(moduleName);
                    break;
                }
            }
        }
        return module[0];
    }

    private <A extends ArchElement> ArchCollection<A> mockCollection(Elements<A> nameToElement) {
        DefaultArchCollection<A> elements = new DefaultArchCollection<>(nameToElement.elements().size());
        for (A element : nameToElement.elements()) {
            elements.add(element);
        }
        return elements;
    }

    @SuppressWarnings("unchecked")
    private void mockElements(ClassDatabase database) {
        architectureDescription.forEachUsedClass((className, usedClasses) -> {
            ArchClass archClass = nameToClass.getOrMock(className);
            completeMocking(database, architectureDescription.getModuleForClass(className), className, archClass);
        });

        architectureDescription.forEachUsedClass((className, usedClasses) -> {
            ArchClass archClass = nameToClass.get(getSimpleName(className));
            lenient().doAnswer(args -> {
                Consumer<ArchClass> consumer = args.getArgument(1, Consumer.class);
                usedClasses.stream()
                        .map(name -> nameToClass.get(getSimpleName(name)))
                        .filter(Objects::nonNull)
                        .forEach(consumer);
                return null;
            }).when(database).forEachUsedClass(eq(archClass), any());
        });
    }
}
