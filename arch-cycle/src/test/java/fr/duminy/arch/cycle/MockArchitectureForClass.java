package fr.duminy.arch.cycle;

import fr.duminy.arch.api.ArchClass;
import fr.duminy.arch.api.ClassDatabase;

import static org.mockito.Mockito.when;

class MockArchitectureForClass extends MockArchitectureWithCycles<ArchClass> {
    @SuppressWarnings("unused") // called by junit 5
    public MockArchitectureForClass(String architecture) {
        super(architecture);
    }

    @Override
    protected void completeMocking(ClassDatabase database, String moduleName, String className, ArchClass archClass) {
        if (getExpectation().hasCycle()) {
            when(archClass.name()).thenReturn(className);
        }
    }

    @Override
    protected ArchClass getElementByName(String name) {
        return getNameToClass().get(name);
    }
}
