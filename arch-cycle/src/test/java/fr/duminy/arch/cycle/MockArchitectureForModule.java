package fr.duminy.arch.cycle;

import fr.duminy.arch.api.ArchClass;
import fr.duminy.arch.api.ArchModule;
import fr.duminy.arch.api.ArchPackage;
import fr.duminy.arch.api.ClassDatabase;

import static fr.duminy.arch.cycle.MockArchitectureForPackage.completePackageMocking;
import static org.mockito.Mockito.lenient;

public class MockArchitectureForModule extends MockArchitectureWithCycles<ArchModule> {
    @SuppressWarnings("unused") // called by junit 5
    public MockArchitectureForModule(String architecture) {
        super(architecture);
    }

    @Override
    protected void completeMocking(ClassDatabase database, String moduleName, String className, ArchClass archClass) {
        completeModuleMocking(database, getNameToModule(), getNameToPackage(), moduleName, className, archClass);
    }

    public static void completeModuleMocking(ClassDatabase database, Elements<ArchModule> nameToModule, Elements<ArchPackage> nameToPackage, String moduleName, String className, ArchClass archClass) {
        ArchModule archModule = nameToModule.getOrMock(moduleName);
        ArchPackage archPackage = completePackageMocking(database, nameToPackage, className, archClass);
        lenient().when(database.module(archPackage)).thenReturn(archModule);
    }

    @Override
    protected ArchModule getElementByName(String name) {
        return getNameToModule().get(name);
    }
}
