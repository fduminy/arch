package fr.duminy.arch.cycle;

import fr.duminy.arch.api.ArchClass;
import fr.duminy.arch.api.ArchPackage;
import fr.duminy.arch.api.ClassDatabase;

import static fr.duminy.arch.utils.ClassUtils.getPackageName;
import static org.mockito.Mockito.lenient;

public class MockArchitectureForPackage extends MockArchitectureWithCycles<ArchPackage> {
    @SuppressWarnings("unused") // called by junit 5
    public MockArchitectureForPackage(String architecture) {
        super(architecture);
    }

    @Override
    protected void completeMocking(ClassDatabase database, String moduleName, String className, ArchClass archClass) {
        completePackageMocking(database, getNameToPackage(), className, archClass);
    }

    public static ArchPackage completePackageMocking(ClassDatabase database, Elements<ArchPackage> nameToPackage, String className,
                                                     ArchClass archClass) {
        ArchPackage archPackage = nameToPackage.getOrMock(getPackageName(className));
        lenient().when(database.packag(archClass)).thenReturn(archPackage);
        return archPackage;
    }

    @Override
    protected ArchPackage getElementByName(String name) {
        return getNameToPackage().get(name);
    }
}
