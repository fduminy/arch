package fr.duminy.arch.cycle;

import fr.duminy.arch.api.ArchElement;
import fr.duminy.arch.api.Cycle;

abstract class MockArchitectureWithCycles<T extends ArchElement> extends MockArchitecture<T, CyclesDescription<T>> {
    MockArchitectureWithCycles(String architecture) {
        super(architecture);
    }

    @Override
    final protected CyclesDescription<T> createExpectation(String expectation) {
        return new CyclesDescription<>(expectation, this::getElementByName);
    }

    final Iterable<? extends Cycle<T>> getExpectedCycles() {
        return getExpectation().getExpectedCycles();
    }
}
