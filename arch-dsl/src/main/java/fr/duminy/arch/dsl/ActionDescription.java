package fr.duminy.arch.dsl;

public class ActionDescription {
    private static final String SEPARATOR = "->";

    private final String elementToMove;
    private final String target;

    private String action;

    public ActionDescription(String action) {
        String[] actionParts = action.split(SEPARATOR);
        elementToMove = actionParts[0].trim();
        target = actionParts[1].trim();
        if (action.length() == (elementToMove.length() + SEPARATOR.length() + target.length())) {
            this.action = action;
        }
    }

    public ActionDescription(String elementToMove, String target) {
        this.elementToMove = elementToMove.trim();
        this.target = target.trim();
    }

    public String getElementToMove() {
        return elementToMove;
    }

    public String getTarget() {
        return target;
    }

    @Override
    public String toString() {
        if (action == null) {
            action = elementToMove + SEPARATOR + target;
        }
        return action;
    }
}
