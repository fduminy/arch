package fr.duminy.arch.dsl;

import java.util.List;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.joining;

public class ArchitectureDescription {
    private static final String SEPARATOR = "|";
    private static final String SEPARATOR_REGEX = "\\" + SEPARATOR;
    private static final String SEPARATOR_TO_STRING = ' ' + SEPARATOR + ' ';

    private String architecture;
    private final List<ClassDependencyDescription> classDependencies;

    public ArchitectureDescription(String architecture) {
        classDependencies = stream(architecture.split(SEPARATOR_REGEX))
                .map(ClassDependencyDescription::new).toList();
    }

    @Override
    public String toString() {
        if (architecture == null) {
            architecture = classDependencies.stream()
                    .map(ClassDependencyDescription::toString)
                    .collect(joining(SEPARATOR_TO_STRING));
        }
        return architecture;
    }

    public List<ClassDependencyDescription> getClassDependencies() {
        return classDependencies;
    }
}
