package fr.duminy.arch.dsl;

import fr.duminy.arch.api.ArchClass;
import fr.duminy.arch.api.ClassDatabase;

import static fr.duminy.arch.dsl.ClassDependencyDescription.append;
import static java.util.Comparator.comparing;

public class ArchitectureUtils {
    private ArchitectureUtils() {
        // utility class
    }

    public static String toString(ClassDatabase database) {
        StringBuilder result = new StringBuilder();
        database.classes().values().stream()
                .sorted(comparing(ArchClass::name)
                        .thenComparing(archClass -> database.module(archClass).name()))
                .forEach(archClass -> {
                    if (!result.isEmpty()) {
                        result.append(" | ");
                    }
                    append(result, archClass, database);
                });
        return result.toString();
    }
}
