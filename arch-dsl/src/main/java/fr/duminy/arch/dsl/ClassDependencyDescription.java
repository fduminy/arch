package fr.duminy.arch.dsl;

import fr.duminy.arch.api.ArchClass;
import fr.duminy.arch.api.ClassDatabase;

import java.util.*;

import static fr.duminy.arch.api.Defaults.DEFAULT_MODULE;
import static fr.duminy.arch.utils.ClassUtils.*;
import static java.lang.String.join;
import static java.util.Collections.emptySet;

public class ClassDependencyDescription {
    private static final String DEPENDENCY_SEPARATOR = "->";
    private static final String CLASS_LIST_SEPARATOR = ",";
    private static final char MODULE_SEPARATOR = ':';

    private final String className;
    private final String moduleName;
    private final Set<String> usedClasses;

    private String classDependency;

    ClassDependencyDescription(String classDependency) {
        classDependency = classDependency.trim();
        String module = DEFAULT_MODULE.name();
        int index = classDependency.indexOf(MODULE_SEPARATOR);
        if (index > 0) {
            module = classDependency.substring(0, index);
            classDependency = classDependency.substring(index + 1);
        }
        moduleName = module.trim();

        index = classDependency.indexOf(DEPENDENCY_SEPARATOR);
        Set<String> dependencies = emptySet();
        String clazz;
        if (index < 0) {
            clazz = classDependency;
        } else {
            clazz = classDependency.substring(0, index);
            String[] classes = classDependency.substring(index + DEPENDENCY_SEPARATOR.length()).split(CLASS_LIST_SEPARATOR);
            dependencies = new LinkedHashSet<>(classes.length);
            for (String dependency : classes) {
                dependencies.add(dependency.trim());
            }
        }
        className = clazz.trim();
        usedClasses = dependencies;
    }

    public String getClassName() {
        return className;
    }

    public String getModuleName() {
        return moduleName;
    }

    public Set<String> getUsedClasses() {
        return usedClasses;
    }

    @Override
    public String toString() {
        if (classDependency == null) {
            StringBuilder builder = new StringBuilder();
            append(builder, moduleName, getPackageName(className), getSimpleName(className), usedClasses);
            classDependency = builder.toString();
        }
        return classDependency;
    }

    static void append(StringBuilder builder, ArchClass archClass, ClassDatabase database) {
        List<String> usedClasses = new ArrayList<>();
        database.forEachUsedClass(archClass, usedClass ->
                usedClasses.add(getFullQualifiedName(database.packag(usedClass), usedClass)));

        append(builder, database.module(archClass).name(), database.packag(archClass).name(),
                archClass.name(),
                usedClasses);
    }

    private static void append(StringBuilder builder, String moduleName, String packageName, String className,
                               Collection<String> usedClasses) {
        if (!moduleName.isEmpty()) {
            builder.append(moduleName).append(MODULE_SEPARATOR);
        }
        builder.append(packageName).append('.').append(className);
        if (!usedClasses.isEmpty()) {
            builder.append(DEPENDENCY_SEPARATOR)
                    .append(join(CLASS_LIST_SEPARATOR, usedClasses));
        }
    }
}
