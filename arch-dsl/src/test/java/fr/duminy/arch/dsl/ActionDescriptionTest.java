package fr.duminy.arch.dsl;

import org.assertj.core.api.SoftAssertions;
import org.assertj.core.api.junit.jupiter.SoftAssertionsExtension;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

@ExtendWith(SoftAssertionsExtension.class)
class ActionDescriptionTest {
    private static final String ELEMENT_TO_MOVE = "x.A";
    private static final String TARGET = "y";
    private static final String TO_STRING = ELEMENT_TO_MOVE + "->" + TARGET;

    @ParameterizedTest
    @CsvSource(value = {
            "x.A->y",

            // with leading and/or trailing whitespace
            " x.A->y",
            "x.A->y ",
            " x.A->y ",

            // with whitespace before separator and/or after separator
            "x.A ->y",
            "x.A-> y",
            "x.A -> y",
    })
    void constructorFromString(String descriptionAsString, SoftAssertions softly) {
        ActionDescription description = new ActionDescription(descriptionAsString);
        softly.assertThat(description.getElementToMove()).isEqualTo(ELEMENT_TO_MOVE);
        softly.assertThat(description.getTarget()).isEqualTo(TARGET);
        softly.assertThat(description).hasToString(TO_STRING);
    }

    @ParameterizedTest
    @CsvSource(value = {
            "x.A,y",

            // with whitespace before separator and/or after element to move
            " x.A,y",
            "x.A ,y",
            " x.A ,y",

            // with whitespace before separator and/or after target
            "x.A, y",
            "x.A,y ",
            "x.A, y ",
    }, ignoreLeadingAndTrailingWhitespace = false)
    void constructorFromParts(String elementToMove, String target, SoftAssertions softly) {
        ActionDescription description = new ActionDescription(elementToMove, target);
        softly.assertThat(description.getElementToMove()).isEqualTo(ELEMENT_TO_MOVE);
        softly.assertThat(description.getTarget()).isEqualTo(TARGET);
        softly.assertThat(description).hasToString(TO_STRING);
    }
}