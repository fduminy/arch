package fr.duminy.arch.dsl;

import org.assertj.core.api.SoftAssertions;
import org.assertj.core.api.junit.jupiter.SoftAssertionsExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static java.util.stream.Collectors.toList;

class ArchitectureDescriptionTest {
    @Test
    @ExtendWith(SoftAssertionsExtension.class)
    @SuppressWarnings({"java:S2970", "JUnitMalformedDeclaration"})
        //assertAll called by SoftAssertionsExtension
    void constructor(SoftAssertions softly) {
        ArchitectureDescription description = new ArchitectureDescription(" m1 : x.A -> y.B | m2 : z.C ");

        softly.assertThat(description).hasToString("m1:x.A->y.B | m2:z.C");
        softly.assertThat(description.getClassDependencies().stream().map(Object::toString).collect(toList()))
                .containsExactly("m1:x.A->y.B", "m2:z.C");
    }
}