package fr.duminy.arch.dsl;

import fr.duminy.arch.api.ArchClass;
import fr.duminy.arch.api.ArchModule;
import fr.duminy.arch.api.ArchPackage;
import fr.duminy.arch.api.ClassDatabase;
import org.assertj.core.api.SoftAssertions;
import org.assertj.core.api.junit.jupiter.SoftAssertionsExtension;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junitpioneer.jupiter.cartesian.CartesianTest;
import org.junitpioneer.jupiter.cartesian.CartesianTest.Values;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static fr.duminy.arch.utils.ClassUtils.*;
import static java.lang.String.join;
import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assumptions.assumeTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(SoftAssertionsExtension.class)
class ClassDependencyDescriptionTest {
    private static final String PACKAGE_NAME = "x";
    private static final String CLASS_NAME = "A";
    private static final String MODULE_NAME = "m1";
    private static final Set<String> USED_CLASSES = new HashSet<>(asList("y.B", "z.C"));

    @CartesianTest(name = "{0}{1}{2}")
    void constructor(@Values(strings = {"", MODULE_NAME}) String moduleName,
                     @Values(strings = {":", " :", ": ", " : "}) String moduleSeparator,
                     @Values(strings = {
                             "x.A->y.B,z.C",

                             // with leading and/or trailing whitespace
                             " x.A->y.B,z.C",
                             "x.A->y.B,z.C ",
                             " x.A->y.B,z.C ",

                             // with whitespace before arrow separator and/or after arrow separator
                             "x.A ->y.B,z.C",
                             "x.A-> y.B,z.C",
                             "x.A -> y.B,z.C",

                             // with whitespace before dependency separator and/or after dependency separator
                             "x.A->y.B ,z.C",
                             "x.A->y.B, z.C",
                             "x.A->y.B , z.C",
                     }) String descriptionString, SoftAssertions softly) {
        assumeTrue(!moduleName.isEmpty() || (moduleSeparator.length() == 1/*first separator*/));
        moduleSeparator = moduleName.isEmpty() ? "" : moduleSeparator;
        descriptionString = moduleName + moduleSeparator + descriptionString;
        ClassDependencyDescription description = new ClassDependencyDescription(descriptionString);
        softly.assertThat(description.getModuleName()).isEqualTo(moduleName);
        softly.assertThat(description.getClassName()).isEqualTo(getFullQualifiedName(PACKAGE_NAME, CLASS_NAME));
        softly.assertThat(description.getUsedClasses()).containsExactlyInAnyOrderElementsOf(USED_CLASSES);
        softly.assertThat(description).hasToString(moduleName +
                moduleSeparator.trim() + getFullQualifiedName(PACKAGE_NAME, CLASS_NAME) + "->" + join(",", USED_CLASSES));
    }

    @CartesianTest
    void append(@Values(strings = {"", "aPrefix_"}) String initialBuffer,
                @Values(booleans = {false, true}) boolean defaultModule) {
        StringBuilder buffer = new StringBuilder(initialBuffer);
        String expectedToString = initialBuffer +
                (defaultModule ? "" : (MODULE_NAME + ':')) + getFullQualifiedName(PACKAGE_NAME, CLASS_NAME) +
                "->" + join(",", USED_CLASSES);
        ArchModule archModule = mock(ArchModule.class);
        when(archModule.name()).thenReturn(defaultModule ? "" : MODULE_NAME);
        ArchPackage archPackage = mock(ArchPackage.class);
        when(archPackage.name()).thenReturn(PACKAGE_NAME);
        ClassDatabase database = mock(ClassDatabase.class);
        when(database.module(archPackage)).thenReturn(archModule);
        ArchClass archClass = mock(ArchClass.class);
        when(archClass.name()).thenReturn(CLASS_NAME);
        when(database.module(archClass)).thenReturn(archModule);
        List<ArchPackage> usedClassPackages = new ArrayList<>();
        List<ArchClass> usedClasses = USED_CLASSES.stream()
                .map(usedClassName -> {
                    ArchClass usedClass = mock(ArchClass.class);
                    when(usedClass.name()).thenReturn(getSimpleName(usedClassName));
                    ArchPackage usedClassPackage = mock(ArchPackage.class);
                    usedClassPackages.add(usedClassPackage);
                    when(usedClassPackage.name()).thenReturn(getPackageName(usedClassName));
                    return usedClass;
                }).toList();
        when(database.packag(any())).then(args -> {
            ArchClass clazz = args.getArgument(0);
            if (clazz == archClass) {
                return archPackage;
            }
            int index = usedClasses.indexOf(clazz);
            return (index < 0) ? null : usedClassPackages.get(index);
        });
        doAnswer(args -> {
            usedClasses.forEach(args.getArgument(1));
            return null;
        }).when(database).forEachUsedClass(eq(archClass), any());

        ClassDependencyDescription.append(buffer, archClass, database);

        assertThat(buffer).hasToString(expectedToString);
    }
}