package fr.duminy.arch.engine;

import fr.duminy.arch.api.ArchClass;
import fr.duminy.arch.api.Architecture;
import fr.duminy.arch.api.ClassDatabase;

import java.util.function.Function;

abstract class AbstractMove implements Action {
    private final Function<ClassDatabase, CloneClassDatabaseBuilder> builderFactory;

    AbstractMove(Function<ClassDatabase, CloneClassDatabaseBuilder> builderFactory) {
        this.builderFactory = builderFactory;
    }

    @Override
    final public Architecture transform(Architecture original) {
        CloneClassDatabaseBuilder builder = builderFactory.apply(original.database());
        original.database().classes().values().forEach(archClass -> {
            String moduleName = getNewModuleName(original, archClass);
            String packageName = getNewPackageName(original, archClass);
            builder.add(moduleName, packageName, archClass.id());
        });
        return new DefaultArchitecture(builder.build());
    }

    abstract String getNewModuleName(Architecture original, ArchClass archClass);

    abstract String getNewPackageName(Architecture original, ArchClass archClass);
}
