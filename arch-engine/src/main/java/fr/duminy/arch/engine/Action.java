package fr.duminy.arch.engine;

import fr.duminy.arch.api.ArchElement;
import fr.duminy.arch.api.ArchitectureTransformation;

public interface Action extends ArchitectureTransformation {

    ArchElement subject();
}
