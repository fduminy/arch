package fr.duminy.arch.engine;

import fr.duminy.arch.api.SourceArchitectureBuilder;

import java.nio.file.Path;

public class ArchEngine {
    private ArchEngine() {
    }

    public static SourceArchitectureBuilder createSourceArchitectureBuilder(Path rootFolder) {
        return new DefaultSourceArchitectureBuilder(rootFolder);
    }
}
