package fr.duminy.arch.engine;

import fr.duminy.arch.api.*;
import fr.duminy.arch.model.DefaultArchClass;
import fr.duminy.arch.model.DefaultArchCollection;
import fr.duminy.arch.model.DefaultArchModule;
import fr.duminy.arch.model.DefaultArchPackage;

import java.util.ArrayList;
import java.util.List;

import static fr.duminy.arch.engine.DefaultClassDatabase.*;

class CloneClassDatabaseBuilder {
    private ArchCollection<ArchClass> classes;
    private final ClassDatabase database;
    private ArchCollection<ArchModule> modules;
    private ArchCollection<ArchPackage> packages;
    private long[] classCodes;
    private List<ArchClass> updatedClasses;

    CloneClassDatabaseBuilder(ClassDatabase database) {
        this.database = database;
    }

    void add(String moduleName, String packageName, int classIndex) {
        if (modules == null) {
            modules = database.modules();
        }
        ArchModule module = modules.get(moduleName);
        if (module == null) {
            module = new DefaultArchModule(modules.values().size(), moduleName);
            modules = addElement(modules, module);
        }
        int moduleIndex = module.id();

        if (packages == null) {
            packages = database.packages();
        }
        ArchPackage packag = packages.get(packageName);
        if (packag == null) {
            packag = new DefaultArchPackage(packages.values().size(), packageName);
            packages = addElement(packages, packag);
        }
        int packageIndex = packag.id();

        if (classes == null) {
            classes = database.classes();
            classCodes = new long[classes.values().size()];
        }
        ArchClass clazz = database.classes().get(classIndex);
        if (!database.packag(clazz).equals(packag)) {
            clazz = new DefaultArchClass(clazz.id(), clazz.name());
            if (updatedClasses == null) {
                updatedClasses = new ArrayList<>();
            }
            updatedClasses.add(clazz);
        }

        long moduleCode = MODULE_MASK.getCode(moduleIndex);
        long packageCode = PACKAGE_MASK.getCode(packageIndex);
        long classCode = CLASS_MASK.getCode(clazz.id());
        classCodes[clazz.id()] = moduleCode | packageCode | classCode;
    }

    DefaultClassDatabase build() {
        if (updatedClasses != null) {
            classes = replaceElements(classes, updatedClasses);
        }
        return new DefaultClassDatabase(classCodes, database.classDependencyIndexes(), modules, packages, classes);
    }

    private <E extends ArchElement> ArchCollection<E> addElement(ArchCollection<E> elements, E element) {
        DefaultArchCollection<E> newElements = new DefaultArchCollection<>(elements.values().size() + 1);
        for (E e : elements.values()) {
            newElements.add(e);
        }
        newElements.add(element);
        return newElements;
    }

    private <E extends ArchElement> ArchCollection<E> replaceElements(ArchCollection<E> elements, List<E> updatedElements) {
        DefaultArchCollection<E> newElements = new DefaultArchCollection<>(elements.values().size());
        for (E e : elements.values()) {
            for (E updatedElement : updatedElements) {
                if (e.id() == updatedElement.id()) {
                    e = updatedElement;
                    break;
                }
            }
            newElements.add(e);
        }
        return newElements;
    }
}