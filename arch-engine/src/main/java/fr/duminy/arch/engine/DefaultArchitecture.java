package fr.duminy.arch.engine;

import fr.duminy.arch.api.Architecture;
import fr.duminy.arch.api.ArchitectureVisitor;
import fr.duminy.arch.api.ClassDatabase;

import java.util.Objects;

final class DefaultArchitecture implements Architecture {
    private final DefaultClassDatabase database;

    DefaultArchitecture(DefaultClassDatabase database) {
        this.database = database;
    }

    @Override
    public void accept(ArchitectureVisitor visitor) {
        database.classes().values().forEach(visitor::visitClass);
    }

    @Override
    public String toString() {
        return database.toString();
    }

    @Override
    public ClassDatabase database() {
        return database;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        var that = (DefaultArchitecture) obj;
        return Objects.equals(this.database, that.database);
    }

    @Override
    public int hashCode() {
        return Objects.hash(database);
    }
}
