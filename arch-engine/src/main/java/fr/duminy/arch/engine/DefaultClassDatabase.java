package fr.duminy.arch.engine;

import fr.duminy.arch.api.*;
import fr.duminy.arch.dsl.ArchitectureUtils;
import fr.duminy.arch.model.DefaultArchClass;
import fr.duminy.arch.model.DefaultArchCollection;
import fr.duminy.arch.model.DefaultArchModule;
import fr.duminy.arch.model.DefaultArchPackage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;

import static fr.duminy.arch.api.Defaults.DEFAULT_PACKAGE;
import static fr.duminy.arch.utils.ClassUtils.getSimpleName;
import static java.lang.reflect.Array.newInstance;
import static java.util.Collections.unmodifiableList;
import static java.util.Objects.requireNonNull;

final class DefaultClassDatabase implements ClassDatabase {
    static final Mask MODULE_MASK = new Mask(0xFFFF000000000000L);
    static final Mask PACKAGE_MASK = new Mask(0x0000FFFF00000000L);
    static final Mask CLASS_MASK = new Mask(0x00000000FFFFFFFFL);

    private final ArchCollection<ArchClass> classes;
    private final ArchCollection<ArchPackage> packages;
    private final ArchCollection<ArchModule> modules;
    private final long[] classCodes;
    private final int[][] classDependencyIndexes;

    private List<ArchPackage>[] moduleIndexToPackages;
    private ArchModule[] packageIndexToModule;
    private List<ArchClass>[] packageIndexToClasses;
    private String stringValue;

    DefaultClassDatabase(long[] classCodes, int[][] classDependencyIndexes, List<String> moduleNames, List<String> packageNames, List<String> classNames) {
        this(classCodes, classDependencyIndexes,
                collection(moduleNames, DefaultArchModule::new),
                collection(packageNames, (id, name) -> DEFAULT_PACKAGE.name().equals(name) ?
                        DEFAULT_PACKAGE : new DefaultArchPackage(id, name)),
                collection(classNames, (id, name) -> new DefaultArchClass(id, getSimpleName(name))));
    }

    DefaultClassDatabase(long[] classCodes, int[][] classDependencyIndexes, ArchCollection<ArchModule> modules, ArchCollection<ArchPackage> packages, ArchCollection<ArchClass> classes) {
        this.modules = requireNonNull(modules, "modules is null");
        this.packages = requireNonNull(packages, "packages is null");
        this.classes = requireNonNull(classes, "classes is null");
        this.classCodes = requireNonNull(classCodes, "classCodes is null");
        this.classDependencyIndexes = requireNonNull(classDependencyIndexes, "classDependencyIndexes is null");
    }

    @Override
    public ArchCollection<ArchClass> classes() {
        return classes;
    }

    @Override
    public List<ArchClass> classes(ArchPackage packag) {
        if (packageIndexToClasses == null) {
            packageIndexToClasses = children(this::packag, packages, classes);
        }
        return unmodifiableList(packageIndexToClasses[packag.id()]);
    }

    @Override
    public void forEachUsedClass(ArchClass archClass, Consumer<ArchClass> usedClassConsumer) {
        for (int usedClassIndex : classDependencyIndexes[archClass.id()]) {
            usedClassConsumer.accept(classes.get(usedClassIndex));
        }
    }

    @Override
    public int[][] classDependencyIndexes() {
        return classDependencyIndexes;
    }

    @Override
    public ArchCollection<ArchPackage> packages() {
        return packages;
    }

    @Override
    public List<ArchPackage> packages(ArchModule module) {
        if (moduleIndexToPackages == null) {
            moduleIndexToPackages = children(this::module, modules, packages);
        }
        return unmodifiableList(moduleIndexToPackages[module.id()]);
    }

    @Override
    public ArchPackage packag(ArchClass archClass) {
        long classCode = classCodes[archClass.id()];
        return packages.get(PACKAGE_MASK.getIndex(classCode));
    }

    @Override
    public ArchCollection<ArchModule> modules() {
        return modules;
    }

    @Override
    public ArchModule module(ArchPackage packag) {
        int packageIndex = packag.id();
        if (packageIndexToModule == null) {
            packageIndexToModule = new ArchModule[packages.values().size()];
            for (int currentPackageIndex = 0; currentPackageIndex < packages.values().size(); currentPackageIndex++) {
                int moduleIndex = -1;
                for (long classCode : classCodes) {
                    if (PACKAGE_MASK.getIndex(classCode) == currentPackageIndex) {
                        moduleIndex = MODULE_MASK.getIndex(classCode);
                        break;
                    }
                }
                if (moduleIndex >= 0) {
                    ArchModule module = modules.get(moduleIndex);
                    packageIndexToModule[currentPackageIndex] = module;
                }
            }
        }
        return packageIndexToModule[packageIndex];
    }

    @Override
    public ArchModule module(ArchClass archClass) {
        long classCode = classCodes[archClass.id()];
        return modules.get(MODULE_MASK.getIndex(classCode));
    }

    @Override
    public void invalidateCaches() {
        moduleIndexToPackages = null;
        packageIndexToModule = null;
        packageIndexToClasses = null;
        stringValue = null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DefaultClassDatabase that = (DefaultClassDatabase) o;
        return Arrays.equals(classCodes, that.classCodes);
    }


    @Override
    public int hashCode() {
        return Arrays.hashCode(classCodes);
    }

    @Override
    public String toString() {
        if (stringValue == null) {
            stringValue = ArchitectureUtils.toString(this);
        }
        return stringValue;
    }

    @SuppressWarnings("unchecked")
    private <C extends ArchElement, P extends ArchElement> List<C>[] children(Function<C, P> parent,
                                                                              ArchCollection<P> allParents,
                                                                              ArchCollection<C> allChildren) {
        List<C>[] parentIndexToChildren = (List<C>[]) newInstance(List.class, allParents.values().size());
        allChildren.values().forEach(archPackage -> {
            int parentIndex = parent.apply(archPackage).id();
            List<C> children = parentIndexToChildren[parentIndex];
            if (children == null) {
                children = new ArrayList<>();
                parentIndexToChildren[parentIndex] = children;
            }
            children.add(archPackage);
        });
        return parentIndexToChildren;
    }

    private static <T extends ArchElement> ArchCollection<T> collection(List<String> names,
                                                                        BiFunction<Integer, String, T> factory) {
        DefaultArchCollection<T> elements = new DefaultArchCollection<>(names.size());
        for (String name : names) {
            elements.add(factory.apply(elements.size(), name));
        }

        return elements;
    }
}
