package fr.duminy.arch.engine;

import fr.duminy.arch.api.*;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Integer.MAX_VALUE;
import static java.util.Arrays.asList;
import static java.util.Collections.unmodifiableCollection;
import static java.util.Comparator.comparingInt;

public final class DefaultSourceArchitectureBuilder implements SourceArchitectureBuilder {
    private static final List<Class<? extends Configurator>> EXECUTION_ORDER =
            asList(SourceFolderFinder.class, SourceCodeScanner.class);

    private final List<Configurator> configurators = new ArrayList<>();
    private final List<SourceFolder> sourceFolders = new ArrayList<>();
    private final Path rootFolder;
    private final SourceClassDatabaseBuilder builder = new SourceClassDatabaseBuilder();

    public DefaultSourceArchitectureBuilder(Path rootFolder) {
        this.rootFolder = rootFolder;
    }

    @Override
    public Path getRootFolder() {
        return rootFolder;
    }

    @Override
    public SourceArchitectureBuilder configureWith(Configurator configurator) {
        configurators.add(configurator);
        return this;
    }

    @Override
    public SourceArchitectureBuilder addSourceFolder(SourceFolder sourceFolder) {
        sourceFolders.add(sourceFolder);
        return this;
    }

    @Override
    public Iterable<SourceFolder> getSourceFolders() {
        return unmodifiableCollection(sourceFolders);
    }

    @Override
    public SourceArchitectureBuilder addClass(String moduleName, String packageName, String classSimpleName, String className, String... usedClassNames) {
        builder.add(moduleName, packageName, classSimpleName, className, usedClassNames);
        return this;
    }

    @Override
    public Architecture build() throws ArchException {
        configurators.sort(comparingInt(configurator -> {
            int index = MAX_VALUE; // unordered classes will be executed last
            for (int i = 0, execution_orderSize = EXECUTION_ORDER.size(); i < execution_orderSize; i++) {
                Class<?> clazz = EXECUTION_ORDER.get(i);
                if (clazz.isInstance(configurator)) {
                    index = i;
                    break;
                }
            }
            return index;
        }));
        for (Configurator configurator : configurators) {
            configurator.configure(this);
        }
        if (sourceFolders.isEmpty()) {
            throw new ArchException("no source folder");
        }
        if (builder.isEmpty()) {
            throw new ArchException("no classes");
        }
        DefaultClassDatabase database = builder.build();
        return new DefaultArchitecture(database);
    }
}
