package fr.duminy.arch.engine;

import static java.lang.String.format;

class Mask {
    private final long mask;
    private final int shift;
    private final int maxIndex;

    Mask(long mask) {
        this.mask = mask;
        int nbShift = 0;
        long value = 1;
        while ((value & mask) == 0) {
            value <<= 1;
            nbShift++;
        }
        this.shift = nbShift;
        int lastIndex = (int) (mask >>> shift);
        maxIndex = (lastIndex < 0) ? Integer.MAX_VALUE : lastIndex;
    }

    int getIndex(long code) {
        return (int) ((code & mask) >>> shift);
    }

    int maxIndex() {
        return maxIndex;
    }

    long getCode(int index) {
        if (index > maxIndex) {
            throw new IndexOutOfBoundsException(format("index too big for mask %s : 0x%X", this, index));
        }
        if (index < 0) {
            throw new IndexOutOfBoundsException(format("negative index : 0x%X", index));
        }
        return ((long) index) << shift;
    }

    @Override
    public String toString() {
        return format("Mask(0x%X)", mask);
    }
}
