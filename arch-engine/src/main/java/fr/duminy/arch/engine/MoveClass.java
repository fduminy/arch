package fr.duminy.arch.engine;

import fr.duminy.arch.api.*;
import fr.duminy.arch.dsl.ActionDescription;
import org.jheaps.annotations.VisibleForTesting;

import java.util.function.Function;

import static fr.duminy.arch.utils.ClassUtils.getFullQualifiedName;
import static java.util.Objects.requireNonNull;

public class MoveClass extends AbstractMove {
    private final ArchPackage packageOfClassToMove;
    private final ArchClass archClass;
    private final ArchPackage targetPackage;

    public MoveClass(ArchPackage packageOfClassToMove, ArchClass archClass, ArchPackage targetPackage) {
        this(CloneClassDatabaseBuilder::new, packageOfClassToMove, archClass, targetPackage);
    }

    @VisibleForTesting
    MoveClass(Function<ClassDatabase, CloneClassDatabaseBuilder> builderFactory, ArchPackage packageOfClassToMove, ArchClass archClass, ArchPackage targetPackage) {
        super(builderFactory);
        this.packageOfClassToMove = requireNonNull(packageOfClassToMove, "packageOfClassToMove is null");
        this.archClass = requireNonNull(archClass, "archClass is null");
        this.targetPackage = requireNonNull(targetPackage, "targetPackage is null");
    }

    @Override
    public ArchElement subject() {
        return archClass;
    }

    @Override
    public String toString() {
        return new ActionDescription(getFullQualifiedName(packageOfClassToMove, archClass), targetPackage.name()).toString();
    }

    @Override
    String getNewModuleName(Architecture original, ArchClass archClass) {
        ArchModule module = original.database().module(archClass);
        if (archClass == this.archClass) {
            ArchModule targetModule = original.database().module(targetPackage);
            if (targetModule != null) {
                module = targetModule;
            }
        }
        return module.name();
    }

    @Override
    String getNewPackageName(Architecture original, ArchClass archClass) {
        String packageName = original.database().packag(archClass).name();
        if (archClass == this.archClass) {
            packageName = targetPackage.name();
        }
        return packageName;
    }
}
