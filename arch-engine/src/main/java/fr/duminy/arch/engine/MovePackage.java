package fr.duminy.arch.engine;

import fr.duminy.arch.api.*;
import fr.duminy.arch.dsl.ActionDescription;
import org.jheaps.annotations.VisibleForTesting;

import java.util.function.Function;

import static java.util.Objects.requireNonNull;

public class MovePackage extends AbstractMove {
    private final ArchPackage archPackage;
    private final ArchModule targetModule;

    public MovePackage(ArchPackage archPackage, ArchModule targetModule) {
        this(CloneClassDatabaseBuilder::new, archPackage, targetModule);
    }

    @VisibleForTesting
    MovePackage(Function<ClassDatabase, CloneClassDatabaseBuilder> builderFactory, ArchPackage archPackage, ArchModule targetModule) {
        super(builderFactory);
        requireNonNull(archPackage, "archPackage is null");
        requireNonNull(targetModule, "targetModule is null");
        this.archPackage = archPackage;
        this.targetModule = targetModule;
    }

    @Override
    public ArchElement subject() {
        return archPackage;
    }

    @Override
    public String toString() {
        return new ActionDescription(archPackage.name(), targetModule.name()).toString();
    }

    @Override
    String getNewModuleName(Architecture original, ArchClass archClass) {
        ArchModule newModule = (original.database().packag(archClass) == archPackage) ? targetModule : original.database().module(archClass);
        return newModule.name();
    }

    @Override
    String getNewPackageName(Architecture original, ArchClass archClass) {
        return original.database().packag(archClass).name();
    }
}
