package fr.duminy.arch.engine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static fr.duminy.arch.api.Defaults.DEFAULT_MODULE;
import static fr.duminy.arch.api.Defaults.DEFAULT_PACKAGE;
import static fr.duminy.arch.engine.DefaultClassDatabase.*;
import static java.util.Comparator.comparing;
import static java.util.Comparator.naturalOrder;

class SourceClassDatabaseBuilder {
    static final int[] EMPTY = new int[0];

    private final List<ArchClassInfo> classes;

    SourceClassDatabaseBuilder() {
        classes = new ArrayList<>();
    }

    void add(String moduleName, String packageName, String classSimpleName, String className, String[] usedClassNames) {
        classes.add(new ArchClassInfo(moduleName, packageName, classSimpleName, className, usedClassNames));
    }

    DefaultClassDatabase build() {
        List<String> moduleNames = new ArrayList<>(classes.size()/*worst case*/);
        List<String> packageNames = new ArrayList<>(classes.size()/*worst case*/);
        List<String> classNames = new ArrayList<>(classes.size()/*worst case*/);
        classes.sort(comparing(classInfo -> classInfo.className));
        for (ArchClassInfo clazz : classes) {
            if (!moduleNames.contains(clazz.moduleName)) {
                moduleNames.add(clazz.moduleName);
            }
            if (!packageNames.contains(clazz.packageName)) {
                packageNames.add(clazz.packageName);
            }
            classNames.add(clazz.className);
        }
        moduleNames.sort(naturalOrder());
        packageNames.sort(naturalOrder());

        Map<String, Long> moduleNameToCode = new HashMap<>(moduleNames.size(), 1);
        for (int moduleIndex = 0; moduleIndex < moduleNames.size(); moduleIndex++) {
            moduleNameToCode.put(moduleNames.get(moduleIndex), MODULE_MASK.getCode(moduleIndex));
        }

        Map<String, Long> packageNameToCode = new HashMap<>(packageNames.size(), 1);
        for (int packageIndex = 0; packageIndex < packageNames.size(); packageIndex++) {
            packageNameToCode.put(packageNames.get(packageIndex), PACKAGE_MASK.getCode(packageIndex));
        }

        long[] classCodes = new long[classes.size()];
        for (int classIndex = 0; classIndex < classes.size(); classIndex++) {
            ArchClassInfo clazz = classes.get(classIndex);
            long moduleCode = moduleNameToCode.get(clazz.moduleName);
            long packageCode = packageNameToCode.get(clazz.packageName);
            long classCode = CLASS_MASK.getCode(classIndex);
            classCodes[classIndex] = moduleCode | packageCode | classCode;
        }

        int[][] classDependencyIndexes = new int[classes.size()][];
        for (int classIndex = 0; classIndex < classes.size(); classIndex++) {
            ArchClassInfo clazz = classes.get(classIndex);
            classDependencyIndexes[classIndex] = (clazz.usedClassNames.length == 0) ? EMPTY : new int[clazz.usedClassNames.length];
            for (int i = 0; i < clazz.usedClassNames.length; i++) {
                classDependencyIndexes[classIndex][i] = findClassIndexByName(classes, clazz.usedClassNames[i]);
            }
        }

        return new DefaultClassDatabase(classCodes, classDependencyIndexes, moduleNames, packageNames, classNames);
    }

    private int findClassIndexByName(List<ArchClassInfo> archClassInfos, String className) {
        int classDependencyIndex = -1;
        for (int i = 0; i < archClassInfos.size(); i++) {
            if (archClassInfos.get(i).className.equals(className)) {
                classDependencyIndex = i;
                break;
            }
        }
        return classDependencyIndex;
    }

    public boolean isEmpty() {
        return classes.isEmpty();
    }

    static class ArchClassInfo {
        final String moduleName;
        final String packageName;
        final String className;
        final String[] usedClassNames;

        ArchClassInfo(String moduleName, String packageName, String classSimpleName, String className, String... usedClassNames) {
            this.moduleName = (moduleName == null) || moduleName.isBlank() ? DEFAULT_MODULE.name() : moduleName;
            if ((packageName == null) || packageName.isBlank()) {
                this.packageName = DEFAULT_PACKAGE.name();
                this.className = classSimpleName;
            } else {
                this.packageName = packageName;
                this.className = className;
            }
            this.usedClassNames = usedClassNames;
        }
    }
}
