package fr.duminy.arch.engine;

import fr.duminy.arch.api.Architecture;
import fr.duminy.arch.api.ClassDatabase;
import fr.duminy.arch.dsl.ClassDependencyDescription;
import org.assertj.core.api.SoftAssertions;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import static fr.duminy.arch.utils.ClassUtils.getPackageName;
import static fr.duminy.arch.utils.ClassUtils.getSimpleName;
import static java.util.Arrays.asList;
import static java.util.Comparator.comparing;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

abstract class AbstractMoveTest {
    final void transform(MockArchitectureWithChanges mockArchitecture, CloneClassDatabaseBuilder builder,
                         SoftAssertions softly,
                         Function<Function<ClassDatabase, CloneClassDatabaseBuilder>, AbstractMove> moveFactory) {
        Architecture original = mockArchitecture.getMock();
        List<List<Object>> actual = new ArrayList<>();
        doAnswer(args -> {
            String moduleName = args.getArgument(0);
            String packageName = args.getArgument(1);
            int classIndex = args.getArgument(2);
            actual.add(new ArrayList<>(asList(moduleName, packageName, classIndex)));
            return null;
        }).when(builder).add(any(), any(), anyInt());
        DefaultClassDatabase expectedDatabase = mock(DefaultClassDatabase.class);
        when(builder.build()).thenReturn(expectedDatabase);
        AbstractMove underTest = moveFactory.apply(database -> builder);

        Architecture transformed = underTest.transform(original);

        softly.assertThat(transformed).extracting(Architecture::database).isSameAs(expectedDatabase);
        List<ClassDependencyDescription> classDependencies = mockArchitecture.getExpectedClasses();
        List<List<Object>> expected = new ArrayList<>();
        for (ClassDependencyDescription d : classDependencies) {
            int classId = original.database().classes().values().stream()
                    .filter(c -> getSimpleName(c.name()).equals(getSimpleName(d.getClassName())))
                    .findFirst().orElseThrow().id();
            expected.add(asList(d.getModuleName(), getPackageName(d.getClassName()), classId));
        }
        expected.sort(comparing(a -> (Integer) a.get(2)));
        softly.assertThat(actual).usingRecursiveFieldByFieldElementComparator().containsExactlyElementsOf(expected);
    }
}