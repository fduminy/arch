package fr.duminy.arch.engine;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.nio.file.Path;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class ArchEngineTest {
    @Test
    void createSourceArchitectureBuilder(@Mock Path rootFolder) {
        assertThat(ArchEngine.createSourceArchitectureBuilder(rootFolder))
                .isExactlyInstanceOf(DefaultSourceArchitectureBuilder.class);
    }
}