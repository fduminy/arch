package fr.duminy.arch.engine;

import fr.duminy.arch.api.ArchClass;
import fr.duminy.arch.api.ArchModule;
import fr.duminy.arch.api.ArchPackage;
import fr.duminy.arch.api.ClassDatabase;
import fr.duminy.arch.dsl.ClassDependencyDescription;
import fr.duminy.arch.model.DefaultArchClass;
import fr.duminy.arch.model.DefaultArchCollection;
import fr.duminy.arch.model.DefaultArchModule;
import fr.duminy.arch.model.DefaultArchPackage;
import org.assertj.core.api.SoftAssertions;
import org.assertj.core.api.junit.jupiter.SoftAssertionsExtension;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static fr.duminy.arch.cycle.MockArchitecture.mockClassDependencyIndexes;
import static fr.duminy.arch.utils.ClassUtils.getPackageName;
import static fr.duminy.arch.utils.ClassUtils.getSimpleName;
import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.params.provider.Arguments.of;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith({MockitoExtension.class, SoftAssertionsExtension.class})
class CloneClassDatabaseBuilderTest {
    @Mock
    private ClassDatabase oldDatabase;
    @InjectMocks
    private CloneClassDatabaseBuilder underTest;

    private static Stream<Arguments> architectures() {
        return Stream.of(
                // single class
                of("m1:x.A"),

                // with dependency in same module, in same package
                of("m1:x.A->x.B | m1:x.B"),
                of("m1:x.A->x.B | m1:x.B->x.A"),
                of("m1:x.B->x.A | m1:x.A->x.B"), // similar as above but classes declared is reverse alphabetical order of their name

                // with dependency in same module, in different package
                of("m1:x.A->y.B | m1:y.B"),
                of("m1:x.A->y.B | m1:y.B->x.A"),
                of("m1:y.A->x.B | m1:x.B->y.A"), // similar as above but classes declared is reverse alphabetical order of their package name

                // with dependency in different module, in different package
                of("m1:x.A->y.B | m2:y.B"),
                of("m1:x.A->y.B | m2:y.B->x.A"),
                of("m2:x.A->y.B | m1:y.B->x.A"), // similar as above but classes declared is reverse alphabetical order of their module name

                // with default package
                of("m1:A"),
                of("m1:A->B | m1:B"),
                of("m1:A->B | m1:B->A"),
                of("m1:A->y.B | m1:y.B->A"), // similar as above but one class is not in default module
                of("m1:B->A | m1:A->B") // similar as above but classes declared is reverse alphabetical order of their name
        );
    }

    @ParameterizedTest
    @MethodSource("architectures")
    void classes(ArchitectureDescription architecture) {
        setup(architecture);

        DefaultClassDatabase newDatabase = underTest.build();

        assertThat(newDatabase.classes().values()).usingRecursiveFieldByFieldElementComparator()
                .containsExactlyElementsOf(architecture.classes());
    }

    @ParameterizedTest
    @MethodSource("architectures")
    void classesOfPackage(ArchitectureDescription architecture, SoftAssertions softly) {
        setup(architecture);

        DefaultClassDatabase newDatabase = underTest.build();

        for (ArchPackage packag : oldDatabase.packages().values()) {
            softly.assertThat(newDatabase.classes(packag)).usingRecursiveFieldByFieldElementComparator()
                    .as("package=%s", packag)
                    .containsExactlyElementsOf(architecture.classes().stream()
                            .filter(clazz -> {
                                String packageName = architecture.getClassDependencies().stream()
                                        .filter(desc -> getSimpleName(desc.getClassName()).equals(clazz.name()))
                                        .map(desc -> getPackageName(desc.getClassName()))
                                        .findFirst().orElseThrow(AssertionError::new);
                                return packag.name().equals(packageName);
                            })
                            .toList());
        }
    }

    @ParameterizedTest
    @MethodSource("architectures")
    void packages(ArchitectureDescription architecture) {
        setup(architecture);

        DefaultClassDatabase newDatabase = underTest.build();

        assertThat(newDatabase.packages()).isSameAs(oldDatabase.packages());
    }

    @SuppressWarnings("ComparatorMethodParameterNotUsed")
    @ParameterizedTest
    @MethodSource("architectures")
    void packagesOfModule(ArchitectureDescription architecture, SoftAssertions softly) {
        setup(architecture);

        DefaultClassDatabase newDatabase = underTest.build();

        for (ArchModule module : oldDatabase.modules().values()) {
            softly.assertThat(newDatabase.packages(module)).usingElementComparator((o1, o2) -> o1 == o2 ? 0 : -1)
                    .as("module=%s", module)
                    .containsExactlyElementsOf(architecture.getClassDependencies().stream()
                            .filter(classDef -> module.name().equals(classDef.getModuleName()))
                            .map(classDef -> getPackageName(classDef.getClassName()))
                            .sorted()
                            .flatMap(packageName -> oldDatabase.packages().values().stream().filter(pack -> pack.name().equals(packageName)))
                            .distinct()
                            .collect(toList()));
        }
    }

    @ParameterizedTest
    @MethodSource("architectures")
    void packageOfClass(ArchitectureDescription architecture, SoftAssertions softly) {
        setup(architecture);

        DefaultClassDatabase newDatabase = underTest.build();

        for (ArchClass clazz : architecture.classes()) {
            softly.assertThat(newDatabase.packag(clazz))
                    .as("clazz=%s", clazz)
                    .isSameAs(architecture.getClassDependencies().stream()
                            .filter(classDef -> clazz.name().equals(getSimpleName(classDef.getClassName())))
                            .map(classDef -> getPackageName(classDef.getClassName()))
                            .flatMap(packageName -> oldDatabase.packages().values().stream().filter(pack -> pack.name().equals(packageName)))
                            .findFirst().orElseThrow(AssertionError::new));
        }
    }

    @ParameterizedTest
    @MethodSource("architectures")
    void modules(ArchitectureDescription architecture) {
        setup(architecture);

        DefaultClassDatabase newDatabase = underTest.build();

        assertThat(newDatabase.modules()).isSameAs(oldDatabase.modules());
    }

    @ParameterizedTest
    @MethodSource("architectures")
    void moduleOfPackage(ArchitectureDescription architecture, SoftAssertions softly) {
        setup(architecture);

        DefaultClassDatabase newDatabase = underTest.build();

        for (ArchPackage packag : oldDatabase.packages().values()) {
            softly.assertThat(newDatabase.module(packag))
                    .as("packag=%s", packag)
                    .isSameAs(module(classDef -> packag.name().equals(getPackageName(classDef.getClassName())), architecture));
        }
    }

    @ParameterizedTest
    @MethodSource("architectures")
    void moduleOfClass(ArchitectureDescription architecture, SoftAssertions softly) {
        setup(architecture);

        DefaultClassDatabase newDatabase = underTest.build();

        for (ArchClass clazz : architecture.classes()) {
            softly.assertThat(newDatabase.module(clazz))
                    .as("clazz=%s", clazz)
                    .isSameAs(module(classDef -> clazz.name().equals(getSimpleName(classDef.getClassName())), architecture));
        }
    }

    private void setup(ArchitectureDescription architecture) {
        DefaultArchCollection<ArchModule> modules = new DefaultArchCollection<>(architecture.classes.size());
        DefaultArchCollection<ArchPackage> packages = new DefaultArchCollection<>(architecture.classes.size());
        DefaultArchCollection<ArchClass> classes = new DefaultArchCollection<>(architecture.classes.size());
        List<ClassDependencyDescription> classDependencies = new ArrayList<>(architecture.getClassDependencies());
        classDependencies.sort(comparing(ClassDependencyDescription::getClassName));
        Map<ArchClass, ArchPackage> classToPackage = new HashMap<>();
        for (int i = 0; i < classDependencies.size(); i++) {
            ClassDependencyDescription description = classDependencies.get(i);
            modules.add(new DefaultArchModule(i, description.getModuleName()));
            DefaultArchPackage packag = new DefaultArchPackage(i, getPackageName(description.getClassName()));
            packages.add(packag);
            DefaultArchClass clazz = new DefaultArchClass(i, getSimpleName(description.getClassName()));
            classes.add(clazz);
            classToPackage.put(clazz, packag);
        }
        when(oldDatabase.modules()).thenReturn(modules);
        when(oldDatabase.packages()).thenReturn(packages);
        when(oldDatabase.packag(any())).then(args ->
                classToPackage.get(args.getArgument(0, ArchClass.class)));
        when(oldDatabase.classes()).thenReturn(classes);
        mockClassDependencyIndexes(oldDatabase);

        for (int i = 0; i < classDependencies.size(); i++) {
            ClassDependencyDescription clazz = classDependencies.get(i);
            String clazzName = clazz.getClassName();
            underTest.add(clazz.getModuleName(), getPackageName(clazzName), i);
        }
    }

    private ArchModule module(Predicate<ClassDependencyDescription> filter, ArchitectureDescription architecture) {
        return architecture.getClassDependencies().stream()
                .filter(filter)
                .map(ClassDependencyDescription::getModuleName)
                .flatMap(moduleName -> oldDatabase.modules().values().stream().filter(module -> module.name().equals(moduleName)))
                .findFirst().orElseThrow(AssertionError::new);
    }

    static class ArchitectureDescription extends fr.duminy.arch.dsl.ArchitectureDescription {
        private final List<ArchClass> classes;

        @SuppressWarnings("unused")
            // called by junit 5
        ArchitectureDescription(String architecture) {
            super(architecture);
            classes = buildClasses();
        }

        public List<ArchClass> classes() {
            return classes;
        }

        private List<ArchClass> buildClasses() {
            List<ClassDependencyDescription> classes = new ArrayList<>(getClassDependencies());
            classes.sort(comparing(ClassDependencyDescription::getClassName));
            List<ArchClass> result = new ArrayList<>();
            for (ClassDependencyDescription clazz : classes) {
                String className = clazz.getClassName();
                DefaultArchClass archClass = new DefaultArchClass(result.size(), getSimpleName(className));
                result.add(archClass);
            }
            return result;
        }
    }
}