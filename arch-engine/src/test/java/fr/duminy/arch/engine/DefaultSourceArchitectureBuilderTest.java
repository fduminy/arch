package fr.duminy.arch.engine;

import fr.duminy.arch.api.*;
import fr.duminy.arch.engine.SourceClassDatabaseBuilder.ArchClassInfo;
import org.assertj.core.api.SoftAssertions;
import org.assertj.core.api.junit.jupiter.SoftAssertionsExtension;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static fr.duminy.arch.api.Defaults.DEFAULT_PACKAGE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.InstanceOfAssertFactories.list;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class DefaultSourceArchitectureBuilderTest {
    @InjectMocks
    private DefaultSourceArchitectureBuilder underTest;
    @Mock
    private Path rootFolder;

    @Nested
    class WhenConstructing {
        @Test
        void then_save_root_folder() {
            assertThat(underTest.getRootFolder()).isSameAs(rootFolder);
        }
    }

    @Nested
    class WhenConfiguringWith {
        @Mock
        private Configurator configurator;

        @Test
        void no_interactions_with_configurator() {
            underTest.configureWith(configurator);
            verifyNoInteractions(configurator);
        }

        @Test
        void then_return_self() {
            assertThat(underTest.configureWith(configurator)).isSameAs(underTest);
        }
    }

    @Nested
    @ExtendWith(SoftAssertionsExtension.class)
    class WhenAddingSourceFolder {
        @Mock
        private SourceFolder sourceFolder1;
        @Mock
        private SourceFolder sourceFolder2;

        @Test
        void then_return_self(SoftAssertions softly) {
            softly.assertThat(underTest.addSourceFolder(sourceFolder1)).isSameAs(underTest);
            softly.assertThat(underTest.addSourceFolder(sourceFolder2)).isSameAs(underTest);
            softly.assertThat(underTest.getSourceFolders()).containsExactly(sourceFolder1, sourceFolder2);
        }
    }

    @Nested
    @ExtendWith(SoftAssertionsExtension.class)
    class WhenAddingClass {
        @Test
        void then_return_self(@Mock SourceFolder sourceFolder1, @Mock SourceFolder sourceFolder2, SoftAssertions softly) {
            softly.assertThat(underTest.addClass(sourceFolder1.moduleName(), "package1", "Class1", "package1.Class1")).isSameAs(underTest);
            softly.assertThat(underTest.addClass(sourceFolder2.moduleName(), "package2", "Class2", "package2.Class2", "package3.Class3", "package4.Class4")).isSameAs(underTest);
            softly.assertThat(underTest)
                    .extracting("builder.classes", list(ArchClassInfo.class))
                    .usingRecursiveFieldByFieldElementComparator()
                    .containsExactlyInAnyOrder(new ArchClassInfo(sourceFolder1.moduleName(), "package1", "Class1", "package1.Class1"),
                            new ArchClassInfo(sourceFolder2.moduleName(), "package2", "Class2", "package2.Class2", "package3.Class3", "package4.Class4"));
        }
    }

    @SuppressWarnings("JUnitMalformedDeclaration")
    @Nested
    class WhenBuilding {
        @Test
        void without_configuration_then_throw_an_exception() {
            assertThatThrownBy(() -> underTest.build())
                    .isInstanceOf(ArchException.class).hasMessage("no source folder");
        }

        @Test
        void with_configurators_doing_nothing_then_throw_an_exception(@Mock Configurator configurator) {
            underTest.configureWith(configurator);
            assertThatThrownBy(() -> underTest.build())
                    .isInstanceOf(ArchException.class).hasMessage("no source folder");
        }

        @Test
        void without_source_folder_then_throw_an_exception() {
            underTest.addClass(null, null, null, "class1");
            assertThatThrownBy(() -> underTest.build())
                    .isInstanceOf(ArchException.class).hasMessage("no source folder");
        }

        @Test
        void without_class_then_throw_an_exception(@Mock SourceFolder sourceFolder) {
            underTest.addSourceFolder(sourceFolder);
            assertThatThrownBy(() -> underTest.build())
                    .isInstanceOf(ArchException.class).hasMessage("no classes");
        }

        @ParameterizedTest
        @ValueSource(booleans = {false, true})
        void then_execute_configurators_in_good_order(boolean addInGoodOrder) throws ArchException {
            SourceFolderFinder sourceFolderFinder = mock(SourceFolderFinder.class);
            doAnswer(invocation -> {
                underTest.addSourceFolder(mock(SourceFolder.class));
                return null;
            }).when(sourceFolderFinder).configure(underTest);
            SourceCodeScanner sourceCodeScanner = mock(SourceCodeScanner.class);
            doAnswer(invocation -> {
                underTest.addClass("module", null, "Class1", "Class1");
                return null;
            }).when(sourceCodeScanner).configure(underTest);
            if (addInGoodOrder) {
                underTest.configureWith(sourceFolderFinder).configureWith(sourceCodeScanner);
            } else {
                underTest.configureWith(sourceCodeScanner).configureWith(sourceFolderFinder);
            }

            assertThat(underTest.build()).isNotNull();
            InOrder inOrder = inOrder(sourceCodeScanner, sourceFolderFinder);
            inOrder.verify(sourceFolderFinder).configure(underTest);
            inOrder.verify(sourceCodeScanner).configure(underTest);
        }

        @Test
        @ExtendWith(SoftAssertionsExtension.class)
        @SuppressWarnings("java:S2970")
//assertAll called by SoftAssertionsExtension
        void then_return_a_built_architecture(@Mock SourceFolder sourceFolder,
                                              SoftAssertions softly) throws ArchException {
            underTest.addSourceFolder(sourceFolder)
                    .addClass("module1", "package1", "Class1", "package1.Class1")
                    .addClass("module2", "package2", "Class2", "package2.Class2");

            Architecture architecture = underTest.build();

            softly.assertThat(architecture).isNotNull();
            if (softly.wasSuccess()) {
                Collection<ArchClass> classes = architecture.database().classes().values();
                softly.assertThat(classes).extracting(ArchClass::name)
                        .containsExactly("Class1", "Class2");
                softly.assertThat(classes).extracting(archClass -> architecture.database().packag(archClass))
                        .extracting(ArchPackage::name)
                        .containsExactly("package1", "package2");
                softly.assertThat(classes).extracting(archClass -> architecture.database().packag(archClass))
                        .extracting(pack -> architecture.database().module(pack))
                        .extracting(ArchModule::name)
                        .containsExactly("module1", "module2");
            }
        }

        @Test
        @ExtendWith(SoftAssertionsExtension.class)
        @SuppressWarnings("java:S2970")
//assertAll called by SoftAssertionsExtension
        void then_return_a_built_architecture_with_shared_package(@Mock SourceFolder sourceFolder,
                                                                  SoftAssertions softly) throws ArchException {
            when(sourceFolder.moduleName()).thenReturn("module1");
            underTest.addSourceFolder(sourceFolder)
                    .addClass(sourceFolder.moduleName(), "package1", "Class1", "package1.Class1")
                    .addClass(sourceFolder.moduleName(), "package1", "Class2", "package1.Class2");

            Architecture architecture = underTest.build();

            softly.assertThat(architecture).isNotNull();
            if (softly.wasSuccess()) {
                List<ArchClass> classes = new ArrayList<>(architecture.database().classes().values());
                softly.assertThat(classes).extracting(ArchClass::name)
                        .containsExactly("Class1", "Class2");
                softly.assertThat(classes).extracting(archClass -> architecture.database().packag(archClass))
                        .extracting(ArchPackage::name)
                        .containsExactly("package1", "package1");
                softly.assertThat(classes).extracting(archClass -> architecture.database().packag(archClass))
                        .extracting(pack -> architecture.database().module(pack))
                        .extracting(ArchModule::name)
                        .containsExactly("module1", "module1");
                softly.assertThat(architecture.database().packag(classes.get(0)))
                        .isSameAs(architecture.database().packag(classes.get(1)));
            }
        }

        @Test
        @ExtendWith(SoftAssertionsExtension.class)
        @SuppressWarnings("java:S2970")
//assertAll called by SoftAssertionsExtension
        void then_return_a_built_architecture_with_default_package(@Mock SourceFolder sourceFolder, SoftAssertions softly) throws ArchException {
            underTest.addSourceFolder(sourceFolder).addClass(sourceFolder.moduleName(), null, "Class3", "Class3");

            Architecture architecture = underTest.build();

            softly.assertThat(architecture).isNotNull();
            if (softly.wasSuccess()) {
                List<ArchClass> classes = new ArrayList<>(architecture.database().classes().values());
                softly.assertThat(classes).extracting(ArchClass::name)
                        .containsExactly("Class3");
                softly.assertThat(classes.get(0)).extracting(architecture.database()::packag)
                        .isSameAs(DEFAULT_PACKAGE);
            }
        }
    }

    @SuppressWarnings("JUnitMalformedDeclaration")
    @Nested
    class WhenBuiltArchitectureToString {
        @Test
        void then_return_description(@Mock SourceFolder sourceFolder1, @Mock SourceFolder sourceFolder2) throws ArchException {
            when(sourceFolder1.moduleName()).thenReturn("module1");
            underTest.addSourceFolder(sourceFolder1)
                    .addClass(sourceFolder1.moduleName(), "package1", "ClassA", "package1.ClassA",
                            "package1.ClassB", "package3.ClassC")
                    .addClass(sourceFolder1.moduleName(), "package1", "ClassB", "package1.ClassB");
            when(sourceFolder2.moduleName()).thenReturn("module2");
            underTest.addSourceFolder(sourceFolder2)
                    .addClass(sourceFolder2.moduleName(), "package3", "ClassC", "package3.ClassC", "package4.ClassD")
                    .addClass(sourceFolder2.moduleName(), "package4", "ClassD", "package4.ClassD");

            assertThat(underTest.build()).hasToString("module1:package1.ClassA->package1.ClassB,package3.ClassC | " +
                    "module1:package1.ClassB | " +
                    "module2:package3.ClassC->package4.ClassD | " +
                    "module2:package4.ClassD");
        }
    }
}