package fr.duminy.arch.engine;

import fr.duminy.arch.cycle.Expectation;

class ExpectedArchitecture extends Expectation {
    protected ExpectedArchitecture(String expectation) {
        super(expectation);
    }

    @Override
    protected void parse() {
    }
}
