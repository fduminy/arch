package fr.duminy.arch.engine;

import org.junit.jupiter.api.DynamicNode;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static java.lang.Long.parseUnsignedLong;
import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.DynamicContainer.dynamicContainer;
import static org.junit.jupiter.api.DynamicTest.dynamicTest;

class MaskTest {
    @TestFactory
    List<DynamicNode> getCode() {
        return buildNodes((mask, value, code) ->
                assertThat(mask.getCode(value)).isEqualTo(code));
    }

    @ParameterizedTest
    @CsvSource(value = {
            "FFFF000000000000,000000000000",
            "0000FFFF00000000,00000000",
            "00000000FFFFFFFF,"
    })
    void getCode_from_valid_index_close_to_bounds(String maskValue, String codeSuffix) {
        String finalCodeSuffix = (codeSuffix == null) ? "" : codeSuffix;
        Mask mask = new Mask(parseHex(maskValue));
        assertAll(
                () -> assertThat(hex(mask.getCode(0))).isEqualTo(hex(0)),

                // close to max index but valid
                () -> assertThat(hex(mask.getCode(0xFFFE)))
                        .isEqualTo("0xFFFE" + finalCodeSuffix),
                () -> assertThat(hex(mask.getCode(0xFFFF)))
                        .isEqualTo("0xFFFF" + finalCodeSuffix)
        );
    }

    @MethodSource("masks")
    @ParameterizedTest
    void getCode_from_too_big_index(Mask mask) {
        assertAll(
                () -> assertThatThrownBy(() -> mask.getCode(0x1_0000))
                        .as("mask=%s", mask)
                        .isExactlyInstanceOf(IndexOutOfBoundsException.class)
                        .hasMessage("index too big for mask %s : 0x%X", mask, 0x1_0000),
                () -> assertThatThrownBy(() -> mask.getCode(0x1_0001))
                        .as("mask=%s", mask)
                        .isExactlyInstanceOf(IndexOutOfBoundsException.class)
                        .hasMessage("index too big for mask %s : 0x%X", mask, 0x1_0001)
        );
    }

    @MethodSource("masks")
    @ParameterizedTest
    void getCode_from_negative_index(Mask mask) {
        assertAll(
                () -> assertThatThrownBy(() -> mask.getCode(-2))
                        .isExactlyInstanceOf(IndexOutOfBoundsException.class)
                        .hasMessage("negative index : 0x%X", -2),
                () -> assertThatThrownBy(() -> mask.getCode(-1))
                        .isExactlyInstanceOf(IndexOutOfBoundsException.class)
                        .hasMessage("negative index : 0x%X", -1)
        );
    }

    @TestFactory
    List<DynamicNode> getIndex() {
        return buildNodes((mask, value, code) ->
                assertThat(mask.getIndex(code)).isEqualTo(value));
    }

    @CsvSource(value = {
            "001F,0x001F",
            "00FF,0x00FF",
            "1F00,0x001F",
            "FF00,0x00FF",

            // maxIndex=Integer.MAX_VALUE
            "000000007FFFFFFF,0x7FFFFFFF",
            "000000008FFFFFFF,0x7FFFFFFF",
            "000000009FFFFFFF,0x7FFFFFFF",
            "00000000AFFFFFFF,0x7FFFFFFF",
            "00000000BFFFFFFF,0x7FFFFFFF",
            "00000000CFFFFFFF,0x7FFFFFFF",
            "00000000DFFFFFFF,0x7FFFFFFF",
            "00000000EFFFFFFF,0x7FFFFFFF",
            "00000000FFFFFFFF,0x7FFFFFFF",
            "0000007FFFFFFF00,0x7FFFFFFF",
            "0000008FFFFFFF00,0x7FFFFFFF",
            "0000009FFFFFFF00,0x7FFFFFFF",
            "000000AFFFFFFF00,0x7FFFFFFF",
            "000000BFFFFFFF00,0x7FFFFFFF",
            "000000CFFFFFFF00,0x7FFFFFFF",
            "000000DFFFFFFF00,0x7FFFFFFF",
            "000000EFFFFFFF00,0x7FFFFFFF",
            "000000FFFFFFFF00,0x7FFFFFFF",
            "7FFFFFFF00000000,0x7FFFFFFF",
            "8FFFFFFF00000000,0x7FFFFFFF",
            "9FFFFFFF00000000,0x7FFFFFFF",
            "AFFFFFFF00000000,0x7FFFFFFF",
            "BFFFFFFF00000000,0x7FFFFFFF",
            "CFFFFFFF00000000,0x7FFFFFFF",
            "DFFFFFFF00000000,0x7FFFFFFF",
            "EFFFFFFF00000000,0x7FFFFFFF",
            "FFFFFFFF00000000,0x7FFFFFFF"
    })
    @ParameterizedTest
    void maxIndex(String mask, int maxIndex) {
        assertThat(hex(new Mask(parseHex(mask)).maxIndex())).isEqualTo(hex(maxIndex));
    }

    @Test
    void getIndex_from_compound_codes() {
        Mask mask = new Mask(0x0000_FFFF_0000_0000L);
        assertThat(mask.getIndex(0x0000100000000L)).isEqualTo(1);
        assertThat(mask.getIndex(0x1000100000001L)).isEqualTo(1);
    }

    private static String hex(long value) {
        return format("0x%X", value);
    }

    private static long parseHex(String mask) {
        return parseUnsignedLong(mask, 16);
    }

    private static Stream<Arguments> masks() {
        return Stream.of(Arguments.of(
                new Mask(0xFFFF_0000_0000_0000L),
                new Mask(0x0000_0000_FFFF_FFFFL),
                new Mask(0x0000_FFFF_0000_0000L)
        ));
    }

    private List<DynamicNode> buildNodes(Executable test) {
        List<DynamicNode> tests = new ArrayList<>();
        long maskValue = 0xFF;
        for (int i = 0; i < 8; i++, maskValue = maskValue << 8) {
            int finalI = i;
            Mask mask = new Mask(maskValue);
            List<DynamicNode> maskTests = new ArrayList<>();
            for (int value : new int[]{0, 1, 127, 128, 254, 255}) {
                maskTests.add(dynamicTest("value=" + value,
                        () -> {
                            long code = (long) value << (finalI * 8);
                            test.execute(mask, value, code);
                        }));
            }
            tests.add(dynamicContainer("Mask " + format("0x%X", maskValue), maskTests));
        }
        return tests;
    }

    private interface Executable {
        void execute(Mask mask, int value, long code);
    }
}