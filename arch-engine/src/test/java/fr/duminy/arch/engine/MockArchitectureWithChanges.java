package fr.duminy.arch.engine;

import fr.duminy.arch.api.ArchClass;
import fr.duminy.arch.api.ArchModule;
import fr.duminy.arch.api.ArchPackage;
import fr.duminy.arch.api.ClassDatabase;
import fr.duminy.arch.cycle.MockArchitecture;
import fr.duminy.arch.dsl.ActionDescription;
import fr.duminy.arch.dsl.ArchitectureDescription;
import fr.duminy.arch.dsl.ClassDependencyDescription;

import java.util.ArrayList;
import java.util.List;

import static fr.duminy.arch.cycle.MockArchitectureForModule.completeModuleMocking;
import static fr.duminy.arch.utils.ClassUtils.getPackageName;
import static fr.duminy.arch.utils.ClassUtils.getSimpleName;
import static java.util.Comparator.comparing;
import static org.mockito.Mockito.lenient;

class MockArchitectureWithChanges extends MockArchitecture<ArchClass, ExpectedArchitecture> {
    private ActionDescription action;
    private String expectedArchitecture;

    @SuppressWarnings("unused")
        // called by junit 5
    MockArchitectureWithChanges(String architecture) {
        super(architecture);
    }

    @Override
    final protected ExpectedArchitecture createExpectation(String expectation) {
        String action;
        int endOfAction = expectation.indexOf('|');
        if (endOfAction < 0) {
            action = expectation;
            expectation = getArchitectureDescription().toString();
        } else {
            action = expectation.substring(0, endOfAction).trim();
            expectation = expectation.substring(endOfAction + 1).trim();
        }
        expectedArchitecture = expectation;

        this.action = new ActionDescription(action);
        return new ExpectedArchitecture(expectation);
    }

    @Override
    protected void completeMocking(ClassDatabase database, String moduleName, String className, ArchClass archClass) {
        lenient().when(archClass.name()).thenReturn(getSimpleName(className));
        completeModuleMocking(database, getNameToModule(), getNameToPackage(), moduleName, className, archClass);
    }

    @Override
    protected ArchClass getElementByName(String name) {
        return getNameToClass().get(name);
    }

    ArchPackage getPackageToMove() {
        return getNameToPackage().get(action.getElementToMove());
    }

    ArchModule getTargetModule() {
        return getNameToModule().getOrMock(action.getTarget());
    }

    ArchClass getClassToMove() {
        return getNameToClass().get(getSimpleName(action.getElementToMove()));
    }

    ArchPackage getPackageOfClassToMove() {
        return getNameToPackage().get(getPackageName(action.getElementToMove()));
    }

    ArchPackage getTargetPackage() {
        return getNameToPackage().getOrMock(action.getTarget());
    }

    List<ClassDependencyDescription> getExpectedClasses() {
        List<ClassDependencyDescription> classDependencies = new ArrayList<>(new ArchitectureDescription(expectedArchitecture).getClassDependencies());
        classDependencies.sort(comparing(ClassDependencyDescription::getClassName));
        return classDependencies;
    }

}
