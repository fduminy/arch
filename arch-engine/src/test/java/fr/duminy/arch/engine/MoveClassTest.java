package fr.duminy.arch.engine;

import org.assertj.core.api.SoftAssertions;
import org.assertj.core.api.junit.jupiter.SoftAssertionsExtension;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith({MockitoExtension.class, SoftAssertionsExtension.class})
class MoveClassTest extends AbstractMoveTest {

    @ParameterizedTest
    @ValueSource(strings = {
            // no changes
            "m1:x.A => x.A->x",
            "m1:x.A | m1:x.B => x.A->x",
            "m1:x.A->x.B | m1:x.B => x.A->x",
            "m1:x.A->x.B | m1:x.B->x.A => x.A->x",
            "m1:x.A->y.B | m2:y.B->x.A => x.A->x | m1:x.A->y.B | m2:y.B->x.A",

            // move from one package to another in the same module
            "m1:x.A => x.A->y | m1:y.A",
            "m1:x.A | m1:x.B => x.A->y | m1:x.B | m1:y.A",
            "m1:x.A->x.B | m1:x.B => x.A->y | m1:x.B | m1:y.A->x.B",
            "m1:x.A->x.B | m1:x.B->x.A => x.A->y | m1:x.B->y.A | m1:y.A->x.B",

            // move from one package to another in a different module
            "m1:x.A | m2:y.B => x.A->y | m2:y.A | m2:y.B",
    })
    void transform(MockArchitectureWithChanges mockArchitecture, @Mock CloneClassDatabaseBuilder builder, SoftAssertions softly) {
        transform(mockArchitecture, builder, softly, builderFactory ->
                new MoveClass(builderFactory, mockArchitecture.getPackageOfClassToMove(), mockArchitecture.getClassToMove(), mockArchitecture.getTargetPackage()));
    }
}