package fr.duminy.arch.engine;

import org.assertj.core.api.SoftAssertions;
import org.assertj.core.api.junit.jupiter.SoftAssertionsExtension;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith({MockitoExtension.class, SoftAssertionsExtension.class})
class MovePackageTest extends AbstractMoveTest {
    @ParameterizedTest
    @ValueSource(strings = {
            // no changes
            "m1:x.A => x->m1",
            "m1:x.A | m1:x.B => x->m1",
            "m1:x.A->x.B | m1:x.B => x->m1",
            "m1:x.A->x.B | m1:x.B->x.A => x->m1",
            "m1:x.A->y.B | m2:y.B->x.A => x->m1 | m1:x.A->y.B | m2:y.B->x.A",

            // changes
            "m1:x.A => x->m2 | m2:x.A",
            "m1:x.A | m1:x.B => x->m2 | m2:x.A | m2:x.B",
            "m1:x.A->x.B | m1:x.B => x->m2 | m2:x.A->x.B | m2:x.B",
            "m1:x.A->x.B | m1:x.B->x.A => x->m2 | m2:x.A->x.B | m2:x.B->x.A",
            "m1:x.A->y.B | m2:y.B->x.A => x->m2 | m2:x.A->y.B | m2:y.B->x.A",
    })
    void transform(MockArchitectureWithChanges mockArchitecture, @Mock CloneClassDatabaseBuilder builder, SoftAssertions softly) {
        transform(mockArchitecture, builder, softly, builderFactory ->
                new MovePackage(builderFactory, mockArchitecture.getPackageToMove(), mockArchitecture.getTargetModule()));
    }
}