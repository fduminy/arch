package fr.duminy.arch.java;

import fr.duminy.arch.api.ArchException;
import fr.duminy.arch.api.SourceArchitectureBuilder;
import fr.duminy.arch.api.SourceCodeScanner;
import fr.duminy.arch.api.SourceFolder;
import org.slf4j.Logger;
import spoon.Launcher;
import spoon.SpoonAPI;
import spoon.reflect.declaration.CtType;
import spoon.reflect.reference.CtTypeReference;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Stream;

import static java.nio.file.Files.exists;
import static java.nio.file.Files.walk;
import static java.util.stream.Stream.empty;
import static org.slf4j.LoggerFactory.getLogger;

public class JavaSourceCodeScanner implements SourceCodeScanner {
    private static final Logger LOGGER = getLogger(JavaSourceCodeScanner.class);

    @Override
    public void configure(SourceArchitectureBuilder builder) throws ArchException {
        Collection<JavaFile> javaFiles = getJavaFiles(builder.getSourceFolders());

        Map<JavaFile, Collection<CtType<?>>> allTypes = new HashMap<>();
        Set<String> allTypeNames = new HashSet<>();
        javaFiles.forEach(javaFile -> {
            SpoonAPI spoon = new Launcher();
            spoon.addInputResource(javaFile.javaFile().toString());
            Collection<CtType<?>> declaredTypes = spoon.buildModel().getAllTypes();
            declaredTypes.forEach(declaredType -> allTypeNames.add(declaredType.getQualifiedName()));
            allTypes.put(javaFile, declaredTypes);
        });

        allTypes.forEach((javaFile, declaredTypes) ->
                declaredTypes.forEach(declaredType -> {
                    String moduleName = javaFile.sourceFolder().moduleName();
                    String packageName = declaredType.getPackage().getQualifiedName();
                    String classSimpleName = declaredType.getSimpleName();
                    String className = declaredType.getQualifiedName();
                    String[] usedClassNames = getUsedTypes(declaredType)
                            .map(CtTypeReference::getQualifiedName)
                            .filter(allTypeNames::contains)
                            .toArray(String[]::new);
                    builder.addClass(moduleName, packageName, classSimpleName, className, usedClassNames);
                }));
    }

    private Collection<JavaFile> getJavaFiles(Iterable<SourceFolder> sourceFolders) throws ArchException {
        List<JavaFile> javaFiles = new ArrayList<>();
        for (SourceFolder sourceFolder : sourceFolders) {
            if (!exists(sourceFolder.folder())) {
                LOGGER.warn("{} doesn't exist", sourceFolder.folder());
                continue;
            }
            try (Stream<Path> files = walk(sourceFolder.folder())) {
                files.filter(Files::isRegularFile)
                        .filter(file -> file.getFileName().toString().endsWith(".java"))
                        .forEach(file -> javaFiles.add(new JavaFile(sourceFolder, file)));
            } catch (IOException e) {
                throw new ArchException(e);
            }
        }
        return javaFiles;
    }

    private record JavaFile(SourceFolder sourceFolder, Path javaFile) {
    }

    private Stream<CtTypeReference<?>> getUsedTypes(CtType<?> type) {
        try {
            return type.getUsedTypes(true).stream()
                    .filter(usedType -> !usedType.getQualifiedName().endsWith(type.getQualifiedName()));
        } catch (Exception e) {
            LOGGER.warn("Error getting used types for {} : {}", type.getQualifiedName(),
                    e.getMessage(), e);
            return empty();
        }
    }
}
