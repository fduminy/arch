package fr.duminy.arch.java.test;

import java.nio.file.Path;

import static java.io.File.separatorChar;
import static java.nio.file.Paths.get;

public class TestSources {
    public static String getClassName(String subPackage, String classSimpleName) {
        return getRootPackage() + '.' + subPackage + '.' + classSimpleName;
    }

    public static String getClassName(String classSimpleName) {
        return getRootPackage() + '.' + classSimpleName;
    }

    public static Path getSourceFolder(String relativeSourceFolder) {
        return getRootFolder().resolve(relativeSourceFolder);
    }

    private static Path getRootFolder() {
        String folder = getRootPackage().replace('.', separatorChar);
        return get("src", "test-sources").resolve(folder);
    }

    private static String getRootPackage() {
        return TestSources.class.getPackageName();
    }
}
