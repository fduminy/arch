package fr.duminy.arch.java.test.exclude.self.singleclass;

@SuppressWarnings("ALL") // used by tests
class MainClass {
    private final int value;

    MainClass() {
        value = 123;
    }
}
