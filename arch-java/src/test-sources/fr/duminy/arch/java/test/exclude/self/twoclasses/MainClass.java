package fr.duminy.arch.java.test.exclude.self.twoclasses;

@SuppressWarnings("ALL") // used by tests
class MainClass {
    private final ParentOfMainClass parent;

    MainClass() {
        parent = new ParentOfMainClass();
    }
}
