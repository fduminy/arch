package fr.duminy.arch.java;

import fr.duminy.arch.api.*;
import org.assertj.core.api.SoftAssertions;
import org.assertj.core.api.junit.jupiter.InjectSoftAssertions;
import org.assertj.core.api.junit.jupiter.SoftAssertionsExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;
import java.util.stream.Stream.Builder;

import static fr.duminy.arch.java.test.TestSources.getClassName;
import static fr.duminy.arch.java.test.TestSources.getSourceFolder;
import static java.lang.System.arraycopy;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static java.util.stream.Stream.builder;
import static org.junit.jupiter.params.provider.Arguments.of;
import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class, SoftAssertionsExtension.class})
class JavaSourceCodeScannerTest {
    @InjectMocks
    private JavaSourceCodeScanner underTest;

    @InjectSoftAssertions
    private SoftAssertions softly;

    @Test
    void configure_with_missing_directory(@TempDir Path tempDir) throws ArchException {
        SourceFolder sourceFolder = new SourceFolder("emptyModule", tempDir.resolve("emptySourceFolder"));
        MockBuilderForSingleClass builder = spy(new MockBuilderForSingleClass(singletonList(sourceFolder),
                "a.b.C"));

        underTest.configure(builder);

        verify(builder, never()).addClass(any(), any(), any(), any());
    }

    @ParameterizedTest(name = "#{index} - ...{0}.{1}")
    @MethodSource
    void configure(String subFolder, String className, String... usedClassNames) throws ArchException {
        SourceFolder sourceFolder = new SourceFolder("moduleFor" + subFolder, getSourceFolder(subFolder));
        SourceFolder sourceFolder2 = new SourceFolder("moduleFor" + "folder2", getSourceFolder("folder2"));
        String classFullName = getClassName(subFolder, className);
        MockBuilderForSingleClass builder = new MockBuilderForSingleClass(asList(sourceFolder, sourceFolder2), classFullName);

        underTest.configure(builder);

        softly.assertThat(builder.actualInvocations).usingRecursiveFieldByFieldElementComparatorIgnoringFields("usedClassNames")
                .containsExactlyInAnyOrder(new Invocation(sourceFolder.moduleName(), classFullName, usedClassNames));
        if (softly.wasSuccess()) {
            softly.assertThat(builder.actualInvocations.get(0).usedClassNames).containsExactlyInAnyOrder(usedClassNames);
        }
    }

    @ParameterizedTest
    @ValueSource(strings = {"exclude/self/singleclass", "exclude/self/twoclasses"})
    void configure_must_not_include_class_in_used_classes(String subFolder) throws ArchException {
        String className = "MainClass";
        SourceFolder sourceFolder = new SourceFolder("moduleFor" + subFolder, getSourceFolder(subFolder));
        String classFullName = getClassName(subFolder.replace('/', '.'), className);

        MockBuilderForSingleClass builder = new MockBuilderForSingleClass(singletonList(sourceFolder), classFullName);

        underTest.configure(builder);

        if (!builder.actualInvocations.isEmpty()) {
            softly.assertThat(builder.actualInvocations.get(0).usedClassNames).doesNotContain(classFullName);
        }
    }

    @SuppressWarnings("unused")
    private static Stream<Arguments> configure() {
        String[][] allArguments = {
                {"folder2", "Class2", "folder2.Class3"},
                {"folder2", "Class3", "folder2.Class4"},
                {"folder2", "Class4", "folder2.Class2"},
                {"folder1", "Class1", "folder2.Class2", "folder2.Class3", "folder2.Class4"}
        };
        Builder<Arguments> argumentsBuilder = builder();
        for (String[] arguments : allArguments) {
            String[] usedClassNames = new String[arguments.length - 2];
            arraycopy(arguments, 2, usedClassNames, 0, usedClassNames.length);
            for (int i = 0; i < usedClassNames.length; i++) {
                usedClassNames[i] = getClassName(usedClassNames[i]);
            }
            argumentsBuilder.add(of(arguments[0], arguments[1], usedClassNames));
        }
        return argumentsBuilder.build();
    }

    private static class MockBuilderForSingleClass implements SourceArchitectureBuilder {
        private final List<Invocation> actualInvocations = new ArrayList<>();
        private final List<SourceFolder> sourceFolders;
        private final String className;

        public MockBuilderForSingleClass(List<SourceFolder> sourceFolders, String className) {
            this.sourceFolders = sourceFolders;
            this.className = className;
        }

        @Override
        public Path getRootFolder() {
            return null;
        }

        @Override
        public SourceArchitectureBuilder configureWith(Configurator configurator) {
            return null;
        }

        @Override
        public SourceArchitectureBuilder addSourceFolder(SourceFolder sourceFolder) {
            return null;
        }

        @Override
        public Iterable<SourceFolder> getSourceFolders() {
            return sourceFolders;
        }

        @Override
        public SourceArchitectureBuilder addClass(String moduleName, String packageName, String classSimpleName, String className, String... usedClassNames) {
            if (this.className.equals(className)) {
                actualInvocations.add(new Invocation(moduleName, className, usedClassNames));
            }
            return this;
        }

        @Override
        public Architecture build() {
            return null;
        }
    }

    @SuppressWarnings("unused")
    private record Invocation(String moduleName, String className, String... usedClassNames) {
    }
}