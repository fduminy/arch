package fr.duminy.arch.maven;

import fr.duminy.arch.api.ArchException;
import fr.duminy.arch.api.SourceArchitectureBuilder;
import fr.duminy.arch.api.SourceFolder;
import fr.duminy.arch.api.SourceFolderFinder;
import org.apache.maven.model.Model;
import org.apache.maven.model.building.*;

import java.nio.file.Path;
import java.util.Objects;

import static java.nio.file.Files.exists;
import static java.nio.file.Paths.get;
import static org.apache.maven.model.building.ModelBuildingRequest.VALIDATION_LEVEL_MINIMAL;

public class MavenConfigurator implements SourceFolderFinder {
    @Override
    public void configure(SourceArchitectureBuilder builder) throws ArchException {
        configure(builder, builder.getRootFolder());
    }

    private void configure(SourceArchitectureBuilder builder, Path directory) throws ArchException {
        Path pomFile = directory.resolve("pom.xml");
        if (!exists(pomFile)) {
            return;
        }
        Model model = readModel(pomFile);
        if (Objects.equals(model.getPackaging(), "jar")) {
            String moduleName = model.getGroupId() + ':' + model.getArtifactId();
            builder.addSourceFolder(new SourceFolder(moduleName, get(model.getBuild().getSourceDirectory())));
        }
        for (String module : model.getModules()) {
            configure(builder, pomFile.getParent().resolve(module));
        }
    }

    private Model readModel(Path pomFile) throws ArchException {
        ModelBuildingRequest request = new DefaultModelBuildingRequest();
        request.setProcessPlugins(false);
        request.setPomFile(pomFile.toFile());
        request.setValidationLevel(VALIDATION_LEVEL_MINIMAL);

        try {
            ModelBuilder modelBuilder = new DefaultModelBuilderFactory().newInstance();
            return modelBuilder.build(request).getEffectiveModel();
        } catch (ModelBuildingException e) {
            throw new ArchException(e);
        }
    }
}
