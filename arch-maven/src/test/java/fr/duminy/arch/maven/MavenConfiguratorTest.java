package fr.duminy.arch.maven;

import fr.duminy.arch.api.ArchException;
import fr.duminy.arch.api.SourceArchitectureBuilder;
import fr.duminy.arch.api.SourceFolder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static java.nio.file.Path.of;
import static java.nio.file.Paths.get;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class MavenConfiguratorTest {
    private static final Path TEST_PROJECTS_DIRECTORY = of("src", "test-projects").toAbsolutePath();

    @Mock
    private SourceArchitectureBuilder builder;
    @InjectMocks
    private MavenConfigurator underTest;

    @Nested
    class WhenConfigureWithoutPOMFile {
        private static final Path ROOT = TEST_PROJECTS_DIRECTORY.resolve("no-pom-file");

        @BeforeEach
        void setUp() {
            when(builder.getRootFolder()).thenReturn(ROOT);
        }

        @Test
        void then_do_nothing() throws ArchException {
            underTest.configure(builder);

            verify(builder).getRootFolder();
            verifyNoMoreInteractions(builder);
        }
    }

    @Nested
    class WhenConfigureSingleModule {
        private static final Path ROOT = TEST_PROJECTS_DIRECTORY.resolve("single-module");

        @BeforeEach
        void setUp() {
            when(builder.getRootFolder()).thenReturn(ROOT);
        }

        @Test
        void then_add_source_folder() throws ArchException {
            underTest.configure(builder);

            verify(builder).addSourceFolder(new SourceFolder(
                    "fr.duminy.arch.test:single-module",
                    ROOT.resolve(get("src", "main", "java"))));
        }
    }

    @Nested
    class WhenConfigureMultipleModules {
        private static final Path ROOT = TEST_PROJECTS_DIRECTORY.resolve("multiple-modules");

        @BeforeEach
        void setUp() {
            when(builder.getRootFolder()).thenReturn(ROOT);
        }

        @Test
        void then_add_source_folder() throws ArchException {
            List<SourceFolder> sourceFolders = new ArrayList<>();
            when(builder.addSourceFolder(any())).then((Answer<Void>) invocationOnMock -> {
                sourceFolders.add(invocationOnMock.getArgument(0, SourceFolder.class));
                return null;
            });

            underTest.configure(builder);

            assertThat(sourceFolders).containsExactlyInAnyOrder(
                    new SourceFolder("fr.duminy.arch.test:module1", ROOT.resolve(get("module1", "src", "main", "java"))),
                    new SourceFolder("fr.duminy.arch.test:module3", ROOT.resolve(get("module2", "module3", "src", "main", "java")))
            );
        }
    }
}