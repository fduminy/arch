package fr.duminy.arch.model;

import fr.duminy.arch.api.ArchClass;

public record DefaultArchClass(int id, String name) implements ArchClass {
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        return id == ((DefaultArchClass) o).id;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public String toString() {
        return "DefaultArchClass[id=" + ((ArchClass) this).id() + ", " +
                "name=" + ((ArchClass) this).name() + ']';
    }
}
