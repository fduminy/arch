package fr.duminy.arch.model;

import fr.duminy.arch.api.ArchCollection;
import fr.duminy.arch.api.ArchElement;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;

public class DefaultArchCollection<T extends ArchElement> implements ArchCollection<T> {
    private final List<T> elements;
    private final int fixedSize;

    public DefaultArchCollection(int fixedSize) {
        if (fixedSize < 0) {
            throw new IllegalArgumentException("fixedSize is negative");
        }
        this.fixedSize = fixedSize;
        this.elements = new ArrayList<>(fixedSize);
    }

    @Override
    public T get(String name) {
        T element = null;
        for (T e : elements) {
            if (e.name().equals(name)) {
                element = e;
                break;
            }
        }
        return element;
    }

    @Override
    public T get(int index) {
        return elements.get(index);
    }

    public void add(T element) {
        computeIfAbsent(element.name(), name -> element);
    }

    public void computeIfAbsent(String name, Function<? super String, ? extends T> mappingFunction) {
        T element = get(name);
        if (element == null) {
            ensureNotFull();
            elements.add(mappingFunction.apply(name));
        }
    }

    @Override
    public Collection<T> values() {
        return elements;
    }

    public int size() {
        return elements.size();
    }

    private void ensureNotFull() {
        if (elements.size() >= fixedSize) {
            throw new IllegalArgumentException("Collection is full (fixedSize=" + fixedSize + ")");
        }
    }
}
