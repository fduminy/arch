package fr.duminy.arch.model;

import fr.duminy.arch.api.ArchModule;

@SuppressWarnings("unused")
public record DefaultArchModule(int id, String name) implements ArchModule {
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        return id == ((DefaultArchModule) o).id;
    }

    @Override
    public int hashCode() {
        return id;
    }
}
