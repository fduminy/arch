package fr.duminy.arch.model;

import fr.duminy.arch.api.ArchPackage;

@SuppressWarnings("unused")
public record DefaultArchPackage(int id, String name) implements ArchPackage {
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        return id == ((DefaultArchPackage) o).id;
    }

    @Override
    public int hashCode() {
        return id;
    }
}
