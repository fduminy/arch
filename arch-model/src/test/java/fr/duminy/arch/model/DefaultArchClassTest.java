package fr.duminy.arch.model;

import fr.duminy.arch.api.ArchClass;
import org.assertj.core.api.SoftAssertions;
import org.assertj.core.api.junit.jupiter.SoftAssertionsExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(SoftAssertionsExtension.class)
class DefaultArchClassTest {
    @Test
    void constructor(SoftAssertions softly) {
        DefaultArchClass archClass = new DefaultArchClass(1, "Class");
        softly.assertThat(archClass).extracting(ArchClass::id, ArchClass::name)
                .containsExactly(1, "Class");
    }
}