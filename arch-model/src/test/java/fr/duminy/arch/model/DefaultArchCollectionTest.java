package fr.duminy.arch.model;

import fr.duminy.arch.api.ArchElement;
import org.assertj.core.api.SoftAssertions;
import org.assertj.core.api.junit.jupiter.SoftAssertionsExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.when;

class DefaultArchCollectionTest {
    @SuppressWarnings("JUnitMalformedDeclaration")
    @Test
    @ExtendWith(SoftAssertionsExtension.class)
    void constructor(SoftAssertions softly) {
        DefaultArchCollection<ArchElement> collection = new DefaultArchCollection<>(1);

        softly.assertThat(collection.values()).isEmpty();
        softly.assertThat(collection.size()).isZero();
        softly.assertThatThrownBy(() -> collection.get(0))
                .isExactlyInstanceOf(IndexOutOfBoundsException.class);
    }

    @Test
    void constructor_negative_fixedSize() {
        assertThatThrownBy(() -> new DefaultArchCollection<>(-1))
                .isExactlyInstanceOf(IllegalArgumentException.class);
    }

    @SuppressWarnings("JUnitMalformedDeclaration")
    @Test
    @ExtendWith({SoftAssertionsExtension.class, MockitoExtension.class})
    void add(@Mock ArchElement element1, @Mock ArchElement element2,
             @Mock ArchElement element3, SoftAssertions softly) {
        when(element1.name()).thenReturn("element1");
        when(element2.name()).thenReturn("element2");
        when(element3.name()).thenReturn("element3");
        DefaultArchCollection<ArchElement> collection = new DefaultArchCollection<>(3);

        collection.add(element1);
        collection.add(element2);
        collection.add(element3);

        softly.assertThat(collection.values()).containsExactly(element1, element2, element3);
        softly.assertThat(collection.size()).isEqualTo(3);
    }

    @SuppressWarnings("JUnitMalformedDeclaration")
    @Test
    @ExtendWith(MockitoExtension.class)
    void add_too_many_elements(@Mock ArchElement element1, @Mock ArchElement element2) {
        when(element1.name()).thenReturn("element1");
        when(element2.name()).thenReturn("element2");
        DefaultArchCollection<ArchElement> collection = new DefaultArchCollection<>(1);
        collection.add(element1);

        assertThatThrownBy(() -> collection.add(element2)).isExactlyInstanceOf(IllegalArgumentException.class);
    }

    @SuppressWarnings("JUnitMalformedDeclaration")
    @Test
    @ExtendWith({SoftAssertionsExtension.class, MockitoExtension.class})
    void get_by_index(@Mock ArchElement element1, @Mock ArchElement element2,
                      @Mock ArchElement element3, SoftAssertions softly) {
        when(element1.name()).thenReturn("element1");
        when(element2.name()).thenReturn("element2");
        when(element3.name()).thenReturn("element3");
        DefaultArchCollection<ArchElement> collection = new DefaultArchCollection<>(3);

        collection.add(element1);
        collection.add(element2);
        collection.add(element3);

        softly.assertThat(collection.get(0)).isSameAs(element1);
        softly.assertThat(collection.get(1)).isSameAs(element2);
        softly.assertThat(collection.get(2)).isSameAs(element3);
    }

    @SuppressWarnings("JUnitMalformedDeclaration")
    @ParameterizedTest
    @ValueSource(ints = {-1, 1})
    @ExtendWith(MockitoExtension.class)
    void get_by_invalid_index(int invalidIndex, @Mock ArchElement element1) {
        when(element1.name()).thenReturn("element1");
        DefaultArchCollection<ArchElement> collection = new DefaultArchCollection<>(3);

        collection.add(element1);

        assertThatThrownBy(() -> collection.get(invalidIndex))
                .isExactlyInstanceOf(IndexOutOfBoundsException.class);
    }

    @SuppressWarnings("JUnitMalformedDeclaration")
    @Test
    @ExtendWith({SoftAssertionsExtension.class, MockitoExtension.class})
    void get_by_name(@Mock ArchElement element1, @Mock ArchElement element2,
                     @Mock ArchElement element3, SoftAssertions softly) {
        when(element1.name()).thenReturn("element1");
        when(element2.name()).thenReturn("element2");
        when(element3.name()).thenReturn("element3");
        DefaultArchCollection<ArchElement> collection = new DefaultArchCollection<>(3);

        collection.add(element1);
        collection.add(element2);
        collection.add(element3);

        softly.assertThat(collection.get("element1")).isSameAs(element1);
        softly.assertThat(collection.get("element2")).isSameAs(element2);
        softly.assertThat(collection.get("element3")).isSameAs(element3);
    }

    @SuppressWarnings("JUnitMalformedDeclaration")
    @ParameterizedTest
    @ValueSource(strings = {"", "element2"})
    @ExtendWith(MockitoExtension.class)
    void get_by_invalid_name(String invalidName, @Mock ArchElement element1) {
        when(element1.name()).thenReturn("element1");
        DefaultArchCollection<ArchElement> collection = new DefaultArchCollection<>(3);

        collection.add(element1);

        assertThat(collection.get(invalidName)).isNull();
    }
}