package fr.duminy.arch.model;

import fr.duminy.arch.api.ArchModule;
import org.assertj.core.api.SoftAssertions;
import org.assertj.core.api.junit.jupiter.SoftAssertionsExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(SoftAssertionsExtension.class)
class DefaultArchModuleTest {
    @Test
    void constructor(SoftAssertions softly) {
        DefaultArchModule archModule1 = new DefaultArchModule(1, "module1");
        softly.assertThat(archModule1).extracting(ArchModule::id).isSameAs(1);
        softly.assertThat(archModule1).extracting(ArchModule::name).isSameAs("module1");
    }
}