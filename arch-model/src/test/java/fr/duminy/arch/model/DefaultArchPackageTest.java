package fr.duminy.arch.model;

import fr.duminy.arch.api.ArchPackage;
import org.assertj.core.api.SoftAssertions;
import org.assertj.core.api.junit.jupiter.SoftAssertionsExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(SoftAssertionsExtension.class)
class DefaultArchPackageTest {
    @Test
    void constructor(SoftAssertions softly) {
        DefaultArchPackage archPackage1 = new DefaultArchPackage(1, "root.package1");
        softly.assertThat(archPackage1).extracting(ArchPackage::id).isSameAs(1);
        softly.assertThat(archPackage1).extracting(ArchPackage::name).isSameAs("root.package1");
    }
}