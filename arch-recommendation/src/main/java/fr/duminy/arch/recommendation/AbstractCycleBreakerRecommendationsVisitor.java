package fr.duminy.arch.recommendation;

import fr.duminy.arch.api.Cycle;
import fr.duminy.arch.engine.Action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.stream;
import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.joining;

public abstract class AbstractCycleBreakerRecommendationsVisitor implements CycleBreakerRecommendationsVisitor {
    static final String ACTION_DELIMITER = ",";

    private final Appendable buffer;
    private final List<Action[]> possibleActionsForACycle = new ArrayList<>();
    private Cycle<?> currentCycle;

    protected AbstractCycleBreakerRecommendationsVisitor(Appendable buffer) {
        this.buffer = buffer;
    }

    @Override
    public final void visitCycleSolutions(Cycle<?> cycle) {
        if (currentCycle != null) {
            finalizeCycleSolutions();
        }
        currentCycle = cycle;
    }

    @Override
    public final void visitAPossibleSolution(Action... actions) {
        possibleActionsForACycle.add(actions);
    }

    @Override
    public final void visitEnd() {
        finalizeCycleSolutions();
    }

    final String asString(Action[] actions) {
        // sorted for tests
        return stream(actions).map(Action::toString).sorted().collect(joining(ACTION_DELIMITER));
    }

    protected abstract void startCycle(Cycle<?> currentCycle);

    protected abstract void startSolution(Action... aSolution);

    protected abstract void endCycle();

    protected abstract void endCycleWithoutSolution();

    protected final void append(CharSequence sequence) {
        try {
            buffer.append(sequence);
        } catch (IOException e) {
            throw new VisitorException(e);
        }
    }

    private void finalizeCycleSolutions() {
        if (currentCycle == null) {
            return;
        }

        startCycle(currentCycle);
        if (possibleActionsForACycle.isEmpty()) {
            endCycleWithoutSolution();
        } else {
            possibleActionsForACycle.stream()
                    .sorted(comparing(this::asString))
                    .forEach(this::startSolution);
            endCycle();
        }

        possibleActionsForACycle.clear();
    }

    private static class VisitorException extends RuntimeException {
        public VisitorException(Throwable cause) {
            super(cause);
        }
    }
}
