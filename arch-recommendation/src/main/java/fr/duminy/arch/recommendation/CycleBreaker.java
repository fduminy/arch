package fr.duminy.arch.recommendation;

import fr.duminy.arch.api.ArchElement;
import fr.duminy.arch.api.Architecture;
import fr.duminy.arch.api.Cycle;
import fr.duminy.arch.api.RecommendationFinder;
import fr.duminy.arch.engine.Action;

import java.util.*;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.joining;

public class CycleBreaker implements RecommendationFinder {
    private final CycleBreakerEngine engine;

    CycleBreaker(CycleBreakerEngine engine) {
        this.engine = engine;
    }

    public CycleBreakerRecommendations find(Architecture architecture) {
        return breakCycles(architecture);
    }

    @SuppressWarnings("unchecked")
    private CycleBreakerRecommendations breakCycles(Architecture architecture) {
        Map<Cycle<?>, List<Generation>> bestArchitectures = engine.findBestArchitectures(architecture);
        bestArchitectures = sortByCycle(bestArchitectures); // sorted for tests

        List<RecommendationsForBreakingACycle> breakers = new ArrayList<>();
        bestArchitectures.forEach(((cycle, generations) -> {
            Set<ActionSet> recommendations = new HashSet<>();
            for (Generation generation : generations) {
                Map<ArchElement, Action> movesBySubject = new HashMap<>();
                generation.walkHistory(gene -> {
                    movesBySubject.put(gene.action().subject(), gene.action());
                    return true;
                });
                if (!movesBySubject.isEmpty()) {
                    recommendations.add(new ActionSet(movesBySubject.values().toArray(Action[]::new)));
                }
            }
            breakers.add(new RecommendationsForBreakingACycle((Cycle<ArchElement>) cycle,
                    recommendations.stream().map(ActionSet::actions).toList()));
        }));
        return new CycleBreakerRecommendations(breakers);
    }

    private Map<Cycle<?>, List<Generation>> sortByCycle(Map<Cycle<?>, List<Generation>> bestArchitectures) {
        Comparator<Cycle<?>> cycleComparator = comparing(cycle ->
                cycle.stream().map(Object::toString)
                        .collect(joining("#")));
        Map<Cycle<?>, List<Generation>> sorted = new TreeMap<>(cycleComparator);
        sorted.putAll(bestArchitectures);
        return sorted;
    }

    @SuppressWarnings("java:S6218")
    private record ActionSet(Action[] actions) {
        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            ActionSet actionSet = (ActionSet) o;
            return Arrays.equals(actions, actionSet.actions);
        }

        @Override
        public int hashCode() {
            return Arrays.hashCode(actions);
        }
    }
}
