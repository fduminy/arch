package fr.duminy.arch.recommendation;

import fr.duminy.arch.api.ArchException;

public interface CycleBreakerBuilder {
    static CycleBreakerBuilder builder() {
        return new CycleBreakerBuilderImpl();
    }

    CycleBreakerBuilder level(CycleLevel level);

    CycleBreakerBuilder movePackage(boolean movePackage);

    CycleBreakerBuilder moveClass(boolean moveClass);

    CycleBreaker build() throws ArchException;
}
