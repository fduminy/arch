package fr.duminy.arch.recommendation;

import fr.duminy.arch.api.ArchElement;
import fr.duminy.arch.api.ArchException;
import fr.duminy.arch.api.ArchModule;
import fr.duminy.arch.api.ArchPackage;
import fr.duminy.arch.recommendation.CycleBreakerEngine.CycleBreakerLevel;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;

import static fr.duminy.arch.recommendation.CycleLevel.MODULE;
import static fr.duminy.arch.recommendation.CycleLevel.PACKAGE;

class CycleBreakerBuilderImpl implements CycleBreakerBuilder {
    private CycleLevel level;
    private boolean movePackage;
    private boolean moveClass;

    @Override
    public CycleBreakerBuilder level(CycleLevel level) {
        this.level = level;
        return this;
    }

    @Override
    public CycleBreakerBuilder movePackage(boolean movePackage) {
        this.movePackage = movePackage;
        return this;
    }

    @Override
    public CycleBreakerBuilder moveClass(boolean moveClass) {
        this.moveClass = moveClass;
        return this;
    }

    @SuppressWarnings("unchecked")
    @Override
    public CycleBreaker build() throws ArchException {
        if (level == null) {
            throw new ArchException("level of cycles to break not specified");
        }
        if (!movePackage && !moveClass) {
            throw new ArchException("can't break module cycles when no moves are allowed");
        }
        if (movePackage && PACKAGE.equals(level)) {
            throw new ArchException("can't break package cycles by moving package");
        }

        BiFunction<Generation, ArchElement, List<? extends ArchElement>> childrenGetter;
        if (MODULE.equals(level)) {
            childrenGetter = (Generation generation, ArchElement module) -> generation.getPackages((ArchModule) module);
        } else {
            childrenGetter = (Generation generation, ArchElement packag) -> generation.getClasses((ArchPackage) packag);
        }

        List<PossibleActionFinder<?, ?>> possibleActionFinders = new ArrayList<>();
        if (movePackage) {
            possibleActionFinders.add(new PossibleMovePackageActionFinder());
        }
        if (moveClass) {
            possibleActionFinders.add(new PossibleMoveClassActionFinder());
        }

        CycleBreakerLevel<?> cycleBreakerLevel = new CycleBreakerLevelImpl<>(childrenGetter, possibleActionFinders.toArray(new PossibleActionFinder[0]), level);
        return new CycleBreaker(new CycleBreakerEngine(cycleBreakerLevel));
    }
}
