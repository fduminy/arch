package fr.duminy.arch.recommendation;

import fr.duminy.arch.api.ArchElement;
import fr.duminy.arch.api.Architecture;
import fr.duminy.arch.api.Cycle;
import fr.duminy.arch.engine.Action;

import java.util.*;
import java.util.concurrent.Callable;
import java.util.function.Consumer;
import java.util.function.Predicate;

import static fr.duminy.arch.recommendation.NullAction.NULL_ACTION;

final class CycleBreakerEngine {
    private final Evaluator evaluator;
    private final CycleBreakerLevel<?> level;
    private final CycleBreakerExecutor executor = new CycleBreakerExecutor();

    CycleBreakerEngine(CycleBreakerLevel<?> level) {
        this.level = level;
        evaluator = new Evaluator(level.cycleLevel());
    }

    Map<Cycle<?>, List<Generation>> findBestArchitectures(Architecture architecture) {
        try {
            Generation firstGeneration = new Generation(null, NULL_ACTION, architecture);
            Collection<Cycle<? extends ArchElement>> cycles = firstGeneration.findCycles(level.cycleLevel());
            Map<Cycle<?>, List<Generation>> generationsPerCycle = new HashMap<>(cycles.size(), 1);
            for (Cycle<?> cycle : cycles) {
                List<Generation> bestArchitectures = findBestArchitectures(firstGeneration, cycle, level);
                bestArchitectures.forEach(Generation::invalidateCaches);
                generationsPerCycle.put(cycle, bestArchitectures);
            }
            return generationsPerCycle;
        } finally {
            executor.shutdown();
        }
    }

    static Generation transform(Generation generation, Action action) {
        if (generation == null) {
            return null;
        }

        Architecture newArchitecture = action.transform(generation.architecture());
        if (newArchitecture == generation.architecture()) {
            return null;
        }

        return new Generation(generation, action, newArchitecture);
    }

    private List<Generation> findBestArchitectures(Generation initialGeneration, Cycle<?> initialCycle, CycleBreakerLevel<?> level) {
        List<String> initialCycleElements = new ArrayList<>();
        level.fillElementNames(initialGeneration, initialCycle, initialCycleElements, n -> true);
        GenerationPool generationPool = new GenerationPool(initialGeneration, evaluator);
        CycleBreakingTask task = new CycleBreakingTask(generationPool, level);
        do {
            generationPool.moveCurrentGenerationsToPreviousGenerations();
            List<Callable<Void>> tasks = generationPool.previousGenerations().parallelStream()
                    .map(generation -> (Callable<Void>) () -> {
                        task.tryToBreakCycle(initialCycleElements, generation);
                        return null;
                    })
                    .toList();
            executor.invokeAll(tasks);
        } while (generationPool.hasEvolved());

        List<Generation> generations = generationPool.currentGenerations();
        if (generations.isEmpty()) {
            generations = generationPool.previousGenerations();
        }
        return generations;
    }

    interface CycleBreakerLevel<E extends ArchElement> {
        void transform(Generation generation, Cycle<E> cycle, Consumer<Generation> newGenerationConsumer);

        void fillElementNames(Generation generation, Cycle<?> cycle, List<String> elementNames, Predicate<String> elementNameFilter);

        boolean matchAnyElementName(Generation generation, Cycle<?> cycle, Predicate<String> elementNameFilter);

        CycleLevel cycleLevel();
    }

    interface TriConsumer<T, U, V> {
        void apply(T t, U u, V v);
    }
}
