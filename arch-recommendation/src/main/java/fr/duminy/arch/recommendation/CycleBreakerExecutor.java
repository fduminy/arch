package fr.duminy.arch.recommendation;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicInteger;

import static java.lang.Runtime.getRuntime;
import static java.util.concurrent.Executors.newFixedThreadPool;
import static java.util.concurrent.TimeUnit.SECONDS;

class CycleBreakerExecutor {
    static final String THREAD_NAME_PREFIX = "CycleBreaker-thread-";
    private final AtomicInteger threadNumber = new AtomicInteger(1);

    private final ExecutorService executor = newFixedThreadPool(
            getRuntime().availableProcessors(),
            runnable -> {
                Thread thread = new Thread(runnable);
                thread.setName(THREAD_NAME_PREFIX + threadNumber.getAndIncrement());
                return thread;
            });

    public void invokeAll(List<Callable<Void>> tasks) {
        try {
            executor.invokeAll(tasks);
        } catch (@SuppressWarnings("java:S2142") InterruptedException e) {
            // ignore
        }
    }

    public void shutdown() {
        executor.shutdown();
        boolean terminated = false;
        while (!terminated) {
            try {
                terminated = executor.awaitTermination(1, SECONDS);
            } catch (@SuppressWarnings("java:S2142") InterruptedException e) {
                // ignore
            }
        }
    }
}
