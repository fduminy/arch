package fr.duminy.arch.recommendation;

import fr.duminy.arch.api.ArchElement;
import fr.duminy.arch.api.Cycle;
import fr.duminy.arch.engine.Action;
import fr.duminy.arch.recommendation.CycleBreakerEngine.CycleBreakerLevel;

import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Predicate;

record CycleBreakerLevelImpl<E extends ArchElement>(
        BiFunction<Generation, ArchElement, List<? extends ArchElement>> childrenGetter,
        PossibleActionFinder<?, E>[] possibleActionFinders,
        CycleLevel cycleLevel)
        implements CycleBreakerLevel<E> {
    @Override
    public void transform(Generation generation, Cycle<E> cycle, Consumer<Generation> newGenerationConsumer) {
        for (PossibleActionFinder<?, E> possibleActionFinder : possibleActionFinders) {
            for (Action action : possibleActionFinder.getPossibleActions(generation, cycle)) {
                newGenerationConsumer.accept(CycleBreakerEngine.transform(generation, action));
            }
        }
    }

    @Override
    public void fillElementNames(Generation generation, Cycle<?> cycle, List<String> elementNames, Predicate<String> elementNameFilter) {
        fillElementNames(generation, cycle, elementNames, elementNameFilter, false);
    }

    @Override
    public boolean matchAnyElementName(Generation generation, Cycle<?> cycle, Predicate<String> elementNameFilter) {
        return fillElementNames(generation, cycle, null, elementNameFilter, true);
    }

    private boolean fillElementNames(Generation generation, Cycle<?> cycle, List<String> elementNames,
                                     Predicate<String> elementNameFilter, boolean stopOnFirstAdd) {
        boolean elementAdded = false;
        for (ArchElement parent : cycle) {
            for (ArchElement element : childrenGetter.apply(generation, parent)) {
                if (elementNameFilter.test(element.name())) {
                    elementAdded = true;
                    if (stopOnFirstAdd) {
                        break;
                    }
                    elementNames.add(element.name());
                }
            }
        }
        return elementAdded;
    }
}
