package fr.duminy.arch.recommendation;

import fr.duminy.arch.api.Recommendations;

public
class CycleBreakerRecommendations implements Recommendations {
    private final Iterable<RecommendationsForBreakingACycle> recommendations;

    CycleBreakerRecommendations(Iterable<RecommendationsForBreakingACycle> recommendations) {
        this.recommendations = recommendations;
    }

    public void accept(CycleBreakerRecommendationsVisitor visitor) {
        if (recommendations.iterator().hasNext()) {
            recommendations.forEach(recommendationsForACycle ->
                    recommendationsForACycle.accept(visitor));
        } else {
            visitor.visitNoCycle();
        }
        visitor.visitEnd();
    }
}
