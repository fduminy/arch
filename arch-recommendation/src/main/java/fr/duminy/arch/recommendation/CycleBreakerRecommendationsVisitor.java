package fr.duminy.arch.recommendation;

import fr.duminy.arch.api.Cycle;
import fr.duminy.arch.engine.Action;

public interface CycleBreakerRecommendationsVisitor {
    /**
     * Called when architecture contains has no cycles :-)
     */
    void visitNoCycle();

    /**
     * Called for each cycle found in the architecture.
     */
    void visitCycleSolutions(Cycle<?> cycle);

    /**
     * Called for each solution found to break the current cycle (given by last call to {@linkplain #visitCycleSolutions(Cycle)}).
     */
    void visitAPossibleSolution(Action... actions);

    /**
     * Always called after any other methods.
     */
    void visitEnd();
}
