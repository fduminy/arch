package fr.duminy.arch.recommendation;

import fr.duminy.arch.api.Cycle;
import fr.duminy.arch.recommendation.CycleBreakerEngine.CycleBreakerLevel;

import java.util.List;

@SuppressWarnings({"rawtypes", "unchecked"})
class CycleBreakingTask {
    private final GenerationPool generationPool;
    private final CycleBreakerLevel level;

    CycleBreakingTask(GenerationPool generationPool, CycleBreakerLevel level) {
        this.generationPool = generationPool;
        this.level = level;
    }

    @SuppressWarnings("SuspiciousMethodCalls")
    void tryToBreakCycle(List<String> initialCycleElements, Generation generation) {
        for (Cycle<?> cycle : generation.findCycles(level.cycleLevel())) {
            if (level.matchAnyElementName(generation, cycle, initialCycleElements::contains)) {
                level.transform(generation, cycle, g -> generationPool.add((Generation) g));
                break;
            }
        }
    }
}
