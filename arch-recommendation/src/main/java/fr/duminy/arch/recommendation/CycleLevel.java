package fr.duminy.arch.recommendation;

import fr.duminy.arch.api.ArchClass;
import fr.duminy.arch.api.ArchElement;
import fr.duminy.arch.api.ArchModule;
import fr.duminy.arch.api.ArchPackage;

public enum CycleLevel {
    MODULE(ArchModule.class),
    PACKAGE(ArchPackage.class),
    CLASS(ArchClass.class);

    private final Class<? extends ArchElement> cycleElementClass;

    CycleLevel(Class<? extends ArchElement> cycleElementClass) {
        this.cycleElementClass = cycleElementClass;
    }

    public Class<? extends ArchElement> getCycleElementClass() {
        return cycleElementClass;
    }
}
