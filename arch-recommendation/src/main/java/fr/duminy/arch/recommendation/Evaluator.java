package fr.duminy.arch.recommendation;

import java.util.EnumMap;
import java.util.Map;
import java.util.Set;

import static fr.duminy.arch.recommendation.CycleLevel.*;
import static java.util.Arrays.asList;
import static java.util.EnumSet.of;

class Evaluator {
    private final Set<CycleLevel> levels;
    private final Map<CycleLevel, Integer> levelWeights = new EnumMap<>(CycleLevel.class);

    Evaluator(CycleLevel cycleLevel, CycleLevel... cycleLevels) {
        levels = of(cycleLevel);
        levels.addAll(asList(cycleLevels));
        levelWeights.put(MODULE, 1_000_000);
        levelWeights.put(PACKAGE, 1_000);
        levelWeights.put(CLASS, 1);
    }

    final int eval(Generation generation) {
        int result = 0;
        for (CycleLevel level : levels) {
            result += levelWeights.get(level) * numberOfCycles(generation, level);
        }
        return result;
    }

    private int numberOfCycles(Generation generation, CycleLevel cycleLevel) {
        return generation.findCycles(cycleLevel).size();
    }
}
