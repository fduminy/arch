package fr.duminy.arch.recommendation;

import fr.duminy.arch.api.*;
import fr.duminy.arch.cycle.JGraphTFactory;
import fr.duminy.arch.engine.Action;

import java.util.Collection;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import static fr.duminy.arch.recommendation.NullAction.NULL_ACTION;
import static java.util.Collections.unmodifiableCollection;
import static java.util.Collections.unmodifiableList;

final class Generation {
    private final Generation previous;
    private final Action action;
    private final Architecture architecture;
    private final Map<CycleLevel, Collection<Cycle<? extends ArchElement>>> cyclesByType = new EnumMap<>(CycleLevel.class);
    private Graph graph;

    Generation(Generation previous, Action action, Architecture architecture) {
        this.previous = previous;
        this.action = action;
        this.architecture = architecture;
    }

    Generation previous() {
        return previous;
    }

    Action action() {
        return action;
    }

    Collection<Cycle<? extends ArchElement>> findCycles(CycleLevel cycleLevel) {
        if (graph == null) {
            graph = new JGraphTFactory().createGraph(architecture);
        }

        return cyclesByType.computeIfAbsent(cycleLevel, t -> unmodifiableCollection(graph.findCycles(cycleLevel.getCycleElementClass())));
    }

    List<ArchPackage> getPackages(ArchModule module) {
        return unmodifiableList(architecture.database().packages(module));
    }

    List<ArchClass> getClasses(ArchPackage pack) {
        return unmodifiableList(architecture.database().classes(pack));
    }

    ArchCollection<ArchModule> getModules() {
        return architecture.database().modules();
    }

    public ArchModule getModule(ArchPackage packag) {
        return architecture.database().module(packag);
    }

    ArchCollection<ArchPackage> getPackages() {
        return architecture.database().packages();
    }

    Architecture architecture() {
        return architecture;
    }

    void walkHistory(Predicate<Generation> shouldContinue) {
        Generation currentGeneration = this;
        while (currentGeneration != null) {
            if (!currentGeneration.action().equals(NULL_ACTION)) {
                if (!shouldContinue.test(currentGeneration)) {
                    break;
                }
            }
            currentGeneration = currentGeneration.previous();
        }
    }

    Generation getFirstGeneration() {
        Generation firstGeneration = this;
        while (firstGeneration.previous() != null) {
            firstGeneration = firstGeneration.previous();
        }
        return firstGeneration;
    }

    void invalidateCaches() {
        graph = null;
        cyclesByType.clear();
        architecture.database().invalidateCaches();
    }
}
