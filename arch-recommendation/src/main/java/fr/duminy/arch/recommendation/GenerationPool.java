package fr.duminy.arch.recommendation;

import fr.duminy.arch.api.ClassDatabase;
import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static java.lang.Integer.MAX_VALUE;
import static java.util.Collections.unmodifiableList;
import static org.slf4j.LoggerFactory.getLogger;

class GenerationPool {
    private static final Logger LOGGER = getLogger(GenerationPool.class);

    private final Set<ClassDatabase> oldDatabases = new HashSet<>();
    private final Generations currentGenerations = new Generations();
    private final Generations previousGenerations = new Generations();
    private final Evaluator evaluator;
    private int bestEvaluation = MAX_VALUE;

    GenerationPool(Generation generation, Evaluator evaluator) {
        this.evaluator = evaluator;
        add(generation);
    }

    List<Generation> previousGenerations() {
        return unmodifiableList(previousGenerations.values);
    }

    void moveCurrentGenerationsToPreviousGenerations() {
        previousGenerations.swapWith(currentGenerations);
        currentGenerations.clear();
        oldDatabases.addAll(previousGenerations.keys);
        bestEvaluation = MAX_VALUE;
    }

    List<Generation> currentGenerations() {
        return currentGenerations.generations();
    }

    boolean hasEvolved() {
        boolean shouldContinue;
        if (currentGenerations.size() == 0) {
            shouldContinue = false;
        } else {
            shouldContinue = !previousGenerations.equals(currentGenerations);
        }
        LOGGER.info("previousGenerations.size={} currentGenerations.size={} hasEvolved={}",
                previousGenerations.size(), currentGenerations.size(), shouldContinue);
        return shouldContinue;
    }

    void add(Generation generation) {
        if (oldDatabases.contains(generation.architecture().database())) {
            return;
        }
        int evaluation = evaluator.eval(generation); // lower values are better
        synchronized (currentGenerations) { // may also modify bestEvaluation
            if (evaluation > bestEvaluation) {
                // that evaluation is worse => forget this generation
                return;
            }
            if (evaluation < bestEvaluation) {
                // that evaluation is better => clear currentGenerations
                bestEvaluation = evaluation;
                currentGenerations.clear();
            }
            currentGenerations.addIfAbsent(generation);
        }
    }

    private static class Generations {
        private Set<ClassDatabase> keys = new HashSet<>();
        private List<Generation> values = new ArrayList<>();

        private void clear() {
            keys.clear();
            values.clear();
        }

        private void addIfAbsent(Generation generation) {
            if (keys.add(key(generation))) {
                values.add(generation);
            }
        }

        private int size() {
            return values.size();
        }

        private List<Generation> generations() {
            return unmodifiableList(values);
        }

        @SuppressWarnings({"EqualsWhichDoesntCheckParameterClass", "java:S2259",
                "java:S1206", "java:S2097"})
        @Override
        public boolean equals(Object o) {
            return keys.equals(((Generations) o).keys);
        }

        private ClassDatabase key(Generation nextGeneration) {
            return nextGeneration.architecture().database();
        }

        public void swapWith(Generations generations) {
            Set<ClassDatabase> tmpKeys = generations.keys;
            generations.keys = this.keys;
            this.keys = tmpKeys;

            List<Generation> tmpValues = generations.values;
            generations.values = this.values;
            this.values = tmpValues;
        }
    }
}
