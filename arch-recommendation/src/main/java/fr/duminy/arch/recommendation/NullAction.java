package fr.duminy.arch.recommendation;

import fr.duminy.arch.api.ArchElement;
import fr.duminy.arch.api.Architecture;
import fr.duminy.arch.engine.Action;

final class NullAction implements Action {
    static final NullAction NULL_ACTION = new NullAction();

    private NullAction() {
    }

    @Override
    public ArchElement subject() {
        return null;
    }

    @Override
    public Architecture transform(Architecture original) {
        return original;
    }
}
