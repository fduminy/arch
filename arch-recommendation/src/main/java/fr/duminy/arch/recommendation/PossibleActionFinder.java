package fr.duminy.arch.recommendation;

import fr.duminy.arch.api.ArchElement;
import fr.duminy.arch.api.Cycle;
import fr.duminy.arch.engine.Action;

import java.util.List;

interface PossibleActionFinder<A extends Action, E extends ArchElement> {
    List<A> getPossibleActions(Generation generation, Cycle<E> cycle);
}
