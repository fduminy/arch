package fr.duminy.arch.recommendation;

import fr.duminy.arch.api.*;
import fr.duminy.arch.engine.MoveClass;

import java.util.*;

class PossibleMoveClassActionFinder implements PossibleActionFinder<MoveClass, ArchPackage> {
    public List<MoveClass> getPossibleActions(Generation generation, Cycle<ArchPackage> packages) {
        Set<ArchPackage> packagesInCycle = new HashSet<>();
        boolean classCanGoOutOfItsModule = fillPackagesInCycle(packagesInCycle, generation, packages);

        List<MoveClass> possibleActions = new ArrayList<>();
        for (ArchPackage packageInCycle : packagesInCycle) {
            for (ArchClass classToMove : generation.getClasses(packageInCycle)) {
                fillPossibleActions(generation, packagesInCycle, classCanGoOutOfItsModule,
                        possibleActions, classToMove);
            }
        }
        return possibleActions;
    }

    private void fillPossibleActions(Generation generation, Set<ArchPackage> packagesInCycle, boolean classCanGoOutOfItsModule, List<MoveClass> possibleActions, ArchClass classToMove) {
        Collection<ArchPackage> possibleTargetPackages;
        if (classCanGoOutOfItsModule) {
            possibleTargetPackages = generation.getPackages().values();
        } else {
            possibleTargetPackages = packagesInCycle;
        }

        String initialClassPackage = generation.getFirstGeneration().architecture().database().packag(classToMove).name();
        ClassDatabase database = generation.architecture().database();
        ArchPackage classPackage = database.packag(classToMove);
        for (ArchPackage currentPackage : possibleTargetPackages) {
            if (!classCanGoOutOfItsModule && !Objects.equals(database.module(classToMove).name(), database.module(currentPackage).name())) {
                continue;
            }
            if (currentPackage.name().equals(classPackage.name()) ||
                    currentPackage.name().equals(initialClassPackage)) {
                continue;
            }
            ArchPackage packag = database.packag(classToMove);
            ArchClass clazz = database.classes().get(classToMove.name());
            possibleActions.add(new MoveClass(packag, clazz, currentPackage));
        }
    }

    @SuppressWarnings("CastCanBeRemovedNarrowingVariableType")
    private static boolean fillPackagesInCycle(Set<ArchPackage> packagesInCycle, Generation generation, Cycle<ArchPackage> packages) {
        boolean classCanGoOutOfItsModule = false;
        for (ArchElement element : packages) {
            if (element instanceof ArchModule) {
                classCanGoOutOfItsModule = true;
                packagesInCycle.addAll(generation.getPackages((ArchModule) element));
            } else {
                packagesInCycle.add((ArchPackage) element);
            }
        }
        return classCanGoOutOfItsModule;
    }

}
