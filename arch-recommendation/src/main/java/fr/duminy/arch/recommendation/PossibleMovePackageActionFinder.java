package fr.duminy.arch.recommendation;

import fr.duminy.arch.api.ArchModule;
import fr.duminy.arch.api.ArchPackage;
import fr.duminy.arch.api.Cycle;
import fr.duminy.arch.engine.MovePackage;

import java.util.ArrayList;
import java.util.List;

class PossibleMovePackageActionFinder implements PossibleActionFinder<MovePackage, ArchModule> {
    public List<MovePackage> getPossibleActions(Generation generation, Cycle<ArchModule> modules) {
        List<MovePackage> possibleActions = new ArrayList<>();
        for (ArchModule module : modules) {
            for (ArchPackage packageToMove : generation.getPackages(module)) {
                fillPossibleActions(generation, modules, possibleActions, packageToMove);
            }
        }
        return possibleActions;
    }

    private void fillPossibleActions(Generation generation, Cycle<ArchModule> modules, List<MovePackage> possibleActions, ArchPackage packageToMove) {
        String packageModule = generation.getModule(packageToMove).name();
        String initialPackageModule = initialPackageModule(generation, packageToMove);
        for (ArchModule archModule : modules) {
            String moduleName = archModule.name();
            if (!moduleName.equals(packageModule) && !moduleName.equals(initialPackageModule)) {
                ArchModule targetModule = generation.getModules().get(moduleName);
                possibleActions.add(new MovePackage(packageToMove, targetModule));
            }
        }
    }

    private String initialPackageModule(Generation generation, ArchPackage archPackage) {
        Generation firstGeneration = generation.getFirstGeneration();
        ArchPackage packag = firstGeneration.getPackages().get(archPackage.name());
        return generation.getModule(packag).name();
    }
}
