package fr.duminy.arch.recommendation;

import fr.duminy.arch.api.ArchElement;
import fr.duminy.arch.api.Cycle;
import fr.duminy.arch.api.Recommendations;
import fr.duminy.arch.engine.Action;

class RecommendationsForBreakingACycle implements Recommendations {
    private final Cycle<ArchElement> cycle;
    private final Iterable<Action[]> recommendations;

    RecommendationsForBreakingACycle(Cycle<ArchElement> cycle, Iterable<Action[]> recommendations) {
        this.cycle = cycle;
        this.recommendations = recommendations;
    }

    void accept(CycleBreakerRecommendationsVisitor visitor) {
        visitor.visitCycleSolutions(cycle);
        if (recommendations.iterator().hasNext()) {
            recommendations.forEach(visitor::visitAPossibleSolution);
        }
    }
}
