package fr.duminy.arch.recommendation;

import fr.duminy.arch.api.ArchClass;
import fr.duminy.arch.cycle.MockArchitectureForModule;
import fr.duminy.arch.recommendation.CycleBreakerEngine.CycleBreakerLevel;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.concurrent.TimeoutException;

import static fr.duminy.arch.recommendation.CycleBreakerExecutorTest.waitNoExecutorThread;
import static fr.duminy.arch.recommendation.CycleLevel.PACKAGE;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CycleBreakerEngineTest {
    @Test
    void findBestArchitectures_must_finally_shutdown_executor(@Mock CycleBreakerLevel<ArchClass> cycleBreakerLevel) throws InterruptedException, TimeoutException {
        when(cycleBreakerLevel.cycleLevel()).thenReturn(PACKAGE);
        MockArchitectureForModule mock = new MockArchitectureForModule("m1:x.A->y.B | m2:y.B->x.A");
        CycleBreakerEngine underTest = new CycleBreakerEngine(cycleBreakerLevel);
        waitNoExecutorThread();

        underTest.findBestArchitectures(mock.getMock());

        waitNoExecutorThread();
    }
}