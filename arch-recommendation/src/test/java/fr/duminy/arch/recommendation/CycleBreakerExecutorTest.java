package fr.duminy.arch.recommendation;

import org.assertj.core.api.SoftAssertions;
import org.assertj.core.api.junit.jupiter.SoftAssertionsExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Stream;

import static com.google.code.tempusfugit.temporal.Duration.seconds;
import static com.google.code.tempusfugit.temporal.Timeout.timeout;
import static com.google.code.tempusfugit.temporal.WaitFor.waitOrTimeout;
import static fr.duminy.arch.recommendation.CycleBreakerExecutor.THREAD_NAME_PREFIX;
import static java.lang.Thread.currentThread;
import static java.lang.Thread.getAllStackTraces;
import static java.util.Collections.singletonList;

@ExtendWith(SoftAssertionsExtension.class)
class CycleBreakerExecutorTest {
    @Test
    void invokeAll(SoftAssertions softly) throws InterruptedException, TimeoutException {
        AtomicReference<String> taskWasRunInThread = new AtomicReference<>("");
        Callable<Void> callable = () -> {
            taskWasRunInThread.set(currentThread().getName());
            return null;
        };
        CycleBreakerExecutor underTest = new CycleBreakerExecutor();

        underTest.invokeAll(singletonList(callable));
        softly.assertThat(taskWasRunInThread).hasValueMatching(CycleBreakerExecutorTest::isExecutorThread);
        softly.assertThat(threadNames()).anyMatch(CycleBreakerExecutorTest::isExecutorThread);

        underTest.shutdown();
        waitNoExecutorThread();
    }

    static void waitNoExecutorThread() throws InterruptedException, TimeoutException {
        waitOrTimeout(() -> threadNames().noneMatch(CycleBreakerExecutorTest::isExecutorThread), timeout(seconds(10)));
    }

    static boolean isExecutorThread(String threadName) {
        return threadName.startsWith(THREAD_NAME_PREFIX);
    }

    static Stream<String> threadNames() {
        return getAllStackTraces().keySet().stream().map(Thread::getName);
    }
}