package fr.duminy.arch.recommendation;

import fr.duminy.arch.api.ArchException;
import fr.duminy.arch.api.Architecture;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.junitpioneer.jupiter.cartesian.CartesianTest;
import org.junitpioneer.jupiter.cartesian.CartesianTest.Values;

import static fr.duminy.arch.recommendation.CycleBreakerBuilder.builder;
import static fr.duminy.arch.recommendation.CycleBreakerTest.asString;
import static fr.duminy.arch.recommendation.CycleLevel.MODULE;
import static fr.duminy.arch.recommendation.CycleLevel.PACKAGE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class CycleBreakerIT {
    @AfterAll
    static void afterAll() {
        System.err.println("freeMemory=" + Runtime.getRuntime().freeMemory());
    }

    @CartesianTest
    void find_without_specifying_level(@Values(booleans = {false, true}) boolean movePackage,
                                       @Values(booleans = {false, true}) boolean moveClass) {
        assertThatThrownBy(() -> builder().movePackage(movePackage).moveClass(moveClass).build())
                .isExactlyInstanceOf(ArchException.class)
                .hasMessageContaining("level of cycles to break not specified");
    }

    @Nested
    class Module {
        @ParameterizedTest
        @ValueSource(strings = {
                // without cycles
                "m1:x.A => no cycles",
                "m1:x.A->y.B | m1:y.B => no cycles",

                // with package cycles in single module
                "m1:x.A->y.B | m1:y.B->x.A => no cycles",

                // with one module cycles
                "m1:x.A->y.B | m2:y.B->x.A => (x->m2 | y->m1)",
                "m1:x.A->y.B | m2:y.B->z.C | m3:z.C->x.A => (x->m2,z->m2 | x->m3,y->m3 | y->m1,z->m1)",

                // with two module cycles
                "m1:u.A->v.B | m2:v.B->u.A | m3:w.C->x.D | m4:x.D->w.C => (u->m2 | v->m1) & (w->m4 | x->m3)",

                // with a package in two module cycles forming a bigger module cycle
                "m1:x.A->y.B | m2:y.B->x.A,z.C | m3:z.C->y.B => (x->m2,z->m2 | x->m3,y->m3 | y->m1,z->m1)",

                // with two module cycles linked by one dependency
                "m1:u.A->v.B | m2:v.B->u.A,w.C | m3:w.C->x.D | m4:x.D->w.C => (u->m2 | v->m1) & (w->m4 | x->m3)",

                // with many cycles that gave infinite loop previously but that have now solutions
                "mb:u.A->v.B,w.C | mp:v.B->u.A | mf:w.C->u.A,v.B,x.D | ms:x.D->w.C,u.A => (u->mp,w->mp,x->mp | v->mb,w->mb,x->mb)",

                // with each module linked to other modules
                "mb:u.A->v.B,w.C,x.D | mp:v.B->u.A,w.C,x.D | mf:w.C->u.A,v.B,x.D | ms:x.D->w.C,u.A,v.B => (u->mf,v->mf,x->mf | u->mp,w->mp,x->mp | u->ms,v->ms,w->ms | v->mb,w->mb,x->mb)"
        })
        void find_by_moving_only_packages(MockArchitectureWithActions mockArchitecture) throws ArchException {
            Architecture original = mockArchitecture.getReal();
            CycleBreaker underTest = builder().level(MODULE).movePackage(true).moveClass(false).build();

            CycleBreakerRecommendations recommendations = underTest.find(original);

            assertThat(asString(recommendations)).isEqualTo(mockArchitecture.getRawExpectedActions());
        }

        @ParameterizedTest
        @ValueSource(strings = {
                // without cycles
                "m1:x.A => no cycles",
                "m1:x.A->y.B | m1:y.B => no cycles",

                // with package cycles in single module
                "m1:x.A->y.B | m1:y.B->x.A => no cycles",

                // with one module cycles
                "m1:x.A->y.B | m2:y.B->x.A => (x.A->y | y.B->x)",
        })
        void find_by_moving_only_classes(MockArchitectureWithActions mockArchitecture) throws ArchException {
            Architecture original = mockArchitecture.getReal();
            CycleBreaker underTest = builder().level(MODULE).movePackage(false).moveClass(true).build();

            CycleBreakerRecommendations recommendations = underTest.find(original);

            assertThat(asString(recommendations)).isEqualTo(mockArchitecture.getRawExpectedActions());
        }

        @ParameterizedTest
        @ValueSource(strings = {
                // without cycles
                "m1:x.A => no cycles",
                "m1:x.A->y.B | m1:y.B => no cycles",

                // with package cycles in single module
                "m1:x.A->y.B | m1:y.B->x.A => no cycles",

                // with one module cycles
                "m1:x.A->y.B | m2:y.B->x.A => (x->m2 | x.A->y | y->m1 | y.B->x)",
        })
        void find_by_moving_packages_and_classes(MockArchitectureWithActions mockArchitecture) throws ArchException {
            Architecture original = mockArchitecture.getReal();
            CycleBreaker underTest = builder().level(MODULE).movePackage(true).moveClass(true).build();

            CycleBreakerRecommendations recommendations = underTest.find(original);

            assertThat(asString(recommendations)).isEqualTo(mockArchitecture.getRawExpectedActions());
        }

        @Test
        void find_by_moving_nothing() {
            assertThatThrownBy(() -> builder().level(MODULE).movePackage(false).moveClass(false).build())
                    .isExactlyInstanceOf(ArchException.class)
                    .hasMessageContaining("can't break module cycles when no moves are allowed");
        }

        @Nested
        class AutomaticallyGeneratedCyclesTest {
            private static final int BIG_CYCLE_SIZE = 7;

            private static final int SMALL_CYCLE_SIZE = 3;
            private static final int NUMBER_OF_SMALL_CYCLES = 200;

            @Test
            void find_one_big_cycle() throws ArchException {
                MockArchitectureWithActions mockArchitecture = new CycleBuilder(MODULE).addCycle(BIG_CYCLE_SIZE).build();

                Architecture original = mockArchitecture.getReal();
                CycleBreaker underTest = builder().level(MODULE).movePackage(true).moveClass(false).build();

                CycleBreakerRecommendations recommendations = underTest.find(original);

                assertThat(asString(recommendations)).isEqualTo(mockArchitecture.getRawExpectedActions());
            }

            @Test
            void find_many_small_cycles() throws ArchException {
                CycleBuilder cycleBuilder = new CycleBuilder(MODULE);
                for (int i = 0; i < NUMBER_OF_SMALL_CYCLES; i++) {
                    cycleBuilder.addCycle(SMALL_CYCLE_SIZE);
                }
                MockArchitectureWithActions mockArchitecture = cycleBuilder.build();

                Architecture original = mockArchitecture.getReal();
                CycleBreaker underTest = builder().level(MODULE).movePackage(true).moveClass(false).build();

                CycleBreakerRecommendations recommendations = underTest.find(original);

                assertThat(asString(recommendations)).isEqualTo(mockArchitecture.getRawExpectedActions());
            }
        }
    }

    @Nested
    class Package {

        @ParameterizedTest
        @ValueSource(strings = {
                // no package cycles inside a module
                "m1:u.A->u.B | m1:u.B->u.A => no cycles",

                // package cycles inside a module
                "m1:u.A->v.B | m1:v.B->u.A => (u.A->v | v.B->u)",

                // packages won't go out of their module to break package cycles
                "m1:u.A->v.B | m2:v.B->u.A => no solution",
                "m1:u.A->v.B | m2:v.B->w.C | m3:w.C->u.A => no solution",
        })
        void find(MockArchitectureWithActions mockArchitecture) throws ArchException {
            Architecture original = mockArchitecture.getReal();
            CycleBreaker underTest = builder().level(PACKAGE).moveClass(true).build();

            CycleBreakerRecommendations recommendations = underTest.find(original);

            assertThat(asString(recommendations)).isEqualTo(mockArchitecture.getRawExpectedActions());
        }

        @ParameterizedTest
        @ValueSource(booleans = {false, true})
        void find_by_moving_package(boolean moveClass) {
            assertThatThrownBy(() -> builder().level(PACKAGE).movePackage(true).moveClass(moveClass).build())
                    .isExactlyInstanceOf(ArchException.class)
                    .hasMessageContaining("can't break package cycles by moving package");
        }

        @Nested
        class AutomaticallyGeneratedCyclesTest {
            private static final int BIG_CYCLE_SIZE = 7;

            private static final int SMALL_CYCLE_SIZE = 3;
            private static final int NUMBER_OF_SMALL_CYCLES = 200;

            @Test
            void find_one_big_cycle() throws ArchException {
                MockArchitectureWithActions mockArchitecture = new CycleBuilder(PACKAGE).addCycle(BIG_CYCLE_SIZE).build();
                Architecture original = mockArchitecture.getReal();
                CycleBreaker underTest = builder().level(PACKAGE).moveClass(true).build();

                CycleBreakerRecommendations recommendations = underTest.find(original);

                assertThat(asString(recommendations)).isEqualTo(mockArchitecture.getRawExpectedActions());
            }

            @Test
            void find_many_small_cycles() throws ArchException {
                CycleBuilder cycleBuilder = new CycleBuilder(PACKAGE);
                for (int i = 0; i < NUMBER_OF_SMALL_CYCLES; i++) {
                    cycleBuilder.addCycle(SMALL_CYCLE_SIZE);
                }
                MockArchitectureWithActions mockArchitecture = cycleBuilder.build();
                Architecture original = mockArchitecture.getReal();
                CycleBreaker underTest = builder().level(PACKAGE).moveClass(true).build();

                CycleBreakerRecommendations recommendations = underTest.find(original);

                assertThat(asString(recommendations)).isEqualTo(mockArchitecture.getRawExpectedActions());
            }
        }
    }
}