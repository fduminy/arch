package fr.duminy.arch.recommendation;

import fr.duminy.arch.api.ArchElement;
import fr.duminy.arch.api.Cycle;
import fr.duminy.arch.engine.Action;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static java.util.List.of;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
class CycleBreakerRecommendationsTest {
    @Mock
    private CycleBreakerRecommendationsVisitor visitor;
    @Mock
    private Cycle<ArchElement> cycle1;
    @Mock
    private Action cycle1Solution1Action;
    @Mock
    private Action cycle1Solution2Action;
    @Mock
    private Cycle<ArchElement> cycle2;
    @Mock
    private Action cycle2SolutionAction1;
    @Mock
    private Action cycle2SolutionAction2;

    @Test
    void accept() {
        List<RecommendationsForBreakingACycle> recommendations = new ArrayList<>();
        recommendations.add(new RecommendationsForBreakingACycle(cycle1, of(
                new Action[]{cycle1Solution1Action},
                new Action[]{cycle1Solution2Action}
        )));
        recommendations.add(new RecommendationsForBreakingACycle(cycle2, singletonList(
                new Action[]{cycle2SolutionAction1, cycle2SolutionAction2
                })));

        new CycleBreakerRecommendations(recommendations).accept(visitor);

        InOrder inOrder = inOrder(visitor);
        inOrder.verify(visitor).visitCycleSolutions(cycle1);
        inOrder.verify(visitor).visitAPossibleSolution(cycle1Solution1Action);
        inOrder.verify(visitor).visitAPossibleSolution(cycle1Solution2Action);
        inOrder.verify(visitor).visitCycleSolutions(cycle2);
        inOrder.verify(visitor).visitAPossibleSolution(cycle2SolutionAction1, cycle2SolutionAction2);
        inOrder.verify(visitor).visitEnd();
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    void accept_no_cycles() {
        new CycleBreakerRecommendations(emptyList()).accept(visitor);

        InOrder inOrder = inOrder(visitor);
        inOrder.verify(visitor).visitNoCycle();
        inOrder.verify(visitor).visitEnd();
        inOrder.verifyNoMoreInteractions();
    }

    @SuppressWarnings("unchecked")
    @Test
    void accept_no_solutions() {
        List<RecommendationsForBreakingACycle> recommendations = new ArrayList<>();
        Cycle<ArchElement> cycleWithoutSolution = mock(Cycle.class);
        recommendations.add(new RecommendationsForBreakingACycle(cycle1, singletonList(
                new Action[]{cycle1Solution1Action})));
        recommendations.add(new RecommendationsForBreakingACycle(cycleWithoutSolution, emptyList()));
        recommendations.add(new RecommendationsForBreakingACycle(cycle2, singletonList(
                new Action[]{cycle2SolutionAction1})));

        new CycleBreakerRecommendations(recommendations).accept(visitor);

        InOrder inOrder = inOrder(visitor);
        inOrder.verify(visitor).visitCycleSolutions(cycle1);
        inOrder.verify(visitor).visitAPossibleSolution(cycle1Solution1Action);
        inOrder.verify(visitor).visitCycleSolutions(cycleWithoutSolution);
        inOrder.verify(visitor).visitCycleSolutions(cycle2);
        inOrder.verify(visitor).visitAPossibleSolution(cycle2SolutionAction1);
        inOrder.verify(visitor).visitEnd();
        inOrder.verifyNoMoreInteractions();
    }
}