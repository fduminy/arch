package fr.duminy.arch.recommendation;

import fr.duminy.arch.api.ArchElement;
import fr.duminy.arch.api.Architecture;
import fr.duminy.arch.api.Cycle;
import fr.duminy.arch.cycle.CyclesDescription.MockCycle;
import fr.duminy.arch.engine.Action;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import static fr.duminy.arch.recommendation.CycleBreakerTest.AsStringVisitor.*;
import static java.util.Arrays.asList;
import static java.util.Arrays.stream;
import static java.util.Collections.emptyMap;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.joining;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CycleBreakerTest {
    @Mock
    private CycleBreakerEngine engine;
    @Mock
    private Architecture architecture;
    @InjectMocks
    private CycleBreaker underTest;

    @Test
    void without_cycle() {
        when(engine.findBestArchitectures(architecture)).thenReturn(emptyMap());

        CycleBreakerRecommendations recommendations = underTest.find(architecture);

        assertThat(asString(recommendations)).isEqualTo(NO_CYCLES);
    }

    @Nested
    @SuppressWarnings("JUnitMalformedDeclaration")
    class OneCycle {
        @Test
        void without_solution(@Mock Generation generation) {
            Map<Cycle<?>, List<Generation>> bestArchitectures = new HashMap<>();
            bestArchitectures.put(mockCycle(), singletonList(generation));
            when(engine.findBestArchitectures(architecture)).thenReturn(bestArchitectures);

            CycleBreakerRecommendations recommendations = underTest.find(architecture);

            assertThat(asString(recommendations)).isEqualTo(NO_SOLUTION);
        }

        @Test
        void one_solution_and_one_action(@Mock Action action1) {
            Map<Cycle<?>, List<Generation>> bestArchitectures = new HashMap<>();
            bestArchitectures.put(mockCycle(), singletonList(mockBestArchitecture(action1)));
            when(engine.findBestArchitectures(architecture)).thenReturn(bestArchitectures);

            CycleBreakerRecommendations recommendations = underTest.find(architecture);

            assertThat(asString(recommendations)).isEqualTo(expectedSolution(action1));
        }

        @Test
        void one_solution_and_two_actions(@Mock Action action1, @Mock Action action2) {
            Map<Cycle<?>, List<Generation>> bestArchitectures = new HashMap<>();
            bestArchitectures.put(mockCycle(), singletonList(mockBestArchitecture(action1, action2)));
            when(engine.findBestArchitectures(architecture)).thenReturn(bestArchitectures);

            CycleBreakerRecommendations recommendations = underTest.find(architecture);

            assertThat(asString(recommendations)).isEqualTo(expectedSolution(action1, action2));
        }

        @Test
        void two_solutions_and_one_action_each(@Mock Action action1, @Mock Action action2) {
            Map<Cycle<?>, List<Generation>> bestArchitectures = new HashMap<>();
            bestArchitectures.put(mockCycle(), asList(
                    mockBestArchitecture(1, action1),
                    mockBestArchitecture(2, action2)));
            when(engine.findBestArchitectures(architecture)).thenReturn(bestArchitectures);

            CycleBreakerRecommendations recommendations = underTest.find(architecture);

            assertThat(asString(recommendations)).isEqualTo(BEGIN_OF_SOLUTION + action1
                    + SOLUTION_SEPARATOR + action2 + END_OF_SOLUTION);
        }
    }

    @Nested
    @SuppressWarnings("JUnitMalformedDeclaration")
    class TwoCycles {
        @Test
        void without_solution(@Mock Generation generation) {
            Map<Cycle<?>, List<Generation>> bestArchitectures = new HashMap<>();
            bestArchitectures.put(mockCycle(), singletonList(generation));
            bestArchitectures.put(mockCycle(), singletonList(generation));
            when(engine.findBestArchitectures(architecture)).thenReturn(bestArchitectures);

            CycleBreakerRecommendations recommendations = underTest.find(architecture);

            assertThat(asString(recommendations))
                    .isEqualTo(NO_SOLUTION + CYCLE_SEPARATOR + NO_SOLUTION);
        }

        @Test
        void one_solution_and_one_action(@Mock Action action1, @Mock Action action2) {
            Map<Cycle<?>, List<Generation>> bestArchitectures = new HashMap<>();
            bestArchitectures.put(mockCycle(), singletonList(mockBestArchitecture(1, action1)));
            bestArchitectures.put(mockCycle(), singletonList(mockBestArchitecture(2, action2)));
            when(engine.findBestArchitectures(architecture)).thenReturn(bestArchitectures);

            CycleBreakerRecommendations recommendations = underTest.find(architecture);

            assertThat(asString(recommendations).split(CYCLE_SEPARATOR)).containsExactlyInAnyOrder(
                    expectedSolution(action1), expectedSolution(action2));
        }

        @Test
        void one_solution_and_two_actions(@Mock Action action1, @Mock Action action2,
                                          @Mock Action action3, @Mock Action action4) {
            Map<Cycle<?>, List<Generation>> bestArchitectures = new HashMap<>();
            bestArchitectures.put(mockCycle(), singletonList(mockBestArchitecture(1, action1, action2)));
            bestArchitectures.put(mockCycle(), singletonList(mockBestArchitecture(3, action3, action4)));
            when(engine.findBestArchitectures(architecture)).thenReturn(bestArchitectures);

            CycleBreakerRecommendations recommendations = underTest.find(architecture);

            assertThat(asString(recommendations).split(CYCLE_SEPARATOR)).containsExactlyInAnyOrder(
                    expectedSolution(action1, action2), expectedSolution(action3, action4));
        }

        @Test
        void two_solutions_and_one_action_each(@Mock Action action1, @Mock Action action2,
                                               @Mock Action action3, @Mock Action action4) {
            Map<Cycle<?>, List<Generation>> bestArchitectures = new HashMap<>();
            bestArchitectures.put(mockCycle(), asList(
                    mockBestArchitecture(1, action1),
                    mockBestArchitecture(2, action2)));
            bestArchitectures.put(mockCycle(), asList(
                    mockBestArchitecture(3, action3),
                    mockBestArchitecture(4, action4)));
            when(engine.findBestArchitectures(architecture)).thenReturn(bestArchitectures);

            CycleBreakerRecommendations recommendations = underTest.find(architecture);

            assertThat(asString(recommendations).split(CYCLE_SEPARATOR)).containsExactlyInAnyOrder(
                    expectedSolutions(action1, action2),
                    expectedSolutions(action3, action4));
        }
    }

    private static MockCycle<ArchElement> mockCycle() {
        MockCycle<ArchElement> archElements = new MockCycle<>();
        archElements.add(mock(ArchElement.class));
        return archElements;
    }

    private static Generation mockBestArchitecture(Action... actions) {
        return mockBestArchitecture(1, actions);
    }

    private static Generation mockBestArchitecture(int firstActionNum, Action... actions) {
        Generation bestArchitecture = mock(Generation.class);
        int[] counter = new int[]{firstActionNum};
        List<Generation> previousGenerations = stream(actions).map(action -> {
            Generation generation = mock(Generation.class);
            int actionNum = counter[0]++;
            when(action.toString()).thenReturn("action" + actionNum);
            when(action.subject()).thenReturn(mock(ArchElement.class));
            when(generation.action()).thenReturn(action);
            return generation;
        }).toList();
        doAnswer(args -> {
            Predicate<Generation> shouldContinue = args.getArgument(0);
            previousGenerations.forEach(shouldContinue::test);
            return null;
        }).when(bestArchitecture).walkHistory(any());
        return bestArchitecture;
    }

    private static String expectedSolution(Action... actions) {
        return stream(actions).map(Action::toString)
                .collect(joining(ACTION_DELIMITER, BEGIN_OF_SOLUTION, END_OF_SOLUTION));
    }

    private static String expectedSolutions(Action... actions) {
        return stream(actions).map(Action::toString)
                .collect(joining(SOLUTION_SEPARATOR, BEGIN_OF_SOLUTION, END_OF_SOLUTION));
    }

    static class AsStringVisitor extends AbstractCycleBreakerRecommendationsVisitor {
        static final String SOLUTION_SEPARATOR = " | ";
        static final String CYCLE_SEPARATOR = " & ";
        static final String BEGIN_OF_SOLUTION = "(";
        static final String END_OF_SOLUTION = ")";
        static final String NO_CYCLES = "no cycles";
        static final String NO_SOLUTION = "no solution";

        private boolean firstCycle = true;
        private boolean firstSolution = true;

        private AsStringVisitor(StringBuilder buffer) {
            super(buffer);
        }

        @Override
        public void visitNoCycle() {
            append(NO_CYCLES);
        }

        @Override
        protected void startCycle(Cycle<?> currentCycle) {
            if (!firstCycle) {
                append(CYCLE_SEPARATOR);
            }
            firstCycle = false;
            firstSolution = true;
        }

        @Override
        protected void startSolution(Action... aSolution) {
            if (!firstSolution) {
                append(SOLUTION_SEPARATOR);
            } else {
                append(BEGIN_OF_SOLUTION);
            }
            firstSolution = false;

            append(asString(aSolution));
        }

        @Override
        protected void endCycle() {
            append(END_OF_SOLUTION);
        }

        @Override
        protected void endCycleWithoutSolution() {
            append(NO_SOLUTION);
        }
    }

    static String asString(CycleBreakerRecommendations recommendations) {
        StringBuilder buffer = new StringBuilder();
        recommendations.accept(new AsStringVisitor(buffer));
        return stream(buffer.toString().split(CYCLE_SEPARATOR))
                .sorted().collect(joining(CYCLE_SEPARATOR));
    }
}