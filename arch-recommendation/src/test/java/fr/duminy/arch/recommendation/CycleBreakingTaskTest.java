package fr.duminy.arch.recommendation;

import fr.duminy.arch.api.ArchElement;
import fr.duminy.arch.api.Cycle;
import fr.duminy.arch.recommendation.CycleBreakerEngine.CycleBreakerLevel;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

import static fr.duminy.arch.recommendation.CycleLevel.MODULE;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.mockito.Mockito.*;

@SuppressWarnings({"unchecked", "rawtypes"})
@ExtendWith(MockitoExtension.class)
class CycleBreakingTaskTest {

    @Mock
    private GenerationPool generationPool;
    @Mock
    private CycleBreakerLevel level;
    @Mock
    private Generation generation;
    @Mock
    private Generation newGeneration;
    @Mock
    private Cycle<ArchElement> cycle;
    @Mock
    private List<String> initialCycleElements;
    @InjectMocks
    private CycleBreakingTask underTest;

    @Test
    void tryToBreakCycle_without_cycle() {
        when(level.cycleLevel()).thenReturn(MODULE);
        when(generation.findCycles(MODULE)).thenReturn(singletonList(cycle));

        underTest.tryToBreakCycle(emptyList(), generation);

        verifyNoInteractions(generationPool);
    }

    @Test
    void tryToBreakCycle_with_one_cycle() {
        when(initialCycleElements.contains("m1")).thenReturn(true);
        when(level.cycleLevel()).thenReturn(MODULE);
        doAnswer(args -> args.getArgument(2, Predicate.class).test("m1"))
                .when(level).matchAnyElementName(eq(generation), eq(cycle), any());
        doAnswer(args -> {
            args.getArgument(2, Consumer.class).accept(newGeneration);
            return null;
        }).when(level).transform(eq(generation), eq(cycle), any());
        when(generation.findCycles(MODULE)).thenReturn(singletonList(cycle));

        underTest.tryToBreakCycle(initialCycleElements, generation);

        verify(generationPool).add(newGeneration);
        verifyNoMoreInteractions(generationPool);
    }
}