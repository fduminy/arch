package fr.duminy.arch.recommendation;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.function.Supplier;
import java.util.stream.IntStream;

import static fr.duminy.arch.recommendation.CycleBreakerTest.AsStringVisitor.CYCLE_SEPARATOR;
import static fr.duminy.arch.recommendation.CycleBreakerTest.AsStringVisitor.SOLUTION_SEPARATOR;
import static fr.duminy.arch.recommendation.CycleLevel.MODULE;
import static java.lang.String.format;
import static java.lang.String.join;
import static java.util.stream.IntStream.range;

class CycleBuilder {
    private final StringBuilder architecture = new StringBuilder();
    private final SortedSet<String> expectedActions = new TreeSet<>();
    private final CycleLevel cycleLevel;
    private final List<Cycle> cycles = new ArrayList<>();
    private int startIndex = 0;

    CycleBuilder(CycleLevel cycleLevel) {
        this.cycleLevel = cycleLevel;
    }

    CycleBuilder addCycle(int cycleSize) {
        int startIndexInclusive = startIndex;
        int endIndexExclusive = startIndex + cycleSize;
        startIndex = endIndexExclusive;
        cycles.add(new Cycle(startIndexInclusive, endIndexExclusive, cycleSize));
        return this;
    }

    MockArchitectureWithActions build() {
        int nbDigits = Integer.toString(startIndex - 1).length();
        String indexPattern = "%0" + nbDigits + 'd';
        String classPattern = format("m%s:p%s.C%s->p%s.C%s", indexPattern, indexPattern, indexPattern, indexPattern, indexPattern);
        String movePackagePattern = format("p%s->m%s", indexPattern, indexPattern);
        String moveClassPattern = format("p%s.C%s->p%s", indexPattern, indexPattern, indexPattern);

        for (Cycle cycle : cycles) {
            Supplier<IntStream> rangeSupplier = () -> range(cycle.startIndexInclusive(), cycle.endIndexExclusive());

            rangeSupplier.get().forEach(index -> {
                if (!architecture.isEmpty()) {
                    architecture.append('|');
                }
                int nextIndex = cycle.startIndexInclusive() + ((index - cycle.startIndexInclusive()) + 1) % cycle.size();
                int moduleIndex = MODULE.equals(cycleLevel) ? index : 1;
                architecture.append(format(classPattern, moduleIndex, index, index, nextIndex, nextIndex));
            });

            // add expected solutions
            SortedSet<String> solutions = new TreeSet<>();
            rangeSupplier.get().forEach(targetIndex -> {
                StringBuilder solution = new StringBuilder();
                rangeSupplier.get().forEach(sourceIndex -> {
                    if (sourceIndex == targetIndex) {
                        return;
                    }
                    if (!solution.isEmpty()) {
                        solution.append(',');
                    }
                    String format = MODULE.equals(cycleLevel) ?
                            format(movePackagePattern, sourceIndex, targetIndex) :
                            format(moveClassPattern, sourceIndex, sourceIndex, targetIndex);
                    solution.append(format);
                });
                solutions.add(solution.toString());
            });
            expectedActions.add('(' + join(SOLUTION_SEPARATOR, solutions) + ')');
        }
        String definition = architecture + "=>" + join(CYCLE_SEPARATOR, expectedActions);
        return new MockArchitectureWithActions(definition);
    }

    private record Cycle(int startIndexInclusive, int endIndexExclusive, int size) {
    }
}
