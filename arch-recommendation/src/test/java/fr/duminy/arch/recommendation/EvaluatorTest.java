package fr.duminy.arch.recommendation;

import fr.duminy.arch.api.ArchElement;
import fr.duminy.arch.api.Cycle;
import fr.duminy.arch.cycle.CyclesDescription.MockCycle;
import fr.duminy.arch.model.DefaultArchClass;
import fr.duminy.arch.model.DefaultArchModule;
import fr.duminy.arch.model.DefaultArchPackage;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junitpioneer.jupiter.params.IntRangeSource;

import java.util.ArrayList;
import java.util.Collection;
import java.util.function.BiFunction;

import static fr.duminy.arch.recommendation.CycleLevel.*;
import static java.util.stream.IntStream.range;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class EvaluatorTest {
    @ParameterizedTest
    @IntRangeSource(from = 0, to = 3)
    void eval_module_cycle(int numberOfCycles) {
        eval(numberOfCycles, MODULE, 1_000_000, DefaultArchModule::new);
    }

    @ParameterizedTest
    @IntRangeSource(from = 0, to = 3)
    void eval_package_cycle(int numberOfCycles) {
        eval(numberOfCycles, PACKAGE, 1_000, DefaultArchPackage::new);
    }

    @ParameterizedTest
    @IntRangeSource(from = 0, to = 3)
    void eval_class_cycle(int numberOfCycles) {
        eval(numberOfCycles, CLASS, 1, DefaultArchClass::new);
    }

    @Test
    void eval_multiple_level_cycles() {
        Generation generation = mock(Generation.class);
        when(generation.findCycles(MODULE)).thenReturn(buildCycles(2, DefaultArchModule::new));
        when(generation.findCycles(PACKAGE)).thenReturn(buildCycles(3, DefaultArchPackage::new));
        when(generation.findCycles(CLASS)).thenReturn(buildCycles(4, DefaultArchClass::new));

        assertThat(new Evaluator(MODULE, PACKAGE, CLASS).eval(generation))
                .isEqualTo(2_003_004);
    }

    private void eval(int numberOfCycles, CycleLevel cycleLevel, int weight,
                      BiFunction<Integer, String, ? extends ArchElement> factory) {
        Generation generation = mock(Generation.class);
        Collection<Cycle<? extends ArchElement>> cycles = buildCycles(numberOfCycles, factory);
        when(generation.findCycles(cycleLevel)).thenReturn(cycles);

        assertThat(new Evaluator(cycleLevel).eval(generation))
                .isEqualTo(numberOfCycles * weight);
    }

    private static Collection<Cycle<? extends ArchElement>> buildCycles(int numberOfCycles, BiFunction<Integer, String, ? extends ArchElement> factory) {
        Collection<Cycle<? extends ArchElement>> cycles = new ArrayList<>();
        for (int cycleNum = 0; cycleNum < numberOfCycles; cycleNum++) {
            MockCycle<ArchElement> cycle = new MockCycle<>();
            range(1, 2).forEach(n ->
                    cycle.add(factory.apply(n, Integer.toString(n))));
            cycles.add(cycle);
        }
        return cycles;
    }
}