package fr.duminy.arch.recommendation;

import fr.duminy.arch.cycle.Expectation;

class ExpectedActions extends Expectation {
    ExpectedActions(String expectation) {
        super(expectation);
    }

    @Override
    protected void parse() {
    }
}
