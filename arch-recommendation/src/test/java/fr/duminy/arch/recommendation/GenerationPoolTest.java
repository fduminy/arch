package fr.duminy.arch.recommendation;

import fr.duminy.arch.api.*;
import org.assertj.core.api.SoftAssertions;
import org.assertj.core.api.junit.jupiter.SoftAssertionsExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.function.Consumer;

import static org.mockito.Mockito.when;

@ExtendWith({MockitoExtension.class, SoftAssertionsExtension.class})
class GenerationPoolTest {
    public static final int FIRST_EVALUATION = 100;
    @Mock
    private Generation generation;
    @Mock
    private Architecture architecture;
    @Mock
    private Evaluator evaluator;

    private ClassDatabaseWithId database;
    private GenerationPool underTest;

    @BeforeEach
    void setUp() {
        database = setUpGeneration(generation, architecture);
        underTest = new GenerationPool(generation, evaluator);
    }

    @Test
    void constructor(SoftAssertions softly) {
        softly.assertThat(underTest.previousGenerations()).isEmpty();
        softly.assertThat(underTest.currentGenerations()).containsExactly(generation);
        softly.assertThat(underTest.hasEvolved()).isTrue();
    }

    @Test
    void hasEvolved_after_construction(SoftAssertions softly) {
        softly.assertThat(underTest.hasEvolved()).isTrue();

        softly.assertThat(underTest.previousGenerations()).isEmpty();
        softly.assertThat(underTest.currentGenerations()).containsExactly(generation);
    }

    @Test
    void add_database_with_better_evaluation(@Mock Generation generation2, @Mock Architecture architecture2, SoftAssertions softly) {
        setUpGeneration(generation2, FIRST_EVALUATION - 1/*better eval*/, architecture2, database.id + 1/*different id*/);

        underTest.add(generation2);

        softly.assertThat(underTest.previousGenerations()).isEmpty();
        softly.assertThat(underTest.currentGenerations()).containsExactly(generation2);
        softly.assertThat(underTest.hasEvolved()).isTrue();
    }

    @Test
    void add_database_with_worse_evaluation(@Mock Generation generation2,
                                            @Mock Architecture architecture2,
                                            @Mock ClassDatabase database2,
                                            SoftAssertions softly) {
        when(architecture2.database()).thenReturn(database2);
        when(generation2.architecture()).thenReturn(architecture2);
        when(evaluator.eval(generation2)).thenReturn(FIRST_EVALUATION + 1/*worse eval*/);

        underTest.add(generation2);

        softly.assertThat(underTest.previousGenerations()).isEmpty();
        softly.assertThat(underTest.currentGenerations()).containsExactly(generation);
        softly.assertThat(underTest.hasEvolved()).isTrue();
    }

    @Test
    void add_database_with_same_evaluation_and_same_id(@Mock Generation generation2, @Mock Architecture architecture2, SoftAssertions softly) {
        setUpGeneration(generation2, FIRST_EVALUATION/*same eval*/, architecture2, database.id/*same id*/);

        underTest.add(generation2);

        softly.assertThat(underTest.previousGenerations()).isEmpty();
        softly.assertThat(underTest.currentGenerations()).containsExactly(generation);
        softly.assertThat(underTest.hasEvolved()).isTrue();
    }

    @Test
    void add_database_with_same_evaluation_and_different_id(@Mock Generation generation2, @Mock Architecture architecture2, SoftAssertions softly) {
        setUpGeneration(generation2, FIRST_EVALUATION/*same eval*/, architecture2, database.id + 1/*different id*/);

        underTest.add(generation2);

        softly.assertThat(underTest.previousGenerations()).isEmpty();
        softly.assertThat(underTest.currentGenerations()).containsExactly(generation, generation2);
        softly.assertThat(underTest.hasEvolved()).isTrue();
    }

    @Test
    void add_same_database_as_previous_generation(@Mock Generation generation2,
                                                  @Mock Architecture architecture2,
                                                  SoftAssertions softly) {
        underTest.moveCurrentGenerationsToPreviousGenerations();
        when(generation2.architecture()).thenReturn(architecture2);
        when(architecture2.database()).thenReturn(database);

        underTest.add(generation2);

        softly.assertThat(underTest.previousGenerations()).containsExactly(generation);
        softly.assertThat(underTest.currentGenerations()).isEmpty();
        softly.assertThat(underTest.hasEvolved()).isFalse();
    }

    @Test
    void moveCurrentGenerationsToPreviousGenerations(SoftAssertions softly) {
        underTest.moveCurrentGenerationsToPreviousGenerations();

        softly.assertThat(underTest.previousGenerations()).containsExactly(generation);
        softly.assertThat(underTest.currentGenerations()).isEmpty();
        softly.assertThat(underTest.hasEvolved()).isFalse();
    }

    ClassDatabaseWithId setUpGeneration(Generation generation, Architecture architecture) {
        return setUpGeneration(generation, FIRST_EVALUATION, architecture, null);
    }

    ClassDatabaseWithId setUpGeneration(Generation generation, int evaluation, Architecture architecture, Integer databaseId) {
        ClassDatabaseWithId database = new ClassDatabaseWithId(databaseId);
        when(evaluator.eval(generation)).thenReturn(evaluation);
        when(generation.architecture()).thenReturn(architecture);
        when(architecture.database()).thenReturn(database);
        return database;
    }

    private static class ClassDatabaseWithId implements ClassDatabase {
        private static int NEXT_ID = FIRST_EVALUATION;
        private final int id;

        public ClassDatabaseWithId(Integer databaseId) {
            id = (databaseId == null) ? NEXT_ID++ : databaseId;
        }

        @Override
        public ArchCollection<ArchClass> classes() {
            return null;
        }

        @Override
        public List<ArchClass> classes(ArchPackage packag) {
            return null;
        }

        @Override
        public void forEachUsedClass(ArchClass archClass, Consumer<ArchClass> usedClassConsumer) {
        }

        @Override
        public int[][] classDependencyIndexes() {
            return null;
        }

        @Override
        public ArchCollection<ArchPackage> packages() {
            return null;
        }

        @Override
        public List<ArchPackage> packages(ArchModule module) {
            return null;
        }

        @Override
        public ArchPackage packag(ArchClass archClass) {
            return null;
        }

        @Override
        public ArchCollection<ArchModule> modules() {
            return null;
        }

        @Override
        public ArchModule module(ArchPackage packag) {
            return null;
        }

        @Override
        public ArchModule module(ArchClass archClass) {
            return null;
        }

        @Override
        public void invalidateCaches() {
        }

        @Override
        public int hashCode() {
            return Integer.hashCode(id);
        }

        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof ClassDatabaseWithId)) {
                return false;
            }
            return ((ClassDatabaseWithId) obj).id == id;
        }
    }
}