package fr.duminy.arch.recommendation;

import fr.duminy.arch.api.*;
import fr.duminy.arch.cycle.ArchitectureDescription;
import fr.duminy.arch.cycle.MockArchitecture;
import fr.duminy.arch.engine.DefaultSourceArchitectureBuilder;

import java.nio.file.Path;

import static fr.duminy.arch.cycle.MockArchitectureForModule.completeModuleMocking;
import static fr.duminy.arch.utils.ClassUtils.getPackageName;
import static fr.duminy.arch.utils.ClassUtils.getSimpleName;
import static java.nio.file.Paths.get;
import static org.mockito.Mockito.lenient;

class MockArchitectureWithActions extends MockArchitecture<ArchClass, ExpectedActions> {
    private String expectedActions;

    MockArchitectureWithActions(String architecture) {
        super(architecture);
    }

    @Override
    final protected ExpectedActions createExpectation(String expectation) {
        expectedActions = expectation;
        return new ExpectedActions(expectation);
    }

    @Override
    protected void completeMocking(ClassDatabase database, String moduleName, String className, ArchClass archClass) {
        lenient().when(archClass.name()).thenReturn(getSimpleName(className));
        completeModuleMocking(database, getNameToModule(), getNameToPackage(), moduleName, className, archClass);
    }

    @Override
    protected ArchClass getElementByName(String name) {
        return getNameToClass().get(name);
    }

    public String getRawExpectedActions() {
        return expectedActions;
    }

    public String[] getExpectedActions() {
        return expectedActions.isBlank() ? new String[0] : expectedActions.split(",");
    }


    public Architecture getReal() throws ArchException {
        Path rootFolder = get(".");
        SourceArchitectureBuilder builder = new DefaultSourceArchitectureBuilder(rootFolder);
        ArchitectureDescription description = getArchitectureDescription();
        description.forEachUsedClass((className, usedClasses) ->
                builder.addClass(description.getModuleForClass(className),
                        getPackageName(className), getSimpleName(className), className,
                        usedClasses.toArray(String[]::new)));
        description.getModules().stream()
                .map(moduleName -> new SourceFolder(moduleName, rootFolder.resolve(moduleName)))
                .forEach(builder::addSourceFolder);
        return builder.build();
    }
}
