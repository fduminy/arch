package fr.duminy.arch.recommendation;

import fr.duminy.arch.api.ArchElement;
import fr.duminy.arch.api.Architecture;
import fr.duminy.arch.api.ClassDatabase;
import fr.duminy.arch.api.Cycle;
import fr.duminy.arch.cycle.CyclesDescription.MockCycle;
import fr.duminy.arch.cycle.Elements;
import fr.duminy.arch.dsl.ActionDescription;
import fr.duminy.arch.engine.Action;

import java.util.Collection;
import java.util.function.BiFunction;
import java.util.function.Function;

import static fr.duminy.arch.recommendation.NullAction.NULL_ACTION;

class MockGeneration<S extends ArchElement, T extends ArchElement> {
    private final TriFunction<ClassDatabase, S, T, Action> actionFactory;
    private final BiFunction<ClassDatabase, String, S> sourceGetter;
    private final BiFunction<ClassDatabase, String, T> targetGetter;
    private Generation generation;
    private String[] possibleActions;
    private Collection<T> elements;

    public MockGeneration(String[] history,
                          Function<MockArchitectureWithActions, Elements<T>> elementsGetter,
                          TriFunction<ClassDatabase, S, T, Action> actionFactory,
                          BiFunction<ClassDatabase, String, S> sourceGetter,
                          BiFunction<ClassDatabase, String, T> targetGetter) {
        this.actionFactory = actionFactory;
        this.sourceGetter = sourceGetter;
        this.targetGetter = targetGetter;
        Action action = NULL_ACTION;
        for (String historyEntry : history) {
            ExtMockArchitectureWithActions<T> mock = new ExtMockArchitectureWithActions<>(historyEntry, elementsGetter);
            Architecture architecture = mock.getMock();
            elements = mock.getElements();
            generation = new Generation(generation, action, architecture);

            possibleActions = mock.getExpectedActions();
            action = possibleActions.length == 0 ? NULL_ACTION : createAction(architecture, possibleActions[0]);
        }
    }

    public Generation getGeneration() {
        return generation;
    }

    private Action createAction(Architecture architecture, String expectedAction) {
        ActionDescription action = new ActionDescription(expectedAction);
        S source = sourceGetter.apply(architecture.database(), action.getElementToMove());
        T target = targetGetter.apply(architecture.database(), action.getTarget());
        return actionFactory.apply(architecture.database(), source, target);
    }

    public String[] getExpectedPossibleActions() {
        return possibleActions;
    }

    public Cycle<T> getCycle() {
        MockCycle<T> cycle = new MockCycle<>();
        cycle.addAll(elements);
        return cycle;
    }

    private static class ExtMockArchitectureWithActions<T extends ArchElement> extends MockArchitectureWithActions {
        private final Function<MockArchitectureWithActions, Elements<T>> elementsGetter;

        ExtMockArchitectureWithActions(String architecture,
                                       Function<MockArchitectureWithActions, Elements<T>> elementsGetter) {
            super(architecture);
            this.elementsGetter = elementsGetter;
        }

        public Collection<T> getElements() {
            return elementsGetter.apply(this).elements();
        }
    }

    interface TriFunction<A, B, C, R> {
        R apply(A a, B b, C c);
    }
}
