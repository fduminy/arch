package fr.duminy.arch.recommendation;

import fr.duminy.arch.api.ArchClass;
import fr.duminy.arch.api.ArchPackage;
import fr.duminy.arch.api.Cycle;
import fr.duminy.arch.cycle.MockArchitecture;
import fr.duminy.arch.engine.MoveClass;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static fr.duminy.arch.utils.ClassUtils.getSimpleName;
import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class PossibleMoveClassActionFinderTest {
    @InjectMocks
    private PossibleMoveClassActionFinder underTest;

    @ParameterizedTest
    @ValueSource(strings = {
            "m1:x.A->y.B | m1:y.B->x.A => x.A->y,y.B->x",
            "m1:x.A->y.B | m1:y.B->x.A => x.A->y ; m1:y.A->y.B | m1:y.B->y.A =>",
            "m1:x.A->y.B | m1:y.B->x.A => y.B->x ; m1:x.A->x.B | m1:x.B->x.A =>",

            "m1:x.A->y.B | m1:y.B->z.C | m1:z.C->x.A => x.A->y,x.A->z,y.B->x,y.B->z,z.C->x,z.C->y",
            "m1:x.A->y.B | m1:y.B->z.C | m1:z.C->x.A => x.A->y ;" +
                    "m1:y.A->y.B | m1:y.B->z.C | m1:z.C->y.A => y.A->z,y.B->z,z.C->y",

            "m1:u.A->v.B | m2:v.B->v.C | m2:v.C->u.A => v.C->u ;" +
                    "m1:u.A->v.B | m2:v.B->u.C | m2:u.C->u.A => u.A->v,v.B->u",

            // packages won't go out of their module to break package cycles
            "m1:u.A->v.B | m2:v.B->v.C | m2:v.C->u.A => v.C->u ;" +
                    "m1:u.A->v.B | m2:v.B->u.C | m2:u.C->u.A => u.C->v ;" +
                    "m1:u.A->v.B | m2:v.B->v.C | m2:v.C->u.A =>"
    })
    void getPossibleActions(String history) {
        MockGeneration<ArchClass, ArchPackage> mockGeneration = new MockGeneration<>(history.split(";"),
                MockArchitecture::getNameToPackage,
                (database, classToMove, targetPackage) -> new MoveClass(database.packag(classToMove), classToMove, targetPackage),
                (database, name) -> database.classes().get(getSimpleName(name)),
                (database, name) -> database.packages().get(name));
        Generation generation = mockGeneration.getGeneration();
        String[] expectedActions = mockGeneration.getExpectedPossibleActions();
        Cycle<ArchPackage> cycle = mockGeneration.getCycle();
        assertThat(underTest.getPossibleActions(generation, cycle).stream()
                .map(MoveClass::toString))
                .containsExactlyInAnyOrder(expectedActions);
    }
}