package fr.duminy.arch.recommendation;

import fr.duminy.arch.api.ArchModule;
import fr.duminy.arch.api.ArchPackage;
import fr.duminy.arch.api.Cycle;
import fr.duminy.arch.cycle.MockArchitecture;
import fr.duminy.arch.engine.MovePackage;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class PossibleMovePackageActionFinderTest {
    @InjectMocks
    private PossibleMovePackageActionFinder underTest;

    @ParameterizedTest
    @ValueSource(strings = {
            "m1:x.A->y.B | m2:y.B->x.A => x->m2,y->m1",
            "m1:x.A->y.B | m2:y.B->x.A => x->m2 ; m2:x.A->y.B | m2:y.B->x.A =>",
            "m1:x.A->y.B | m2:y.B->x.A => y->m1 ; m1:x.A->y.B | m1:y.B->x.A =>",

            "m1:x.A->y.B | m2:y.B->z.C | m3:z.C->x.A => x->m2,x->m3,y->m1,y->m3,z->m1,z->m2",
            "m1:x.A->y.B | m2:y.B->z.C | m3:z.C->x.A => x->m2 ;" +
                    "m2:x.A->y.B | m2:y.B->z.C | m3:z.C->x.A => x->m3,y->m3,z->m2"
    })
    void getPossibleActions(String history) {
        MockGeneration<ArchPackage, ArchModule> mockGeneration = new MockGeneration<>(history.split(";"),
                MockArchitecture::getNameToModule,
                (database, packageToMove, targetModule) -> new MovePackage(packageToMove, targetModule),
                (database, name) -> database.packages().get(name),
                (database, name) -> database.modules().get(name));
        Generation generation = mockGeneration.getGeneration();
        String[] expectedActions = mockGeneration.getExpectedPossibleActions();
        Cycle<ArchModule> cycle = mockGeneration.getCycle();
        assertThat(underTest.getPossibleActions(generation, cycle).stream()
                .map(MovePackage::toString))
                .containsExactlyInAnyOrder(expectedActions);
    }
}