package fr.duminy.arch.utils;

import fr.duminy.arch.api.ArchClass;
import fr.duminy.arch.api.ArchPackage;

public class ClassUtils {
    private ClassUtils() {
        // utility class
    }

    public static String getPackageName(String className) {
        int endIndex = className.lastIndexOf('.');
        return (endIndex >= 0) ? className.substring(0, endIndex) : "";
    }

    public static String getSimpleName(String className) {
        String simpleName = className;
        int index = className.lastIndexOf('.');
        if (index > 0) {
            simpleName = className.substring(index + 1);
        }
        return simpleName;
    }

    public static String getFullQualifiedName(ArchPackage packag, ArchClass clazz) {
        String parentName = packag.name();
        String name = clazz.name();
        return getFullQualifiedName(parentName, name);
    }

    public static String getFullQualifiedName(String parentName, String name) {
        return (parentName == null) ? name : (parentName + '.' + name);
    }
}
