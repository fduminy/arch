package fr.duminy.arch.utils;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.assertj.core.api.Assertions.assertThat;

class ClassUtilsTest {
    @ParameterizedTest
    @CsvSource(value = {
            "A,''",
            "x.A,x",
            "x.y.A,x.y"
    })
    void getPackageName(String className, String expectedPackageName) {
        assertThat(ClassUtils.getPackageName(className)).isEqualTo(expectedPackageName);
    }

    @ParameterizedTest
    @CsvSource(value = {
            "A,A",
            "x.A,A",
            "x.y.A,A"
    })
    void getSimpleName(String className, String expectedSimpleName) {
        assertThat(ClassUtils.getSimpleName(className)).isEqualTo(expectedSimpleName);
    }
}